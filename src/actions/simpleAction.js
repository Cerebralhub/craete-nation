const SIMPLE_ACTION = 'SIMPLE_ACTION';

const simpleAction = () => dispatch => {
    dispatch({
        type: SIMPLE_ACTION,
        payload: 'result_simple_action'
    })
}

export default simpleAction