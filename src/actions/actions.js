export const FIRST_ACTION = 'FIRST_ACTION'
export const SECOND_ACTION = 'SECOND_ACTION'
export const THIRD_ACTION = 'THIRD_ACTION'

export const REQUEST_PENDING = 'REQUEST_PENDING'
export const REQUEST_PROCESSING = 'REQUEST_PROCESSING'
export const REQUEST_FULFILLED = 'REQUEST_FULFILLED'
export const REQUEST_REJECTED = 'REQUEST_REJECTED'

export const TOGGLE_LOADER = 'TOGGLE_LOADER'

export const VisibilityFilters = {
    SHOW_ALL: 'SHOW_ALL',
    SHOW_COMPLETED: 'SHOW_COMPLETED',
    SHOW_ACTIVE: 'SHOW_ACTIVE'
  }

export function toggleLoader(status) {
    return {
        type: TOGGLE_LOADER,
        status
    }
}

 export function mainAction() {
    return {
        type: FIRST_ACTION,
        text: 'To see the first action'
    }
}

