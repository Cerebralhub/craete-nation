import React, { Component } from 'react'
import { Link, NavLink } from 'react-router-dom'

import userTypes from '../../constants/userTypes'

import LogoutFunc from '../helpers/logout'

import TriangleLoader from '../Loader/triangleLoader'
// import BrowseJob from '../Professionals/BrowseJobs';

// import LogOut from '../helpers/logout'

import Shimmer from "react-shimmer-effect";
import injectSheet from "react-jss";


import axiosInterceptors from '../helpers/axiosInterceptors';

const StyleSheet = {
  container: {
    border: "0px solid rgba(255, 255, 255, 1)",
    boxShadow: "0px 0px 20px rgba(0, 0, 0, .1)",
    borderRadius: "4px",
    backgroundColor: "white",
    display: "flex",
    padding: "16px",
    width: "200px"
  },
  circle: {
    height: "56px",
    width: "56px",
    borderRadius: "50%"
  },
  line: {
    width: "96px",
    height: "8px",
    alignSelf: "center",
    marginLeft: "16px",
    borderRadius: "8px"
  }
};


class Sidebar extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            userTypeId: ''
        }
    }

    logout = () => {
        LogoutFunc()
    }

    componentDidMount() {
        this.getUserType()
    }

    getUserType() {
        axiosInterceptors().get('/users', {
            params: {
              token: localStorage.getItem('token')
            }
          })
          .then((response) => {
            this.setState({
                userTypeId: response.data.user.usersTypeId
            })
          })
          .catch((err) => {
            // alert(err.response.data.error);
            console.log(err.response.data.error);
            this.setState({ error: err.response.data.error })
            // this.logout()
          })
    }

    activeRoute = (url) => {
      const currentUrl = window.location.pathname;

      if(url == currentUrl)
        return "active"
    }
    
    render() {
      
      const { classes } = this.props;

        if(this.state.userTypeId) {
            // Sidebar For Employers
            if(this.state.userTypeId == userTypes.employers) {
                return(
                    <div className="col-md-12 col-lg-3 dashboard-left-border">
                    <div className="dashboard-left shadow">
                      <ul className="dashboard-menu">
                        <li className={this.activeRoute('/employerdashboard')}><Link to="/employerdashboard"><i className="fa fa-tachometer" />Dashboard</Link></li>
                        <li className={this.activeRoute('/createjob')}><Link to="/createjob"><i className="fa fa-envelope-open" />Post Job</Link></li>
                        <li className={this.activeRoute('/managecandidates')}><Link to="/managecandidates"><i className="fa fa-users" />Manage Candidates</Link></li>
                        <li className={this.activeRoute('/companyprofile')}><Link to="/companyprofile"><i className="fa fa-cog" />Account Settings</Link></li>
                        {/* <li><Link to="message.html"><i className="fa fa-envelope-open" />messages</Link></li> */}
                        {/* <li className={this.activeRoute('/createjob')}><Link to="/createjob"><i className="fa fa-envelope-open" />Post Project</Link></li> */}
                        {/* <li><Link to="manage-candidates.html"><i className="fa fa-briefcase" />manage candidates</Link></li>
                        <li><Link to="transaction.html"><i className="fa fa-rocket" />transaction</Link></li> */}
                        <li className={this.activeRoute('/changepassword')}><Link to="/changepassword"><i className="fa fa-lock" />Change Password</Link></li>
                        <li><Link to="" onClick={this.logout}><i className="fa fa-power-off" />LogOut</Link></li>
                      </ul>
                    </div>
                  </div>
                )
            }
            // Sidebar For Professionals or candidates
            else if(this.state.userTypeId == userTypes.professionals) {
                return(
                    <div className="col-lg-3 col-md-12 dashboard-left-border">
                        <div className="dashboard-left shadow">
                            <ul className="dashboard-menu">
                              <li className={this.activeRoute('/candidatedashboard')}>
                                <Link to="/candidatedashboard"><i className="fa fa-tachometer"></i>Dashboard</Link>
                              </li>
                              {/* <li className={this.activeRoute('/profile')}><Link to='/profile'><i className="fa fa-users"></i>My Profile</Link></li> */}
                              {/* <li><Link to="message.html"><i className="fa fa-envelope-open"></i>messages</Link></li>
                              <li><Link to="manage-jobs.html"><i className="fa fa-briefcase"></i>manage jobs</Link></li> */}
                              <li className={this.activeRoute('/browsejobs')}><Link to="/browsejobs"><i className="fa fa-briefcase"></i>Billing</Link></li>
                              <li className={this.activeRoute('/managejobs')}><Link to="/managejobs"><i className="fa fa-briefcase"></i>Manage Jobs</Link></li>
                              <li className={this.activeRoute('/changepassword')}><Link to="/changepassword"><i className="fa fa-lock"></i>Change password</Link></li>
                              <li><Link to="" onClick={this.logout}><i className="fa fa-power-off"></i>Logout</Link></li>
                            </ul>
                        </div>
                    </div>
                )
            }
        } else {
            return(
            <div className={classes.container}>
              <Shimmer>
                 <div className={classes.line} />
                 <div className={classes.line} />
              </Shimmer>
           </div>
           )
        } 
    }
}

export default injectSheet(StyleSheet)(Sidebar);