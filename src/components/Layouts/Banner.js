import React from 'react';
import { Link } from 'react-router-dom';


class Banner extends React.Component{
    constructor(props){
        super(props);

        this.state = {
            name: props.name,
            image: props.image
        }
        

    }

    render(){
        
        return(
            <section className="jobguru-breadcromb-area">
                {/* <div className="breadcromb-top section_100">
                    <div className="container">
                    <div className="row">
                        <div className="col-md-12">
                            <div className="breadcromb-box">
                                <h3>{this.props.name}</h3>
                            </div>
                        </div>
                    </div>
                    </div>
                </div> */}
                <div className="breadcromb-bottom">
                    <div className="container">
                    <div className="row">
                        <div className="col-md-12">
                            <div className="breadcromb-box-pagin">
                                <ul>
                                <li><Link to="/">home</Link></li>
                                <li className="active-breadcromb"><Link to="#">{this.props.name}</Link></li>
                                </ul>
                            </div>
                            <hr />
                        </div>
                    </div>
                    </div>
                </div>
            </section>
        )
    }
}

export default Banner;
