import React from 'react';
import logo from '../../assets/img/logo.png';
//import footerpost2 from '../assets/img/footer-post-2.jpg';
//import footerpost3 from '../assets/img/footer-post-3.jpg';
import { Link } from 'react-router-dom'




const Footer = () => {
    return(
        <div>
            <footer className="jobguru-footer-area">
                <div className="footer-top section_50">
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-3 col-md-6">
                                <div className="single-footer-widget">
                                    <div className="footer-logo">
                                        <Link to="\">
                                            <img src={logo} alt="jobguru logo"/>
                                        </Link>
                                    </div>
                                    <ul className="footer-social">
                                        <li className="mr-1">
                                            <Link to="#" className="fb">
                                                <i className="fa fa-facebook"></i>
                                            </Link>
                                        </li>
                                        <li className="mr-1">
                                            <Link to="#" className="twitter">
                                                <i className="fa fa-twitter"></i>
                                            </Link>
                                        </li>
                                        <li className="mr-1">
                                            <Link to="#" className="linkedin">
                                                <i className="fa fa-linkedin"></i>
                                            </Link>
                                        </li>
                                        <li className="mr-1">
                                            <Link to="#" className="instagram">
                                                <i className="fa fa-instagram"></i>
                                            </Link>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div className="col-lg-3 col-md-6">
                                <div className="single-footer-widget">
                                    <h3>main links</h3>
                                    <ul>
                                    <li><Link to="#"><i className="fa fa-angle-double-right "></i> About Us</Link></li>
                                    <li><Link to="/contact"><i className="fa fa-angle-double-right "></i> Contact</Link></li>
                                    <li><Link to="/browsejobs"><i className="fa fa-angle-double-right "></i> Browse Jobs</Link></li>
                                    </ul>
                                </div>
                            </div>
                            <div className="col-lg-3 col-md-6">
                            <div className="single-footer-widget footer-contact">
                                <h3>Other Links </h3>
                                <ul>
                                <li><Link to="#"><i className="fa fa-angle-double-right "></i> FAQs </Link></li>
                                <li><Link to="/privacypolicy"><i className="fa fa-angle-double-right "></i> Privacy Policy </Link></li>
                                <li><Link to="/terms&condition"><i className="fa fa-angle-double-right "></i> Terms And Conditions </Link></li>
                                </ul>
                            </div>
                        </div>
                            <div className="col-lg-3 col-md-6">
                                <div className="single-footer-widget footer-contact">
                                    <h3>Contact Info</h3>
                                    <p><i className="fa fa-map-marker"></i> 47, Oba Yekini Street, Ikate, Elegushi, Lekki</p>
                                    <p><i className="fa fa-phone"></i> 012-3456-789</p>
                                    <p><i className="fa fa-envelope-o"></i> info@squarework.com</p>
                                    <p><i className="fa fa-fax"></i> 112-3456-7898</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="footer-copyright">
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-12">
                                <div className="copyright-left">
                                    <p>Copyright &copy; 2020 SquareWork. All Rights Reserved</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
        </div>
    )
}

export default Footer;