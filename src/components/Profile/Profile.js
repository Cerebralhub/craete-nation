import React from 'react';
import Layout from '../Layouts/Layout';
import Banner from '../Layouts/Banner';
import Sidebar from '../Layouts/Sidebar';
import axiosInterceptors from '../helpers/axiosInterceptors';

import swal from 'sweetalert';

class Profile extends React.Component{

  constructor(props) {
    super(props)

    this.state = {
      firstName: '',
      lastName: '',
      phoneNumber: '',
      email: '',
      address: '',
      isLoading: true,
      categories: null,
      category: 1,
      categoryId: '',
      gender: 'male',
      dateOfBirth: '',
      bio: '',
      maritalStatus: 'single'
    }
  }

  componentDidMount() {
    this.getUserInfo()
    this.getCategories()
    this.getProfessionalDetails()
  }

  getCategories() {
    axiosInterceptors().get('/categories')
    .then((response) => {
       this.setState({
          categories: response.data.categories,
          isLoading: false
       })
    })
    .catch((err) => {
       swal('Categories could not be loaded', 'There was a problem loading categories', 'error');
    })
 }

 handleSubmit = e => {
  e.preventDefault();
  
  //Do some checks here
  this.updateProfessional()
  // alert(this.state.category)
}; 

  getProfessionalDetails = () => {
    axiosInterceptors().get('/professionals/show', {
      params: {
        'token': localStorage.getItem('token')
      }
    }).then((response) => {
      if(response.data.category.bio) {
        
      }
      this.setState({
        bio: response.data.category.bio,
        gender: response.data.category.gender,
        category: response.data.category.name,
        maritalStatus: response.data.category.maritalStatus,
        categoryId: response.data.category.categoryId
      })
      
    })
    .catch((err) => {
      this.setState({
        bio: '',
        gender: '',
        category: '',
        maritalStatus: '',
        categoryId: ''
      })
    })
  }

  updateProfessional = () => {
    axiosInterceptors().post('/professionals', {
      // 'firstName': this.state.firstName,
      // 'lastName': this.state.lastName,
      // 'email': this.state.email,
      // 'phoneNumber': this.state.phoneNumber,
      // 'address': this.state.address,
      'categoryId': this.state.category,
      'gender': this.state.gender,
      'dateOfBirth': this.state.dateOfBirth+' 00:00:00',
      'bio': this.state.bio,
      'token': localStorage.getItem('token'),
      'maritalStatus': this.state.maritalStatus
    })
    .then((response) => {
      // alert(response.data.success)
      swal(response.data.success, 'Completed', 'success');
    })
    .catch((err) => {
      // alert(err.response.data.error)
      swal(err.response.data.error, 'Not Successful', 'error');
      console.log(err)
    })

  }

  getUserInfo = () => {
    axiosInterceptors().get('/users', {
      params: {
        token: localStorage.getItem('token')
      }
    })
    .then((response) => {
      this.setState({
        firstName: response.data.user.firstName,
        lastName: response.data.user.lastName,
        phoneNumber: response.data.user.phoneNumber,
        email: response.data.user.email,
        address: response.data.user.address,
      })
    })
    .catch((err) => {
      // alert(err.response.data.error);
      swal(err.response.data.error, 'Not Successful', 'error');
      console.log(err.response.data.error);
      this.setState({ error: err.response.data.error })
    })
  }

  returnGenderSelected = (value) => {
    if(this.state.gender == value) 
      return true
  }

  returnMaritalSelected = (value) => {
    if(this.state.maritalStatus == value) 
      return true
  }

  returnCategorySelected = (value) => {
    if(this.state.category == value) 
      return true
  }

    render(){



        return(
            <Layout>
      <div className="mt-5"></div>
        {/* Candidate Dashboard Area Start */}
        <section className="candidate-dashboard-area section_70">
          <div className="container">
            <div className="row">

                 <Sidebar />
                 
              <div className="col-lg-9 col-md-12">
                <div className="dashboard-right">
                  <div className="candidate-profile shadow">
                    
                    <div className="candidate-single-profile-info">
                      <form onSubmit={this.handleSubmit}>
                        <div className="resume-box">
                          <h3>My Profile</h3>
                          <div className="single-resume-feild feild-flex-2">
                            <div className="single-input">
                              <label htmlFor="name">First Name:</label>
                              <input type="text" defaultValue={this.state.firstName} onChange={event => this.setState({ firstName: event.target.value }) } id="name" />
                            </div>
                            <div className="single-input">
                              <label htmlFor="name">Last Name:</label>
                              <input type="text" defaultValue={this.state.lastName} onChange={event => this.setState({ firstName: event.target.value }) } id="name" />
                            </div>
                          </div>

                          <div className="single-resume-feild feild-flex-2">

                            <div className="single-input">
                              <label htmlFor="name">Phone Number:</label>
                              <input type="text" placeholder={this.state.phoneNumber} onChange={event => this.setState({ lastName: event.target.value }) } id="name" />
                            </div>

                            <div className="single-input">
                              <label htmlFor="Category">Category:</label>
                              <select id="category" onChange={event => this.setState({ category: event.target.value }) }>
                              <option disabled>Category</option>
                                {!this.state.isLoading ?
                                  this.state.categories.map(theCategory => {

                                    const {name, id} = theCategory;

                                    return(<option selected={this.returnCategorySelected(name)} value={id}>{name}</option>)
                                  })
                                  :
                                  <option disabled>Loading categories...</option>
                                }
                                
                              </select>
                            </div>
                          </div>
                          <div className="single-resume-feild feild-flex-2">
                            <div className="single-input">
                              <label htmlFor="Gender">Gender:</label>
                              <select id="gender" onChange={event => this.setState({ gender: event.target.value }) }>
                                <option disabled>Gender</option>
                                
                                <option selected={this.returnGenderSelected('male') } value="male">Male</option>
                                <option selected={this.returnGenderSelected('female')} value="female">Female</option>
                                
                              </select>
                            </div>
                            <div className="single-input">
                              <label htmlFor="dob">Date Of Birth:</label>
                              <input type="date" id="dob" onChange={event => this.setState({ dateOfBirth: event.target.value }) } />
                            </div>
                          </div>

                          {/* <div className="single-resume-feild feild-flex-2">
                            <div className="single-input">
                              <label htmlFor="maritalStatus">Marital Status:</label>
                              <select id="maritalStatus" onChange={event => this.setState({ maritalStatus: event.target.value }) }>
                                <option disabled>Marital Status</option>
                                
                                <option selected={this.returnMaritalSelected('single')} value="single">Single</option>
                                <option selected={this.returnMaritalSelected('married')} value="married">Married</option>
                                <option  selected={this.returnMaritalSelected('divorced')} value="divorced">Divorced</option>
                                
                              </select>
                            </div>
                          </div> */}
                    
                          <div className="single-resume-feild ">
                            <div className="single-input">
                              <label htmlFor="bio">Introduce Yourself:</label>
                              <textarea id="bio" onChange={event => this.setState({ bio: event.target.value }) } placeholder={this.state.bio} />
                            </div>
                          </div>
                        </div>
                        <div className="resume-box">
                          <h3>Contact Information</h3>
                          <p>
                            <button className="btn mx-4" type="button" style={{backgroundColor:"#20D6DE", color:"white"}} data-toggle="collapse" data-target="#collapseContact" aria-expanded="false" aria-controls="collapseExample">
                              <i className="fa fa-plus mx-1"></i><span className="mx-1">Add</span>
                            </button>
                          </p>
                          <div className="collapse" id="collapseContact">
                          <div className="single-resume-feild feild-flex-2 mt-4">
                            <div className="single-input">
                              <label htmlFor="Email">Email:</label>
                              <input type="email" placeholder={this.state.email} onChange={event => this.setState({ email: event.target.value }) } id="Email" />
                            </div>
                          </div>
                          <div className="single-resume-feild feild-flex-2">
                            <div className="single-input">
                              <label htmlFor="Address">Address:</label>
                              <input type="text" placeholder={this.state.address} onChange={event => this.setState({ address: event.target.value }) } id="Address" />
                            </div>
                          </div>
                          </div>
                        </div>

                        <div className="resume-box">
                          <h3>Education</h3>
                          <p>
                            <button className="btn mx-4" type="button" style={{backgroundColor:"#20D6DE", color:"white"}} data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                            <i className="fa fa-plus mx-1"></i><span className="mx-1">Add</span>
                            </button>
                          </p>
                          <div className="collapse" id="collapseExample">
                            <div className="card card-body my-2">
                              Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident.
                            </div>
                          </div>
                        </div>

                        <div className="resume-box">
                          <h3>Certification</h3>
                          <p>
                            <button className="btn mx-4" type="button" style={{backgroundColor:"#20D6DE", color:"white"}} data-toggle="collapse" data-target="#collapseCert" aria-expanded="false" aria-controls="collapseExample">
                            <i className="fa fa-plus mx-1"></i><span className="mx-1">Add</span>
                            </button>
                          </p>
                          <div className="collapse" id="collapseCert">
                            <div className="card card-body my-2">
                              Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident.
                            </div>
                          </div>
                        </div>

                        <div className="resume-box">
                          <h3>Social Links</h3>
                          <p>
                            <button className="btn mx-4" type="button" style={{backgroundColor:"#20D6DE", color:"white"}} data-toggle="collapse" data-target="#collapseSocial" aria-expanded="false" aria-controls="collapseExample">
                            <i className="fa fa-plus mx-1"></i><span className="mx-1">Add</span>
                            </button>
                          </p>
                          <div className="collapse" id="collapseSocial">
                          <div className="single-resume-feild feild-flex-2 mt-4">
                            <div className="single-input">
                              <label htmlFor="name">Facebook:</label>
                              <input type="text" id="facebook" />
                            </div>
                            <div className="single-input">
                              <label htmlFor="name">Instagram:</label>
                              <input type="text"  id="instagram" />
                            </div>
                          </div>

                          <div className="single-resume-feild feild-flex-2">
                            <div className="single-input">
                              <label htmlFor="name">Behance:</label>
                              <input type="text" id="behance" />
                            </div>
                            <div className="single-input">
                              <label htmlFor="name">Twitter:</label>
                              <input type="text"  id="twitter" />
                            </div>
                          </div>

                          </div>
                        </div>

                        <div>
      </div>

                        <div className="submit-resume">
                          <button type="submit">Update</button>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        {/* Candidate Dashboard Area End */}
      
            </Layout>
        );
    }
}

export default Profile