import React from 'react'
import axios from "axios";

import "react-loader-spinner/dist/loader/css/react-spinner-loader.css"
import Loader from 'react-loader-spinner'

import store from '../../store'

import {toggleLoader} from '../../actions/actions'


const serviceBase = ({showLoader = true} = {}) => {

    const api = axios.create({
        baseURL: `${process.env.REACT_APP_API_URL}`,
        headers: {
            'Authorization': localStorage.getItem('token')
        },
        
    });

    api.interceptors.request.use( (config) => {
       
        if(showLoader) {
            store.dispatch(toggleLoader(true));
        }

        return config;
    });

    // api.interceptors.response.use(async (config) => {
    //     // try {
    //     //     // In this moment, hide the spinner
    //     //     // hideLoading();
    //     // }
    //     // catch(e)
    //     // {
    //     //     alert('Error response' + e);   
    //     // }

    //     return config;
    // });

    return api;
};

export default serviceBase;