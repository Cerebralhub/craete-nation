import React from 'react';
import { Link, Redirect, RedirectProps } from 'react-router-dom';

const logout = () => {
    localStorage.removeItem('token')
    localStorage.clear()

    const currentUrl = window.location.href;

    if(!currentUrl.pathname) {
        return window.location.reload();
    }

    return <Redirect to="/" />
 }

export default logout;