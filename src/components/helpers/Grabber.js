import axiosInterceptors from './axiosInterceptors'

 const Grabber = {
    getCategories: () => {
        return axiosInterceptors().get('/categories')
    },
    getUserType: () => {
        return axiosInterceptors().get('/users', {
            params: {
              token: localStorage.getItem('token')
            }
          })
    },
}

export default Grabber