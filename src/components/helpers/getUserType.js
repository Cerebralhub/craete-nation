import axiosInterceptors from './axiosInterceptors'

const getUserType = () => {
    return axiosInterceptors().get('/users', {
        params: {
          token: localStorage.getItem('token')
        }
      })
      // .then((response) => {
      //   return response.data.user.usersTypeId
      // })
      // .catch((err) => {
      //   return err.response.data.error
      // })
}

export default getUserType