import axiosInterceptors from './axiosInterceptors'

const getCategories = () => {
    return axiosInterceptors().get('/categories')
}

export default getCategories