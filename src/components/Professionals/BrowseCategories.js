import React, { Component } from 'react'
import Banner from '../Layouts/Banner';
import Layout from '../Layouts/Layout';

export default class componentName extends Component {
  render() {
    return (
      <Layout>
         <div className="mt-5"></div>

          {/* Categories Area Start */}
      <section className="jobguru-categories-area browse-category-page section_70">
        <div className="container">
          <div className="row">
            <div className="col-md-12">
              <div className="browse-job-head-option shadow">
                <div className="job-browse-search">
                  <form>
                    <input type="search" placeholder="Search Jobs Here..." />
                    <button type="submit"><i className="fa fa-search" /></button>
                  </form>
                </div>
                <div className="job-browse-action">
                  {/* <div className="email-alerts">
                    <input type="checkbox" className="styled" id="b_1" />
                    <label className="styled" htmlFor="b_1">email alerts for this search</label>
                  </div> */}
                  <div className="dropdown">
                    <button className="btn-dropdown dropdown-toggle" type="button" id="dropdowncur" data-toggle="dropdown" aria-haspopup="true">Sort By</button>
                    <ul className="dropdown-menu" aria-labelledby="dropdowncur">
                      <li>Newest</li>
                      <li>Oldest</li>
                      <li>Random</li>
                    </ul>
                  </div>
                </div>
              </div>
              <div className="all-sub-category clearfix">
                <div className="search-category-box">
                  <h3>Design, Art &amp; Multimedia</h3>
                  <ul className="list_category">
                    <li className="in checkbox">
                      <input className="checkbox-spin" type="checkbox" id="cat_101" />
                      <label htmlFor="cat_101"><span />logo design</label>
                    </li>
                    <li className="in checkbox">
                      <input className="checkbox-spin" type="checkbox" id="cat_11" />
                      <label htmlFor="cat_11"><span /> Illustration</label>
                    </li>
                    <li className="in checkbox">
                      <input className="checkbox-spin" type="checkbox" id="cat_12" />
                      <label htmlFor="cat_12"><span />Business Cards &amp; Stationery</label>
                    </li>
                    <li className="in checkbox">
                      <input className="checkbox-spin" type="checkbox" id="cat_13" />
                      <label htmlFor="cat_13"><span />Presentation Design</label>
                    </li>
                    <li className="in checkbox">
                      <input className="checkbox-spin" type="checkbox" id="cat_14" />
                      <label htmlFor="cat_14"><span />Photoshop Editing</label>
                    </li>
                    <li className="in checkbox">
                      <input className="checkbox-spin" type="checkbox" id="cat_16" />
                      <label htmlFor="cat_16"><span />Book &amp; Album Covers</label>
                    </li>
                    <li className="in checkbox">
                      <input className="checkbox-spin" type="checkbox" id="cat_17" />
                      <label htmlFor="cat_17"><span />3D Models</label>
                    </li>
                    <li className="in checkbox">
                      <input className="checkbox-spin" type="checkbox" id="cat_18" />
                      <label htmlFor="cat_18"><span />Product Design</label>
                    </li>
                    <li className="in checkbox">
                      <input className="checkbox-spin" type="checkbox" id="cat_19" />
                      <label htmlFor="cat_19"><span />Logo Animation</label>
                    </li>
                    <li className="in checkbox">
                      <input className="checkbox-spin" type="checkbox" id="cat_20" />
                      <label htmlFor="cat_20"><span /> Lyric &amp; Music Videos</label>
                    </li>
                  </ul>
                </div>
                <div className="search-category-box">
                  <h3>Programming &amp; Tech</h3>
                  <ul className="list_category">
                    <li className="in checkbox">
                      <input className="checkbox-spin" type="checkbox" id="cat_31" />
                      <label htmlFor="cat_31"><span />php/laravel/codignator</label>
                    </li>
                    <li className="in checkbox">
                      <input className="checkbox-spin" type="checkbox" id="cat_32" />
                      <label htmlFor="cat_32"><span />Website Builders &amp; CMS</label>
                    </li>
                    <li className="in checkbox">
                      <input className="checkbox-spin" type="checkbox" id="cat_332" />
                      <label htmlFor="cat_332"><span />Desktop applications</label>
                    </li>
                    <li className="in checkbox">
                      <input className="checkbox-spin" type="checkbox" id="cat_33" />
                      <label htmlFor="cat_33"><span /> Chatbots</label>
                    </li>
                    <li className="in checkbox">
                      <input className="checkbox-spin" type="checkbox" id="cat_34" />
                      <label htmlFor="cat_34"><span />.NET framework</label>
                    </li>
                    <li className="in checkbox">
                      <input className="checkbox-spin" type="checkbox" id="cat_35" />
                      <label htmlFor="cat_35"><span />python programming</label>
                    </li>
                    <li className="in checkbox">
                      <input className="checkbox-spin" type="checkbox" id="cat_37" />
                      <label htmlFor="cat_37"><span />Mobile Apps &amp; Web</label>
                    </li>
                  </ul>
                </div>
                <div className="search-category-box">
                  <h3>Education / Training</h3>
                  <ul className="list_category">
                    <li className="in checkbox">
                      <input className="checkbox-spin" type="checkbox" id="cat_61" />
                      <label htmlFor="cat_61"><span />online lessons</label>
                    </li>
                    <li className="in checkbox">
                      <input className="checkbox-spin" type="checkbox" id="cat_63" />
                      <label htmlFor="cat_63"><span />web development course</label>
                    </li>
                    <li className="in checkbox">
                      <input className="checkbox-spin" type="checkbox" id="cat_64" />
                      <label htmlFor="cat_64"><span />python course</label>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      {/* Categories Area End */}
      </Layout>
    )
  }
}
