import React, { Component } from 'react'
import Layout from '../Layouts/Layout';
import Avatar from 'react-avatar';
import CompanyImage from '../../assets/img/company_page_logo.jpg';

class CompanyDetails extends Component {
  render() {
    return (
      <Layout>
         {/* Single Candidate Start */}
         <section className="single-candidate-page section_70">
          <div className="container">
            <div className="row">
              <div className="col-md-6">
                <div className="single-candidate-box">
                  <div className="single-candidate-img">
                  <Avatar name="Arino Enterprise" size="80" round="50px" color="Grey"/>
                  </div>
                  <div className="single-candidate-box-right">
                    <h4>Arino Enterprise</h4>
                    <p>Animation Studio</p>
                    <div className="single-candidate-rate">
                      <p className="rating-company">4.9</p>
                      <ul>
                        <li><i className="fa fa-star" /></li>
                        <li><i className="fa fa-star" /></li>
                        <li><i className="fa fa-star" /></li>
                        <li><i className="fa fa-star" /></li>
                        <li><i className="fa fa-star-half-o" /></li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-md-6">
                <div className="single-candidate-action">
                  <a href="#" className="candidate-contact"><i className="fa fa-gavel" />posted Jobs</a>
                </div>
              </div>
            </div>
          </div>
        </section>
        {/* Single Candidate End */}
        {/* Single Candidate Bottom Start */}
        <div className="single-candidate-bottom-area section_70">
          <div className="container">
            <div className="row">
              <div className="col-lg-9 col-md-8">
                <div className="single-candidate-bottom-left">
                  <div className="single-candidate-widget">
                    <h3>company description</h3>
                    <p>Duis ac augue sit amet ex blandit facilisis sit amet ut dui. Nulla pharetra fermentum mollis. Duis in tempor tortor. Suspendisse vitae nisl diam. Proin eu erat vestibulum, suscipit quam et, cursus ante.Ut sodales arcu sagittis metus molestie molestie. Nulla maximus volutpat dui. Etiam luctus lobortis massa in pulvinar. Maecenas nunc odio, </p>
                    <p>faucibus in malesuada a, dignissim at odio. Aenean eleifend urna.Nulla maximus volutpat dui. Etiam luctus lobortis massa in pulvinar. Maecenas nunc odio, faucibus in malesuada a, dignissim at odio.</p>
                    <ul className="company-desc-list">
                      <li><i className="fa fa-check" /> Sed consectetur tellus eget odio aliquet</li>
                      <li><i className="fa fa-check" /> Morbi maximus metus vitae mollis ante imperdiet</li>
                      <li><i className="fa fa-check" /> tellus eget vestibulum tellus sollicitudin</li>
                      <li><i className="fa fa-check" /> eros consectetur lectus tellus eget odio aliquet</li>
                      <li><i className="fa fa-check" /> Morbi maximus metus vitae mollis ante imperdiet</li>
                      <li><i className="fa fa-check" /> tellus eget vestibulum tellus sollicitudin</li>
                    </ul>
                    <p>Etiam quis interdum felis, at pellentesque metus. Morbi eget congue lectus. Donec eleifend ultricies urna et euismod. Sed consectetur tellus eget odio aliquet, vel vestibulum tellus sollicitudin. Morbi maximus metus eu eros tincidunt, vitae mollis ante imperdiet. Nulla imperdiet at mauris ut posuere.pellentesque metus. Morbi eget congue lectus. Donec eleifend ultricies urna et euismod. Sed consectetur tellus eget odio aliquet, vel vestibulum tellus sollicitudin. Morbi maximus metus eu eros tincidunt, vitae mollis ante imperdiet. Nulla imperdiet at mauris ut.pellentesque metus. Morbi eget congue lectus. Donec eleifend ultricies urna et euismod. Sed consectetur tellus eget odio aliquet, vel vestibulum tellus sollicitudin. Morbi maximus metus eu eros tincidunt, vitae mollis ante imperdiet. Nulla imperdiet</p>
                    <p>faucibus in malesuada a, dignissim at odio. Aenean eleifend urna.Nulla maximus volutpat dui. Etiam luctus lobortis massa in pulvinar. Maecenas nunc odio, faucibus in malesuada a, dignissim at odio.</p>
                  </div>
                  <div className="single-candidate-widget">
                    <h3>(2) Open Positions</h3>
                    <div className="single-work-history company-single-page">
                      <div className="single-candidate-list">
                        <div className="main-comment">
                          <div className="candidate-text">
                            <div className="candidate-info">
                              <div className="candidate-title">
                                <h3><a href="#">Lead UX/UI Designer</a></h3>
                              </div>
                              <p className="company-state">
                                <i className="fa fa-map-marker" />
                                Chicago
                              </p>
                              <p className="open-icon">
                                <i className="fa fa-clock-o" />
                                Full Time
                              </p>
                              <p className="varify">
                                <i className="fa fa-check" />
                                Salary : $800-$1200
                              </p>
                            </div>
                            <div className="candidate-text-inner">
                              <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Etiam eu velit cursus, tempor ipsum in, tempus lectus. Nullam tempus nisi id nisl luctus, non tempor justo molestie.</p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="single-work-history company-single-page">
                      <div className="single-candidate-list">
                        <div className="main-comment">
                          <div className="candidate-text">
                            <div className="candidate-info">
                              <div className="candidate-title">
                                <h3><a href="#">C Developer (Senior) C .Net</a></h3>
                              </div>
                              <p className="company-state">
                                <i className="fa fa-map-marker" />
                                Chicago
                              </p>
                              <p className="open-icon">
                                <i className="fa fa-clock-o" />
                                Full Time
                              </p>
                              <p className="varify">
                                <i className="fa fa-check" />
                                Salary : $800-$1200
                              </p>
                            </div>
                            <div className="candidate-text-inner">
                              <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Etiam eu velit cursus, tempor ipsum in, tempus lectus. Nullam tempus nisi id nisl luctus, non tempor justo molestie.</p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-lg-3 col-md-4">
                <div className="single-candidate-bottom-right">
                  {/* <div className="single-candidate-widget-2">
                    <a href="#" className="jobguru-btn-2">
                      <i className="fa fa-balance-scale" />
                      compare with others
                    </a>
                  </div> */}
                  <div className="single-candidate-widget-2">
                    <ul>
                      <li><i className="fa fa-envelope" /> example@mail.com</li>
                      <li><i className="fa fa-phone" /> +11-012-3456-89</li>
                      <li><i className="fa fa-globe" /> www.example.com</li>
                    </ul>
                  </div>
                  <div className="single-candidate-widget-2">
                    <h3>Social Links</h3>
                    <ul className="candidate-social">
                      <li><a href="#"><i className="fa fa-facebook" /></a></li>
                      <li><a href="#"><i className="fa fa-twitter" /></a></li>
                      <li><a href="#"><i className="fa fa-linkedin" /></a></li>
                      <li><a href="#"><i className="fa fa-google-plus" /></a></li>
                      <li><a href="#"><i className="fa fa-instagram" /></a></li>
                    </ul>
                  </div>
                  <div className="single-candidate-widget-2">
                    <h3>Quick Contacts</h3>
                    <form>
                      <p>
                        <input type="text" placeholder="Your Name" />
                      </p>
                      <p>
                        <input type="email" placeholder="Your Email Address" />
                      </p>
                      <p>
                        <textarea placeholder="Write here your message" defaultValue={""} />
                      </p>
                      <p>
                        <button type="submit">Send Message</button>
                      </p>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        {/* Single Candidate Bottom End */}
      
      </Layout>
    )
  }
}

export default CompanyDetails;
