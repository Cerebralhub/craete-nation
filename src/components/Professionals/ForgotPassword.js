import React, { Component } from 'react'
import Layout from '../Layouts/Layout';
import Banner from '../Layouts/Banner';
import { Link } from 'react-router-dom';

// import axios from 'axios';

import axiosInterceptors from '../helpers/axiosInterceptors';

const emailRegex = RegExp(/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/)
 
const formValid =  ({ formErrors, ...rest }) => {
  let valid = true;

  // validate form errors being empty
  Object.values(formErrors).forEach( val => { 
    val.length > 0 && (valid = false)
  });

  // validate the form was filled out
  Object.values(rest).forEach(val => {
    val === null && (valid = false)
  });

  return valid;
}

class ForgotPassword extends Component {
    constructor(props) {
        super(props);
    
        this.state = {
          email: null,
          token: '',
          error: '',
          successMsg: '',
          errorMsg: '',
          formErrors: {
            email: "",
            password: ""
          }
        };
      }
    
      forgotPassword() {
        axiosInterceptors().post('/forgot-password', {
          'email': this.state.email
        })
        .then((response) => {
          alert(response.data.success);
          console.log(response);
          this.setState({ successMsg: response.data.success })
    
        })
        .catch((err) => {
          alert(err.response.data.error);
          console.log(err.response.data.error);
          this.setState({ error: err.response.data.error })
        })
      }
    
      handleSubmit = e => {
        e.preventDefault();
        
        if (formValid(this.state)) {
          // alert(`
          //    --SUBMITTING--
          //    email: ${this.state.email}
          //    password: ${this.state.password}
          // `);
    
          this.loginUser();
        } else {
          alert("FORM INVALID - DISPLAY ERROR MESSAGE");
        }
      }; 
    
      handleChange = e => {
        e.preventDefault();
        const { name, value } = e.target;
        let formErrors = this.state.formErrors;
    
        switch (name) {
            case "email":
                formErrors.email =
                emailRegex.test(value)  
                ?""
                : "invalid email address"
            break;
    
            case "password":
               formErrors.password =
               value.length < 6  
                ?"mininum 6 characters required"
               : "";
            break;
            default:
              break;
        }
        this.setState({ formErrors, [name]: value }, () => console.log(this.state));
      };
  render() {
    const { formErrors } = this.state;
    return (
      <Layout>
         <Banner name={this.props.name} />
         <section className="jobguru-login-area section_70">
        <div className="container">
          <div className="row">
            <div className="col-md-12">
              <div className="login-box">
                <div className="login-title">
                  <h3 style={{marginBottom: '15px'}}>Forgot your Password</h3>
                  <p style={{textAlign: 'center'}}>Dont worry! Resetting your password is easy,just type in 
                      the email your resgistered with.
                  </p>
                </div>
                <form onSubmit={this.handleSubmit} noValidate>
                  <div className="single-login-field">
                    <input type="email" placeholder="Email Address"
                     name="email" 
                     noValidate
                     onChange={this.handleChange}
                      />
                    {formErrors.email.length > 0 && (
                       <span className="errorMessage">{formErrors.email}</span>
                    )}
                  </div>
                  <div className="remember-row single-login-field clearfix">
                    <p className="lost-pass">
                      <Link to="/login">Rememeber your password? Login</Link>
                    </p>
                  </div>
                 <Link to="/confirmpassword">
                  <div className="single-login-field">
                    <button type="submit"> SEND</button>
                  </div>
                  </Link>
                </form>
                
              </div>
            </div>
          </div>
        </div>
      </section>
      {/* Login Area End */}

      </Layout>
    )
  }
}

export default ForgotPassword;
