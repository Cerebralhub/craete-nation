import React, { Component } from 'react';
import Layout from '../Layouts/Layout';
import { Link } from 'react-router-dom';
import Banner from '../Layouts/Banner';

import axiosInterceptors from '../helpers/axiosInterceptors';

import Company1 from '../../assets/img/company1.png';
import Company2 from '../../assets/img/company2.png';
import Company3 from '../../assets/img/company3.png';
import Company4 from '../../assets/img/company4.png';
import Sidebar from '../Layouts/Sidebar';

 class BrowseJob extends Component {

  super1

  getJobs = () => {
    axiosInterceptors().get('/jobs', {
      params: {
        'token': localStorage.getItem('token')
      }
    })
    .then((response) => {
      this.setState({
        firstName: response.data.user.firstName,
        lastName: response.data.user.lastName,
        email: response.data.user.email,
        userType: response.data.user.usersTypeId
      })
    })
  }

  render() {
    return (
      <Layout>
        
        <div className="mt-5"></div>

        {/* Top Job Area Start */}
      <section className="jobguru-top-job-area browse-page section_70">
        <div className="container">
          <div className="row">

            

            <div className="col-md-12">
              <div className="browse-job-head-option shadow">
                <div className="job-browse-search">
                  <form>
                    <input type="search" placeholder="Search Jobs Here..." />
                    <button type="submit"><i className="fa fa-search" /></button>
                  </form>
                </div>
                <div className="job-browse-action">
                  <div className="dropdown">
                    <button className="btn-dropdown dropdown-toggle" type="button" id="dropdowncur" data-toggle="dropdown" aria-haspopup="true">Sort By</button>
                    <ul className="dropdown-menu" aria-labelledby="dropdowncur">
                      <li>Newest</li>
                      <li>Oldest</li>
                      <li>Random</li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-md-12">
              <div className="available-count">
                <h4>07 jobs are available in <a href="#">Graphics Design</a> category</h4>
              </div>
            </div>
          </div>
          <div className="row">

            <div className="col-md-12 col-lg-4">
              <div className="sigle-top-job shadow">
                <div className="top-job-company-image">
                  <div className="company-logo-img">
                    <a href="#">
                      <img src={Company4} alt="company image" />
                    </a>
                  </div>
                  <h3><a href="#">3D Animator</a></h3>
                </div>
                <div className="top-job-company-desc">
                  <ul>
                    <li>Location <span className="company-state"><i className="fa fa-map-marker" /> Lekki</span></li>
                    <li>salary <span className="open-icon"><i className="fa fa-credit-card-alt" />₦600-₦120</span></li>
                    <li>status <span className="varify"><i className="fa fa-check" />part time</span></li>
                  </ul>
                  <div className="top-job-company-btn">
                    <Link to="/applyjobs" className="jobguru-btn">Bid now</Link>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-md-12 col-lg-4">
              <div className="sigle-top-job shadow">
                <div className="top-job-company-image">
                  <div className="company-logo-img">
                    <a href="#">
                      <img src={Company2} alt="company image" />
                    </a>
                  </div>
                  <h3><a href="#">2D Animator</a></h3>
                </div>
                <div className="top-job-company-desc">
                  <ul>
                    <li>Location <span className="company-state"><i className="fa fa-map-marker" /> Ikoyi</span></li>
                    <li>salary <span className="open-icon"><i className="fa fa-credit-card-alt" />₦600-₦120</span></li>
                    <li>status <span className="varify"><i className="fa fa-check" />Intern</span></li>
                  </ul>
                  <div className="top-job-company-btn">
                  <Link to="/applyjobs" className="jobguru-btn">Bid now</Link>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-md-12 col-lg-4">
              <div className="sigle-top-job shadow">
                <div className="top-job-company-image">
                  <div className="company-logo-img">
                    <a href="#">
                      <img src={Company3} alt="company image" />
                    </a>
                  </div>
                  <h3><a href="#">Illustrator</a></h3>
                </div>
                <div className="top-job-company-desc">
                  <ul>
                    <li>Location <span className="company-state"><i className="fa fa-map-marker" /> Ajah</span></li>
                    <li>salary <span className="open-icon"><i className="fa fa-credit-card-alt" />₦600-₦120</span></li>
                    <li>status <span className="varify"><i className="fa fa-check" />full time</span></li>
                  </ul>
                  <div className="top-job-company-btn">
                  <Link to="/applyjobs" className="jobguru-btn">Bid now</Link>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-md-12 col-lg-4">
              <div className="sigle-top-job shadow">
                <div className="top-job-company-image">
                  <div className="company-logo-img">
                    <a href="#">
                      <img src={Company1} alt="company image" />
                    </a>
                  </div>
                  <h3><a href="#">Graphics Designer</a></h3>
                </div>
                <div className="top-job-company-desc">
                  <ul>
                    <li>Location <span className="company-state"><i className="fa fa-map-marker" /> V.I</span></li>
                    <li>salary <span className="open-icon"><i className="fa fa-credit-card-alt" />₦600-₦120</span></li>
                    <li>status <span className="varify"><i className="fa fa-check" />part time</span></li>
                  </ul>
                  <div className="top-job-company-btn">
                    <a href="#" className="jobguru-btn">Bid now</a>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-md-12 col-lg-4">
              <div className="sigle-top-job shadow">
                <div className="top-job-company-image">
                  <div className="company-logo-img">
                    <a href="#">
                      <img src={Company3} alt="company image" />
                    </a>
                  </div>
                  <h3><a href="#">Content Writer</a></h3>
                </div>
                <div className="top-job-company-desc">
                  <ul>
                    <li>Location <span className="company-state"><i className="fa fa-map-marker" /> Lekki</span></li>
                    <li>salary <span className="open-icon"><i className="fa fa-credit-card-alt" />₦600-₦100</span></li>
                    <li>status <span className="varify"><i className="fa fa-check" />part time</span></li>
                  </ul>
                  <div className="top-job-company-btn">
                    <a href="#" className="jobguru-btn">Bid now</a>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-md-12 col-lg-4">
              <div className="sigle-top-job shadow">
                <div className="top-job-company-image">
                  <div className="company-logo-img">
                    <a href="#">
                      <img src={Company4} alt="company image" />
                    </a>
                  </div>
                  <h3><a href="#">Illustrator</a></h3>
                </div>
                <div className="top-job-company-desc">
                  <ul>
                    <li>Location <span className="company-state"><i className="fa fa-map-marker" /> V.I</span></li>
                    <li>salary <span className="open-icon"><i className="fa fa-credit-card-alt" />₦600-₦120</span></li>
                    <li>status <span className="varify"><i className="fa fa-check" />part time</span></li>
                  </ul>
                  <div className="top-job-company-btn">
                    <a href="#" className="jobguru-btn">Bid now</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-md-12 col-lg-4">
              <div className="sigle-top-job shadow">
                <div className="top-job-company-image">
                  <div className="company-logo-img">
                    <a href="#">
                      <img src={Company1} alt="company image" />
                    </a>
                  </div>
                  <h3><a href="#">2D Animator</a></h3>
                </div>
                <div className="top-job-company-desc">
                  <ul>
                    <li>Location <span className="company-state"><i className="fa fa-map-marker" /> Ikoyi</span></li>
                    <li>salary <span className="open-icon"><i className="fa fa-credit-card-alt" />₦600-₦120</span></li>
                    <li>status <span className="varify"><i className="fa fa-check" />part time</span></li>
                  </ul>
                  <div className="top-job-company-btn">
                    <a href="#" className="jobguru-btn">Bid now</a>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-md-12 col-lg-4">
              <div className="sigle-top-job shadow">
                <div className="top-job-company-image">
                  <div className="company-logo-img">
                    <a href="#">
                      <img src={Company2} alt="company image" />
                    </a>
                  </div>
                  <h3><a href="#">Illustrator</a></h3>
                </div>
                <div className="top-job-company-desc">
                  <ul>
                    <li>Location <span className="company-state"><i className="fa fa-map-marker" />Chisco</span></li>
                    <li>salary <span className="open-icon"><i className="fa fa-credit-card-alt" />₦600-₦100</span></li>
                    <li>status <span className="varify"><i className="fa fa-check" />part time</span></li>
                  </ul>
                  <div className="top-job-company-btn">
                    <a href="#" className="jobguru-btn">Bid now</a>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-md-12 col-lg-4">
              <div className="sigle-top-job shadow">
                <div className="top-job-company-image">
                  <div className="company-logo-img">
                    <a href="#">
                      <img src={Company3} alt="company image" />
                    </a>
                  </div>
                  <h3><a href="#">Administrative Assistant</a></h3>
                </div>
                <div className="top-job-company-desc">
                  <ul>
                    <li>Location <span className="company-state"><i className="fa fa-map-marker" /> Brisbane</span></li>
                    <li>salary <span className="open-icon"><i className="fa fa-credit-card-alt" />₦600-₦120</span></li>
                    <li>status <span className="varify"><i className="fa fa-check" />part time</span></li>
                  </ul>
                  <div className="top-job-company-btn">
                    <a href="#" className="jobguru-btn">Bid now</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-md-12">
              <div className="pagination-box-row">
                <p>Page 1 of 6</p>
                <ul className="pagination">
                  <li className="active"><a href="#">1</a></li>
                  <li><a href="#">2</a></li>
                  <li><a href="#">3</a></li>
                  <li>...</li>
                  <li><a href="#">6</a></li>
                  <li><a href="#"><i className="fa fa-angle-double-right" /></a></li>
                </ul>
              </div>
            </div>


            <div className="col-md-3">
                
            </div>
      
            <div className="col-md-3">
              
            </div>
      
            <div className="col-md-3">
              
            </div>
      
            <div className="col-md-3">
              
            </div>

          </div>
        </div>
      </section>
      {/* Top Job Area End */}

      
        
      </Layout>
    )
  }
}

export default BrowseJob;
