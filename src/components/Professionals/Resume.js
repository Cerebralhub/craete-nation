import React from 'react';
import Layout from '../Layouts/Layout';
import Banner from '../Layouts/Banner';

// Images
import ResumeAvatar from '../../assets/img/resume-avatar.jpg';

class Resume extends React.Component{
    render(){
        return(
            <Layout>
                <Banner />
                <section className="jobguru-submit-resume-area section_70">
                    <div className="container">
                        <div className="row">
                        <div className="col-md-12">
                            <div className="submit-resume-box">
                                <form>
                                    <div className="resume-box">
                                    <div className="single-resume-feild resume-avatar">
                                        <div className="resume-image">
                                            <img src={ResumeAvatar} alt="resume avatar" />
                                            <div className="resume-avatar-hover">
                                                <div className="resume-avatar-upload">
                                                <p>
                                                    <i className="fa fa-upload"></i>
                                                    upload
                                                </p>
                                                <input type="file" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <h3>Personal Information</h3>
                                    <div className="single-resume-feild feild-flex-2">
                                        <div className="single-input">
                                            <label for="name">Your Name:</label>
                                            <input type="text" placeholder="Your Full Name" id="name" />
                                        </div>
                                        <div className="single-input">
                                            <label for="p_title">Professional title:</label>
                                            <input type="text" placeholder="e.g Web Developer" id="p_title" />
                                        </div>
                                    </div>
                                    <div className="single-resume-feild feild-flex-2">
                                        <div className="single-input">
                                            <label for="Email">Email:</label>
                                            <input type="email" placeholder="Your Email Address" id="Email" />
                                        </div>
                                        <div className="single-input">
                                            <label for="Phone">Phone:</label>
                                            <input type="tel" placeholder="Phone Number" id="Phone" />
                                        </div>
                                    </div>
                                    <div className="single-resume-feild feild-flex-2">
                                        <div className="single-input">
                                            <label for="Region">Country:</label>
                                            <select id="Region">
                                                <option selected>select Country</option>
                                                <option>Arab Amirats</option>
                                                <option>America</option>
                                                <option>Netherlands</option>
                                                <option>Russia</option>
                                                <option>Bangladesh</option>
                                                <option>India</option>
                                                <option>Pakistan</option>
                                                <option>Brazil</option>
                                                <option>Africa</option>
                                            </select>
                                        </div>
                                        <div className="single-input">
                                            <label for="Address">Address:</label>
                                            <input type="text" placeholder="Street Address" id="Address" />
                                        </div>
                                    </div>
                                    <div className="single-resume-feild ">
                                        <div className="single-input">
                                            <label for="Bio">Introduce Yourself:</label>
                                            <textarea id="Bio" placeholder="Write Your Bio Here..."></textarea>
                                        </div>
                                    </div>
                                    </div>
                                    <div className="resume-box">
                                    <h3>Education</h3>
                                    <div className="single-resume-feild ">
                                        <div className="single-input">
                                            <label for="Degree">Degree:</label>
                                            <input type="text" placeholder="Degree's Title" id="Degree" />
                                        </div>
                                    </div>
                                    <div className="single-resume-feild feild-flex-2">
                                        <div className="single-input">
                                            <label for="datepicker_form">From:</label>
                                            <input type="text" placeholder="02.10.2015" id="datepicker_form" className="datepicker" />
                                        </div>
                                        <div className="single-input">
                                            <label for="datepicker_to">To:</label>
                                            <input type="text" placeholder="06.11.2017" id="datepicker_to" className="datepicker" />
                                        </div>
                                    </div>
                                    <div className="single-resume-feild ">
                                        <div className="single-input">
                                            <label for="Institute"> Institute:</label>
                                            <input type="text" placeholder="Institution Name" id="Institute" />
                                        </div>
                                    </div>
                                    <div className="single-resume-feild ">
                                        <div className="single-input">
                                            <label for="edu_info">Additional Information: <span>(optional)</span></label>
                                            <textarea id="edu_info" placeholder="Education Experience Brief"></textarea>
                                        </div>
                                    </div>
                                    </div>
                                    <div className="resume-box">
                                    <h3>Work Experience</h3>
                                    <div className="single-resume-feild ">
                                        <div className="single-input">
                                            <label for="j_post">Job Post:</label>
                                            <input type="text" placeholder="Job Post Title" id="j_post" />
                                        </div>
                                    </div>
                                    <div className="single-resume-feild feild-flex-2">
                                        <div className="single-input">
                                            <label for="datepicker_form2">From:</label>
                                            <input type="text" placeholder="02-10-2015" id="datepicker_form2" className="datepicker" />
                                        </div>
                                        <div className="single-input">
                                            <label for="datepicker_to2">To:</label>
                                            <input type="text" placeholder="06-11-2017" id="datepicker_to2" className="datepicker" />
                                        </div>
                                    </div>
                                    <div className="single-resume-feild ">
                                        <div className="single-input">
                                            <label for="Company"> Company:</label>
                                            <input type="text" placeholder="Company Name" id="Company" />
                                        </div>
                                    </div>
                                    <div className="single-resume-feild ">
                                        <div className="single-input">
                                            <label for="work_info">Additional Information: <span>(optional)</span></label>
                                            <textarea id="work_info" placeholder="Work Experience Brief"></textarea>
                                        </div>
                                    </div>
                                    </div>
                                    <div className="resume-box">
                                    <h3>Skills & Portfolio</h3>
                                    <div className="single-resume-feild ">
                                        <div className="single-input">
                                            <label for="skill">Skills:</label>
                                            <input type="text" placeholder="Comma separate a list of relevant skills" id="skill" />
                                        </div>
                                    </div>
                                    <div className="single-resume-feild feild-flex-2">
                                        <div className="single-input">
                                            <label for="Portfolio">Portfolio:</label>
                                            <input type="text" placeholder="Portfolio Demo URL" id="Portfolio" />
                                        </div>
                                        <div className="single-input">
                                            <label for="w_screen">Works Screenshot</label>
                                            <div className="product-upload">
                                                <p>
                                                <i className="fa fa-upload"></i>
                                                Max file size is 3MB,Suitable files are .jpg & .png
                                                </p>
                                                <input type="file" id="w_screen" />
                                            </div>
                                        </div>
                                    </div>
                                    </div>
                                    <div className="submit-resume">
                                    <button type="submit">Save Resume</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                        </div>
                    </div>
                </section>
            </Layout>
        )
    }
}

export default Resume