import React from 'react';
import { Link } from 'react-router-dom';
import Layout from '../Layouts/Layout';
import Banner from '../Layouts/Banner';
import Sidebar from '../Layouts/Sidebar';
import axiosInterceptors from '../helpers/axiosInterceptors';

import swal from 'sweetalert';

class Managejobs extends React.Component{
    constructor(){
        super()
    }
    render(){
        return(
            <Layout>
                
                {/* <Banner name="Manage Jobs"/> */}
                <div className="mt-5"></div>

                <section className="candidate-dashboard-area section_70">
                    <div className="container">
                        <div className="row">
                
                        <Sidebar/>

                            <div className="col-lg-9 col-md-12">
                            <div className="dashboard-right">
                                <div className="manage-jobs shadow">
                                    <div className="manage-jobs-heading">
                                    <h3>My Job Listing</h3>
                                    </div>
                                    <div className="single-manage-jobs table-responsive">
                                    <table className="table">
                                        <thead>
                                            <tr>
                                                <th>Title</th>
                                                <th>Posted on </th>
                                                <th>Expiring on </th>
                                                <th>Status</th>
                                                <th>Profile</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td className="manage-jobs-title"><a href="#">2D Animator</a></td>
                                                <td className="table-date">28 June, 2019</td>
                                                <td className="table-date">10 July, 2019</td>
                                                <td><span className="pending">Pending Approval</span></td>
                                                <td><div className="candidate-action"><Link to="/companydetails" className="jobguru-btn-blue" style={{color:"white"}}>View</Link></div></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    {/* <div className="pagination-box-row">
                                        <p>Page 1 of 6</p>
                                        <ul className="pagination">
                                            <li className="active"><a href="#">1</a></li>
                                            <li><a href="#">2</a></li>
                                            <li><a href="#">3</a></li>
                                            <li>...</li>
                                            <li><a href="#">6</a></li>
                                            <li><a href="#"><i className="fa fa-angle-double-right"></i></a></li>
                                        </ul>
                                    </div> */}
                                    </div>
                                </div>
                            </div>
                        </div>
                        </div>
                    </div>
                </section>
            </Layout>
        )
    }

}

export default Managejobs;