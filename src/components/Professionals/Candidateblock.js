import React from 'react';
import { Link } from 'react-router-dom';

import Dashboardcontent from './../Professionals/Dashboardcontent';

import axiosInterceptors from '../helpers/axiosInterceptors';

import axios from 'axios';

import LogoutFunc from '../helpers/logout'

import Sidebar from '../Layouts/Sidebar'

class Candidateblock extends React.Component{

    constructor(props) {
        super(props)
    
        this.state = {
          firstName: '',
          lastName: '',
          email: '',
          userType: ''
        }
      }
    
      getUserInfo() {
        axiosInterceptors().get('/users', {
          params: {
            'token': localStorage.getItem('token')
          }
        })
        .then((response) => {
          this.setState({
            firstName: response.data.user.firstName,
            lastName: response.data.user.lastName,
            email: response.data.user.email,
            userType: response.data.user.usersTypeId
          })

          console.log('Output: ' + this.state.firstName + this.state.lastName + this.state.email + 'id: ' + this.state.userType);
        })
      }
    
      componentDidMount() {
        this.getUserInfo();
      }

      logout = () => {
        LogoutFunc()
      }

    render(){
        return(
            <div>
              <section className="candidate-dashboard-area section_70">
                <div className="container">
                  <div className="row">
                      <Sidebar anonymous={this.state.userType} />
                      <Dashboardcontent firstName={this.state.firstName} lastName={this.state.lastName}/>
                    </div>
                  </div>
              </section>
            </div>
        );
    }
}

export default Candidateblock;