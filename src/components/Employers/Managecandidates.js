import React, { Component } from "react";
import Layout from "../Layouts/Layout";
import { Link } from "react-router-dom";
import Sidebar from "../Layouts/Sidebar";
import Avatar from 'react-avatar';
import Userphoto from "../../assets/img/msg-2.png";

class ManageCandidates extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <Layout>
        <div className="mt-5"></div>
        <section className="candidate-dashboard-area section_70">
          <div className="container">
            <div className="row">
              <Sidebar />
              <div className="col-md-12 col-lg-9">
                <div className="dashboard-right">
                  <div className="manage-jobs manage-candidates shadow">
                    <div className="manage-jobs-heading">
                      <h3>Manage Candidates</h3>
                    </div>
                  </div>
                  <div className="candidate-list-page">
                    <div className="single-candidate-list shadow">
                      <div className="main-comment">
                        <div className="candidate-image">
                        <Avatar name="Sharon Abraham" size="70" round="50px" color="Grey"/>
                        </div>
                        <div className="candidate-text">
                          <div className="candidate-info">
                            <div className="candidate-title">
                              <h3>
                                <a href="#">Sharon Abraham</a>
                              </h3>
                            </div>
                            <p>Illustrator</p>
                          </div>
                          <div className="candidate-text-inner">
                            <p className="rating-company">4.9</p>
                            <ul>
                              <li>
                                <i className="fa fa-star"></i>
                              </li>
                              <li>
                                <i className="fa fa-star"></i>
                              </li>
                              <li>
                                <i className="fa fa-star"></i>
                              </li>
                              <li>
                                <i className="fa fa-star"></i>
                              </li>
                              <li>
                                <i className="fa fa-star-half-o"></i>
                              </li>
                            </ul>
                          </div>
                          <div className="candidate-text-bottom">
                            <div className="candidate-text-box">
                              <p className="open-icon">
                                <i className="fa fa-thumbs-up"></i> 100% job
                                success
                              </p>
                              <p className="company-state">
                                <i className="fa fa-map-marker"></i> Lagos
                              </p>
                              {/* <p className="varify"><i className="fa fa-check"></i> $50 / hr</p> */}
                            </div>
                            <div className="candidate-action">
                              <Link to="#" className="jobguru-btn-2 mr-1">
                                Approve
                              </Link>
                              <Link
                                to="/candidatesdetails"
                                className="jobguru-btn-danger"
                              >
                                View Profile
                              </Link>
                            </div>
                          </div>
                          <div className="remove-icon">
                            <a href="#">
                              <i className="fa fa-times"></i>
                            </a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </Layout>
    );
  }
}

export default ManageCandidates;
