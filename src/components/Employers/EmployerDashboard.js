import React, { Component } from 'react'
import Layout from '../Layouts/Layout';
import { Link } from 'react-router-dom';

import axiosInterceptors from '../helpers/axiosInterceptors';
import Avatar from 'react-avatar';

import Sidebar from '../Layouts/Sidebar'

 class EmployerDashboard extends Component {

  constructor(props) {
    super(props)

    this.state = {
      firstName: '',
      lastName: '',
      email: '',
      userType: ''
    }
  }

  getUserInfo() {
    axiosInterceptors().get('/users', {
      params: {
        'token': localStorage.getItem('token')
      }
    })
    .then((response) => {
      this.setState({
        firstName: response.data.user.firstName,
        lastName: response.data.user.lastName,
        email: response.data.user.email,
        userType: response.data.user.usersTypeId
      })
    })
  }

  componentDidMount() {
    this.getUserInfo();
  }

  render() {
    return (
      <Layout>
        <div className="mt-5"></div>
        {/* Candidate Dashboard Area Start */}
        <section className="candidate-dashboard-area section_70">
          <div className="container">
            <div className="row">
              
            <Sidebar />
              <div className="col-md-12 col-lg-9">
                <div className="dashboard-right">
                  <div className="welcome-dashboard card shadow">
                        <div className="card-body">
                        <Avatar name={this.state.firstName + ' ' + this.state.lastName} round="50px" color="Grey"/>
                           <h3 className="mt-4">Welcome <span>{this.state.firstName} {this.state.lastName}</span></h3>
                           <h6 className="mt-2" style={{fontFamily:"Montserrat, sans-serif" , letterSpacing:"2px", color:"#505050"}}>
                              {this.state.category}
                           </h6>
                           <p className="mt-1">Nigeria</p>
                        </div>
                     </div>
                  <div className="row mt-4">
                    <div className="col-lg-4 col-md-12">
                      <div className="widget_card_page shadow grid_flex widget_bg_blue">
                        <div className="widget-icon">
                          <i className="fa fa-briefcase" />
                        </div>
                        <div className="widget-page-text">
                          <h4>0</h4>
                          <h2>Jobs</h2>
                        </div>
                      </div>
                    </div>
                    <div className="col-lg-4 col-md-12">
                      <div className="widget_card_page shadow grid_flex  widget_bg_purple">
                        <div className="widget-icon">
                          <i className="fa fa-paste" />
                        </div>
                        <div className="widget-page-text">
                          <h4>0</h4>
                          <h2>Applications</h2>
                        </div>
                      </div>
                    </div>
                    <div className="col-lg-4 col-md-12">
                      <div className="widget_card_page shadow grid_flex widget_bg_dark_red">
                        <div className="widget-icon">
                          <i className="fa fa-users" />
                        </div>
                        <div className="widget-page-text">
                          <h4>0</h4>
                          <h2>Views</h2>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        {/* Candidate Dashboard Area End */}
      </Layout>
    )
  }
}

export default EmployerDashboard;
