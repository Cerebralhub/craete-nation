import React, { Component } from 'react'
import Layout from '../Layouts/Layout';
import { Link } from 'react-router-dom';

 class PostJob extends Component {
  render() {
    return (
      <Layout>
        {/* Breadcromb Area Start */}
        <section className="jobguru-breadcromb-area">
          <div className="breadcromb-top section_100">
            <div className="container">
              <div className="row">
                <div className="col-md-12">
                  <div className="breadcromb-box">
                    <h3>Create a Project</h3>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="breadcromb-bottom">
            <div className="container">
              <div className="row">
                <div className="col-md-12">
                  <div className="breadcromb-box-pagin">
                    <ul>
                      <li><Link to="#">home</Link></li>
                      <li><Link to="/EmployerDashboard">Employer</Link></li>
                      <li className="active-breadcromb"><Link to="#">Post a Job</Link></li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        {/* Breadcromb Area End */}
        {/* Candidate Dashboard Area Start */}
        <section className="candidate-dashboard-area section_70">
          <div className="container">
            <div className="row">
              <div className="col-md-12 col-lg-3 dashboard-left-border">
                <div className="dashboard-left">
                  <ul className="dashboard-menu">
                    <li>
                      <Link to="employer-dashboard.html">
                        <i className="fa fa-tachometer" />
                        Dashboard
                      </Link>
                    </li>
                    <li><Link to="candidate-profile.html"><i className="fa fa-users" />My Profile</Link></li>
                    <li><Link to="message.html"><i className="fa fa-envelope-open" />messages</Link></li>
                    <li><Link to="manage-jobs.html"><i className="fa fa-briefcase" />manage jobs/projects</Link></li>
                    <li><Link to="candidate-earnings.html"><i className="fa fa-rocket" />earnings</Link></li>
                    <li><Link to="change-password.html"><i className="fa fa-lock" />change password</Link></li>
                    <li><Link to="#"><i className="fa fa-power-off" />LogOut</Link></li>
                  </ul>
                </div>
              </div>
              <div className="single-input submit-resume col-md-4" id="color">
                <Link to="/createJob"><button>Post Job</button></Link>
              </div>
              <div className="single-input submit-resume col-md-4">
                <Link to="Hire-now.html"><button>Post project</button></Link>
              </div>
            </div>
          </div>
        </section>
        {/* Candidate Dashboard Area End */}
      </Layout>
    )
  }
}

export default PostJob;
