import React, { Component } from 'react'
import Layout from '../Layouts/Layout';
import CompanyImage from '../../assets/img/company_page_logo.jpg';
import CompanyOne from '../../assets/img/company1.png';
import CompanyTwo from '../../assets/img/company2.png';
import Avatar from 'react-avatar';
class Applyjobs extends React.Component{
    constructor(){
        super()
    }
    render(){
        return(
            <Layout>
                <section className="single-candidate-page section_70">
                    <div className="container">
                        <div className="row">
                        <div className="col-md-9 col-lg-6">
                            <div className="single-candidate-box">
                                <div className="single-candidate-img">
                                <Avatar name="Arino Enterprise" size="80" round="50px" color="Grey"/>
                                </div>
                                <div className="single-candidate-box-right">
                                    <h4>3D Animator needed</h4>
                                    <p>Arino Enterprise</p>
                                    <div className="job-details-meta">
                                    <p><i className="fa fa-file-text"></i> Applications 1</p>
                                    <p><i className="fa fa-calendar"></i> July 29, 2017</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-md-3 col-lg-6">
                            <div className="single-candidate-action">
                                <a href="#" className="candidate-contact"><i className="fa fa-star"></i>Save Job</a>
                            </div>
                        </div>
                        </div>
                    </div>
                </section>
                


                <section className="single-candidate-bottom-area section_70">
                    <div className="container">
                        <div className="row">
                        <div className="col-md-8 col-lg-9">
                            <div className="single-candidate-bottom-left">
                                <div className="single-candidate-widget">
                                    <h3>job Description</h3>
                                    <p>Duis ac augue sit amet ex blandit facilisis sit amet ut dui. Nulla pharetra fermentum mollis. Duis in tempor tortor. Suspendisse vitae nisl diam. Proin eu erat vestibulum, suscipit quam et, cursus ante.Ut sodales arcu sagittis metus molestie molestie. Nulla maximus volutpat dui. Etiam luctus lobortis massa in pulvinar. Maecenas nunc odio, </p>
                                    <p>faucibus in malesuada a, dignissim at odio. Aenean eleifend urna.Nulla maximus volutpat dui. Etiam luctus lobortis massa in pulvinar. Maecenas nunc odio, faucibus in malesuada a, dignissim at odio.</p>
                                    <p>Sed egestas, ante et vulputate volutpat, eros pede semper est, vitae luctus metus libero eu augue. Morbi purus libero, faucibus adipiscing, commodo quis, gravida id, est. Sed lectus. Praesent elementum hendrerit tortor. Sed semper lorem at felis. Vestibulum volutpat, lacus a ultrices sagittis, mi neque euismod dui, eu pulvinar nunc sapien</p>
                                    <p>Duis ac augue sit amet ex blandit facilisis sit amet ut dui. Nulla pharetra fermentum mollis. Duis in tempor tortor. Suspendisse vitae nisl diam. Proin eu erat vestibulum, suscipit quam et, cursus ante.Ut sodales arcu sagittis metus molestie molestie. Nulla maximus volutpat dui. Etiam luctus lobortis massa in pulvinar. Maecenas nunc odio, </p>
                                    <p>faucibus in malesuada a, dignissim at odio. Aenean eleifend urna.Nulla maximus volutpat dui. Etiam luctus lobortis massa in pulvinar. Maecenas nunc odio, faucibus in malesuada a, dignissim at odio.</p>
                                </div>
                                <div className="single-candidate-widget job-required">
                                    <h3>Skills, and Abilities</h3>
                                    <ul className="company-desc-list">
                                    <li><i className="fa fa-check"></i> Ability to write code – HTML & CSS (SCSS flavor of SASS preferred when writing CSS)</li>
                                    <li><i className="fa fa-check"></i>Proficient in Photoshop, Illustrator, bonus points for familiarity with Sketch (Sketch is our preferred concepting)</li>
                                    <li><i className="fa fa-check"></i>Cross-browser and platform testing as standard practice</li>
                                    <li><i className="fa fa-check"></i>Experience using Invision a plus</li>
                                    <li><i className="fa fa-check"></i>Experience in video production a plus or, at a minimum, a willingness to learn</li>
                                    </ul>
                                </div>
                                <div className="single-candidate-widget clearfix">
                                    <h3>Challenges & Benifits</h3>
                                    <p>Etiam quis interdum felis, at pellentesque metus. Morbi eget congue lectus. Donec eleifend ultricies urna et euismod. Sed consectetur tellus eget odio aliquet, vel vestibulum tellus sollicitudin. Morbi maximus metus eu eros tincidunt, vitae mollis ante imperdiet. Nulla imperdiet at mauris ut posuere.</p>
                                    <p>Donec accumsan auctor iaculis. Nullam non tortor massa. Proin ligula leo, hendrerit quis tincidunt a, sodales eget ligula. Aenean et est tristique, dictum lorem vel, porttitor urna.</p>
                                    <p>Suspendisse gravida elementum lacus, a malesuada tortor sollicitudin ut. Donec pharetra metus lectus, ut eleifend eros sollicitudin et. Ut at lobortis dolor, eget commodo tortor. Curabitur bibendum consequat neque a tincidunt. In in euismod quam. Proin in egestas eros. Cum sociis </p>
                                </div>
                                <div className="single-candidate-widget clearfix">
                                    <h3>share this post</h3>
                                    <ul className="share-job">
                                    <li><a href="#"><i className="fa fa-facebook"></i></a></li>
                                    <li><a href="#"><i className="fa fa-twitter"></i></a></li>
                                    <li><a href="#"><i className="fa fa-google-plus"></i></a></li>
                                    <li><a href="#"><i className="fa fa-pinterest"></i></a></li>
                                    </ul>
                                </div>
                                <div className="single-candidate-widget">
                                    <h3>Similar Jobs</h3>
                                    <div className="sidebar-list-single">
                                    <div className="top-company-list">
                                        <div className="company-list-logo">
                                            <a href="#">
                                            <img src={CompanyOne} alt="company list 1"/>
                                            </a>
                                        </div>
                                        <div className="company-list-details">
                                            <h3><a href="#">Regional Sales Manager</a></h3>
                                            <p className="company-state"><i className="fa fa-map-marker"></i> Chicago, Michigan</p>
                                            <p className="open-icon"><i className="fa fa-clock-o"></i>2 minutes ago</p>
                                            <p className="varify"><i className="fa fa-check"></i>Fixed price : $1200-$2000</p>
                                            <p className="rating-company">4.1</p>
                                        </div>
                                        <div className="company-list-btn">
                                            <a href="#" className="jobguru-btn">bid now</a>
                                        </div>
                                    </div>
                                    </div>
                                    <div className="sidebar-list-single">
                                    <div className="top-company-list">
                                        <div className="company-list-logo">
                                            <a href="#">
                                            <img src={CompanyTwo} alt="company list 1"/>
                                            </a>
                                        </div>
                                        <div className="company-list-details">
                                            <h3><a href="#">Asst. Teacher</a></h3>
                                            <p className="company-state"><i className="fa fa-map-marker"></i> Chicago, Michigan</p>
                                            <p className="open-icon"><i className="fa fa-clock-o"></i>2 minutes ago</p>
                                            <p className="varify"><i className="fa fa-check"></i>Fixed price : $800-$1200</p>
                                            <p className="rating-company">4.2</p>
                                        </div>
                                        <div className="company-list-btn">
                                            <a href="#" className="jobguru-btn">bid now</a>
                                        </div>
                                    </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-md-4 col-lg-3">
                            <div className="single-candidate-bottom-right">
                                <div className="single-candidate-widget-2">
                                    <a href="#" className="jobguru-btn-2" data-toggle="modal" data-target="#applyModal">
                                    <i className="fa fa-paper-plane-o"></i>
                                    Apply Now
                                    </a>
                                </div>
                                <div class="modal fade" id="applyModal" tabindex="-1" role="dialog" aria-labelledby="applyModalLabel" aria-hidden="true">
                           
                           <form onSubmit={this.handleSubmit}>
                           <div class="modal-dialog modal-dialog-centered" role="document">
                              <div class="modal-content">
                                 <div class="modal-header">
                                 <h5 class="modal-title" id="applyModalLabel">Apply</h5>
                                 <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                 </button>
                                 </div>
                                 <div class="modal-body">
                                    
                                    <div className="resume-box">
                                    <div className="single-resume-feild ">
                                    <div className="single-input">
                                             <label htmlFor="salary">Salary Expectation:</label>
                                             <input type="text" placeholder="150,000"  id="applysal" />
                                    </div>
                                       <div className="single-input">
                                          <label htmlFor="coverletter">Cover letter:</label>
                                          <textarea id="coverletter" placeholder="write cover letter" />
                                       </div>
                                    </div>
                                    </div>                         
                                   
                                 </div>
                                 <div class="modal-footer">
                                 <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                 <button type="button" class="btn" style={{backgroundColor: "#20D6DE", color: "#fff"}}>Apply</button>
                                 </div>
                              </div>
                           </div>
                           </form>
                        </div>
                                <div className="single-candidate-widget-2">
                                    <h3>Job overview</h3>
                                    <ul className="job-overview">
                                    <li>
                                        <h4><i className="fa fa-briefcase"></i> Offered Salary</h4>
                                        <p>N15,000 - N20,000</p>
                                    </li>
                                    <li>
                                        <h4><i className="fa fa-map-marker"></i> Location</h4>
                                        <p>London, United Kingdom</p>
                                    </li>
                                    <li>
                                        <h4><i className="fa fa-thumb-tack"></i> Job Type</h4>
                                        <p>Full Time</p>
                                    </li>
                                    <li>
                                        <h4><i className="fa fa-clock-o"></i> Date Posted</h4>
                                        <p>2 days ago</p>
                                    </li>
                                    </ul>
                                </div>
                                {/* <div className="single-candidate-widget-2">
                                    <h3>Quick Contacts</h3>
                                    <form>
                                    <p>
                                        <input type="text" placeholder="Your Name"/>
                                    </p>
                                    <p>
                                        <input type="email" placeholder="Your Email Address"/>
                                    </p>
                                    <p>
                                        <textarea placeholder="Write here your message"></textarea>
                                    </p>
                                    <p>
                                        <button type="submit">Send Message</button>
                                    </p>
                                    </form>
                                </div> */}
                            </div>
                        </div>
                        </div>
                    </div>
                </section>

            </Layout>
        )
    }
}

export default Applyjobs;