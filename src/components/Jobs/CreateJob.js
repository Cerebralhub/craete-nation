import React, { Component } from 'react'
import Layout from '../Layouts/Layout';
import { Link } from 'react-router-dom'

import Sidebar from '../Layouts/Sidebar'
import getUserType from '../helpers/getUserType'

import Countries from 'countries-api'

 class CreateJob extends Component {

  constructor(props) {
    super(props)

    this.state = {
      userTypeId: '',
      jobTitle: '',
      externalLink: '',
      mnSalary: '',
      mxSalary: '',
      jobDescription: ''
    }
  }

  componentDidMount() {
    this.knowUser()

    // console.log(Countries.findAll())
  }

  knowUser = () => {
    getUserType().then((response) =>{
      this.setState({
        userTypeId: response.data.user.usersTypeId
      })
    })
    .catch((err) => {
      console.log(err)
    })
  }

  render() {
    return (
      <Layout>
         <div className="mt-5"></div>
        {/* Candidate Dashboard Area Start */}
        <section className="candidate-dashboard-area section_70">
          <div className="container">
            <div className="row">

              <Sidebar anonymous={this.state.userTypeId} />

              <div className="col-md-12 col-lg-9">
                <div className="dashboard-right">
                  <div className="earnings-page-box shadow manage-jobs">
                    <div className="manage-jobs-heading">
                      <h3>Post a job</h3>
                    </div>
                    <div className="new-job-submission">
                      <form>
                        <div className="resume-box">
                          <div className="single-resume-feild feild-flex-2">
                            <div className="single-input">
                              <label htmlFor="j_title">Job Title:</label>
                              <input type="text" id="j_title" value={(event) => this.setState({ jobTitle: event.target.value })} />
                            </div>
                            <div className="single-input">
                              <label htmlFor="Location">Location:</label>
                              <input type="text" placeholder="e.g. London" id="Location" value={(event) => this.setState({ jobTitle: event.target.value })} />
                            </div>
                          </div>
                          <div className="single-resume-feild feild-flex-2">
                            <div className="single-input">
                              <label htmlFor="j_reg">Job Region:</label>
                              <select id="j_reg">
                                <option value>Select Region</option>
                                <option value={1}>Lagos</option>
                                <option value={2}>Abuja</option>
                                <option value={3}>Portharcourt</option>
                                <option value={4}>Kogi</option>
                                <option value={5}>lokoja</option>
                              </select>
                            </div>
                            <div className="single-input">
                              <label htmlFor="j_type">Job Type:</label>
                              <select id="j_type">
                                {/*<option value=''>Select Region</option>*/}
                                <option value={1}>Full Time</option>
                                <option value={2}>Part Time</option>
                                <option value={3}>Internship</option>
                              </select>
                            </div>
                          </div>
                          <div className="single-resume-feild feild-flex-2">
                            <div className="single-input">
                              <label htmlFor="j_category">Job Category:</label>
                              <select id="j_category">
                                <option value={122}>Animator</option>
                                <option value={124}>Automotive Jobs</option>
                                <option value={132}>Graphic designer</option>
                                <option value={137}>Digital marketers</option>
                                <option value={172}>Social Media Influencers</option>
                                <option value={173}>Content Writers</option>
                                <option value={145}>Illustrators</option>
                                {/*
                                          <option value="147">Illustration</option>
                                          <option value="150">Logo Design</option>
                                          <option value="168">Video</option>
                                          <option value="140">Education Training</option>
                                          <option value="146">Healthcare</option>
                                          <option value="157">Restaurant / Food Service</option>
                                          <option value="159">Sales / Marketing</option>
                                          <option value="175">Display Advertising</option>
                                          <option value="176">Email Marketing</option>
                                          <option value="177">Lead Generation</option>
                                          <option value="179">Marketing Strategy</option>
                                          <option value="180">Public Relations</option>
                                          <option value="165">Telecommunications</option>
                                          <option value="167">Transportation / Logistics</option>
*/}
                              </select>
                            </div>
                            <div className="single-input">
                              <label htmlFor="External">External "Apply for Job" link : <span>(optional)</span></label>
                              <input type="text" placeholder="http://" id="External" value={(event) => this.setState({ externalLink: event.target.value })} />
                            </div>
                          </div>
                          <div className="single-resume-feild feild-flex-2">
                            <div className="single-input">
                              <label htmlFor="mn_salary">Minimum Salary ($):</label>
                              <input type="text" placeholder="e.g. 20000" id="mn_salary" value={(event) => this.setState({ mnSalary: event.target.value })} />
                            </div>
                            <div className="single-input">
                              <label htmlFor="mx_salary">Maximum Salary ($):</label>
                              <input type="text" placeholder="e.g. 50000" id="mx_salary" value={(event) => this.setState({ mxSalary: event.target.value })} />
                            </div>
                          </div>
                          <div className="single-resume-feild">
                            <div className="single-input">
                              <label htmlFor="j_desc">Job Description:</label>
                              <textarea id="j_desc" value={(event) => this.setState({ jobDescription: event.target.value })} />
                            </div>
                          </div>
                          <div className="single-resume-feild upload_file">
                            <div className="product-upload">
                              <p>
                                <i className="fa fa-upload" />
                                Upload Files
                              </p>
                              <input type="file" id="w_screen" />
                            </div>
                            <p>Images or documents that might be helpful in describing your job</p>
                          </div>
                        </div>
                        <div className="single-input submit-resume">
                          <button type="submit">Post Job</button>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        {/* Candidate Dashboard Area End */}
      </Layout>
    )
  }
}

export default CreateJob;
