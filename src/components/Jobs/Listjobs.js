import React, { Component } from 'react';
import Layout from '../Layouts/Layout';
import { Link } from 'react-router-dom';


// Assets
import '../../assets/css/style.css';
import '../../assets/css/responsive.css'
import '../../assets/css/animate.min.css';
import '../../assets/css/select2.min.css';
import '../../assets/css/perfect-scrollbar.min.css';
import { ComputerLogo } from '../../assets/img/company-logo-4.png'

// Components
import Banner from '../Layouts/Banner';

 class Listjobs extends Component {
  render() {
    return (
      <Layout>
        <Banner name={this.props.name} />
          {/* Breadcromb Area Start */}
        {/*<section className="jobguru-breadcromb-area">
          <div className="breadcromb-top section_100">
            <div className="container">
              <div className="row">
                <div className="col-md-12">
                  <div className="breadcromb-box">
                    <h3>Browse Jobs</h3>
                  </div>
                </div>
              </div>
            </div>
    </div>
          <div className="breadcromb-bottom">
            <div className="container">
              <div className="row">
                <div className="col-md-12">
                  <div className="breadcromb-box-pagin">
                    <ul>
                      <li><Link to="#">home</Link></li>
                      <li><Link to="#">candidates</Link></li>
                      <li className="active-breadcromb"><Link to="#">browse jobs</Link></li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>*/}
        {/* Breadcromb Area End */}
        {/* Top Job Area Start */}
        <section className="jobguru-top-job-area browse-page section_70">
          <div className="container">
            <div className="row">
              <div className="col-md-12">
                <div className="browse-job-head-option">
                  <div className="job-browse-search">
                    <form>
                      <input type="search" placeholder="Search Jobs Here..." />
                      <button type="submit"><i className="fa fa-search" /></button>
                    </form>
                  </div>
                  <div className="job-browse-action">
                    <div className="email-alerts">
                      <input type="checkbox" className="styled" id="b_1" />
                      <label className="styled" htmlFor="b_1">email alerts for this search</label>
                    </div>
                    <div className="dropdown">
                      <button className="btn-dropdown dropdown-toggle" type="button" id="dropdowncur" data-toggle="dropdown" aria-haspopup="true">Short By</button>
                      <ul className="dropdown-menu" aria-labelledby="dropdowncur">
                        <li>Newest</li>
                        <li>Oldest</li>
                        <li>Random</li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
              {/*end col*/}
              <div className="col-md-12">
                <div className="available-count">
                  <h4>3087 jobs are available in <Link to="#">programming &amp; tech</Link> category</h4>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-md-12 col-lg-4">
                <div className="sigle-top-job">
                  <div className="top-job-company-image">
                    <div className="company-logo-img">
                      <Link to="#">
                        <img src={{ComputerLogo}} alt="company-Image" />
                      </Link>
                    </div>
                    <h3><Link to="#">D Developer (Senior) C .Net</Link></h3>
                  </div>
                  <div className="top-job-company-desc">
                    <ul>
                      <li>Location <span className="company-state"><i className="fa fa-map-marker" /> Brisbane</span></li>
                      <li>salary <span className="open-icon"><i className="fa fa-credit-card-alt" />$600-$1200</span></li>
                      <li>status <span className="varify"><i className="fa fa-check" />part time</span></li>
                    </ul>
                    <div className="top-job-company-btn">
                      <Link to="#" className="jobguru-btn-2">Bid now</Link>
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-md-12 col-lg-4">
                <div className="sigle-top-job">
                  <div className="top-job-company-image">
                    <div className="company-logo-img">
                      <Link to="#">
                        <img src="assets/img/company-logo-2.png" alt="company image" />
                      </Link>
                    </div>
                    <h3><Link to="#">Certified civil engineer</Link></h3>
                  </div>
                  <div className="top-job-company-desc">
                    <ul>
                      <li>Location <span className="company-state"><i className="fa fa-map-marker" /> Brisbane</span></li>
                      <li>salary <span className="open-icon"><i className="fa fa-credit-card-alt" />$600-$1200</span></li>
                      <li>status <span className="varify"><i className="fa fa-check" />part time</span></li>
                    </ul>
                    <div className="top-job-company-btn">
                      <Link to="#" className="jobguru-btn-2">Bid now</Link>
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-md-12 col-lg-4">
                <div className="sigle-top-job">
                  <div className="top-job-company-image">
                    <div className="company-logo-img">
                      <Link to="#">
                        <img src="assets/img/company-logo-3.png" alt="company image" />
                      </Link>
                    </div>
                    <h3><Link to="#">Administrative Assistant</Link></h3>
                  </div>
                  <div className="top-job-company-desc">
                    <ul>
                      <li>Location <span className="company-state"><i className="fa fa-map-marker" /> Brisbane</span></li>
                      <li>salary <span className="open-icon"><i className="fa fa-credit-card-alt" />$600-$1200</span></li>
                      <li>status <span className="varify"><i className="fa fa-check" />part time</span></li>
                    </ul>
                    <div className="top-job-company-btn">
                      <Link to="#" className="jobguru-btn-2">Bid now</Link>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-md-12 col-lg-4">
                <div className="sigle-top-job">
                  <div className="top-job-company-image">
                    <div className="company-logo-img">
                      <Link to="#">
                        <img src="assets/img/company-logo-1.png" alt="company image" />
                      </Link>
                    </div>
                    <h3><Link to="#">Regional Sales Manager</Link></h3>
                  </div>
                  <div className="top-job-company-desc">
                    <ul>
                      <li>Location <span className="company-state"><i className="fa fa-map-marker" /> Brisbane</span></li>
                      <li>salary <span className="open-icon"><i className="fa fa-credit-card-alt" />$600-$1200</span></li>
                      <li>status <span className="varify"><i className="fa fa-check" />part time</span></li>
                    </ul>
                    <div className="top-job-company-btn">
                      <Link to="#" className="jobguru-btn-2">Bid now</Link>
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-md-12 col-lg-4">
                <div className="sigle-top-job">
                  <div className="top-job-company-image">
                    <div className="company-logo-img">
                      <Link to="#">
                        <img src="assets/img/company-logo-3.png" alt="company image" />
                      </Link>
                    </div>
                    <h3><Link to="#">Human Resources Consultant</Link></h3>
                  </div>
                  <div className="top-job-company-desc">
                    <ul>
                      <li>Location <span className="company-state"><i className="fa fa-map-marker" /> Brisbane</span></li>
                      <li>salary <span className="open-icon"><i className="fa fa-credit-card-alt" />$600-$1200</span></li>
                      <li>status <span className="varify"><i className="fa fa-check" />part time</span></li>
                    </ul>
                    <div className="top-job-company-btn">
                      <Link to="#" className="jobguru-btn-2">Bid now</Link>
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-md-12 col-lg-4">
                <div className="sigle-top-job">
                  <div className="top-job-company-image">
                    <div className="company-logo-img">
                      <Link to="#">
                        <img src="assets/img/company-logo-4.png" alt="company image" />
                      </Link>
                    </div>
                    <h3><Link to="#">Event Support Specialist</Link></h3>
                  </div>
                  <div className="top-job-company-desc">
                    <ul>
                      <li>Location <span className="company-state"><i className="fa fa-map-marker" /> Brisbane</span></li>
                      <li>salary <span className="open-icon"><i className="fa fa-credit-card-alt" />$600-$1200</span></li>
                      <li>status <span className="varify"><i className="fa fa-check" />part time</span></li>
                    </ul>
                    <div className="top-job-company-btn">
                      <Link to="#" className="jobguru-btn-2">Bid now</Link>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-md-12 col-lg-4">
                <div className="sigle-top-job">
                  <div className="top-job-company-image">
                    <div className="company-logo-img">
                      <Link to="#">
                        <img src="assets/img/company-logo-4.png" alt="company image" />
                      </Link>
                    </div>
                    <h3><Link to="#">C Developer (Senior) C .Net</Link></h3>
                  </div>
                  <div className="top-job-company-desc">
                    <ul>
                      <li>Location <span className="company-state"><i className="fa fa-map-marker" /> Brisbane</span></li>
                      <li>salary <span className="open-icon"><i className="fa fa-credit-card-alt" />$600-$1200</span></li>
                      <li>status <span className="varify"><i className="fa fa-check" />part time</span></li>
                    </ul>
                    <div className="top-job-company-btn">
                      <Link to="#" className="jobguru-btn-2">Bid now</Link>
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-md-12 col-lg-4">
                <div className="sigle-top-job">
                  <div className="top-job-company-image">
                    <div className="company-logo-img">
                      <Link to="#">
                        <img src="assets/img/company-logo-2.png" alt="company image" />
                      </Link>
                    </div>
                    <h3><Link to="#">Certified civil engineer</Link></h3>
                  </div>
                  <div className="top-job-company-desc">
                    <ul>
                      <li>Location <span className="company-state"><i className="fa fa-map-marker" /> Brisbane</span></li>
                      <li>salary <span className="open-icon"><i className="fa fa-credit-card-alt" />$600-$1200</span></li>
                      <li>status <span className="varify"><i className="fa fa-check" />part time</span></li>
                    </ul>
                    <div className="top-job-company-btn">
                      <Link to="#" className="jobguru-btn-2">Bid now</Link>
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-md-12 col-lg-4">
                <div className="sigle-top-job">
                  <div className="top-job-company-image">
                    <div className="company-logo-img">
                      <Link to="#">
                        <img src="assets/img/company-logo-3.png" alt="company image" />
                      </Link>
                    </div>
                    <h3><Link to="#">Administrative Assistant</Link></h3>
                  </div>
                  <div className="top-job-company-desc">
                    <ul>
                      <li>Location <span className="company-state"><i className="fa fa-map-marker" /> Brisbane</span></li>
                      <li>salary <span className="open-icon"><i className="fa fa-credit-card-alt" />$600-$1200</span></li>
                      <li>status <span className="varify"><i className="fa fa-check" />part time</span></li>
                    </ul>
                    <div className="top-job-company-btn">
                      <Link to="#" className="jobguru-btn-2">Bid now</Link>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-md-12">
                <div className="pagination-box-row">
                  <p>Page 1 of 6</p>
                  <ul className="pagination">
                    <li className="active"><Link to="#">1</Link></li>
                    <li><Link to="#">2</Link></li>
                    <li><Link to="#">3</Link></li>
                    <li>...</li>
                    <li><Link to="#">6</Link></li>
                    <li><Link to="#"><i className="fa fa-angle-double-right" /></Link></li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </section>
        {/* Top Job Area End */}
      </Layout>
    )
  }
}

export default Listjobs;
