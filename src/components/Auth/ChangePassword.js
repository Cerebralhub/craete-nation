import React, { Component } from 'react'
import Layout from '../Layouts/Layout'
import { Link } from 'react-router-dom'

import Sidebar from '../Layouts/Sidebar'

export default class ChangePassword extends Component {

  constructor(props) {
    super(props)

    this.state = {
      oldPassword: '',
      password: '',
      confirmPassword: ''
    }
  }

  changePassword = () => {
    console.log('Passwords: ', this.state.oldPassword, this.state.password, this.state.confirmPassword)
  }

  render() {
    return (
      <Layout>
        <div className="mt-5"></div>
        {/* Candidate Dashboard Area Start */}
        <section className="candidate-dashboard-area section_70">
          <div className="container">
            <div className="row">

              <Sidebar />

              <div className="col-lg-9 col-md-12">
                <div className="dashboard-right">
                  <div className="change-pass manage-jobs">
                    <div className="manage-jobs-heading">
                      <h3>Change Password</h3>
                    </div>
                    <form>
                      <p>
                        <label htmlFor="old_pass">Old Password</label>
                        <input type="password" onChange={event => this.setState({oldPassword: event.target.value})} placeholder="*******" id="old_pass" />
                      </p>
                      <p>
                        <label htmlFor="new_pass">New Password</label>
                        <input type="password" onChange={event => this.setState({password: event.target.value})} placeholder="*******" id="new_pass" />
                      </p>
                      <p>
                        <label htmlFor="confirm_pass">confirm Password</label>
                        <input type="password" onChange={event => this.setState({confirmPassword: event.target.value})} placeholder="*******" id="confirm_pass" />
                      </p>
                      <p>
                        <button type="submit" onClick={event => this.changePassword} style={{backgroundColor: "#1BC8D1"}}>Update</button>
                      </p>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        {/* Candidate Dashboard Area End */}
      </Layout>
    )
  }
}
