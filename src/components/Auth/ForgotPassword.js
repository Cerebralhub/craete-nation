import React, { Component } from 'react'
import Layout from '../Layouts/Layout';
import Banner from '../Layouts/Banner';
import { Link } from 'react-router-dom';
import swal from 'sweetalert';
import axios from 'axios';
import axiosInterceptors from '../helpers/axiosInterceptors';

import logo from '../../assets/img/logo.png';

import '../../assets/css/common.css';
import '../../assets/css/theme-03.css';


const emailRegex = RegExp(/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/)
 
const formValid =  ({ formErrors, ...rest }) => {
  let valid = true;

  // validate form errors being empty
  Object.values(formErrors).forEach( val => { 
    val.length > 0 && (valid = false)
  });

  // validate the form was filled out
  Object.values(rest).forEach(val => {
    val === null && (valid = false)
  });

  return valid;
}

class ForgotPassword extends Component {
    constructor(props) {
        super(props);
    
        this.state = {
          email: null,
          error: '',
          success: '',
          formErrors: {
            email: "",
            password: ""
          }
        };
      }
    
      forgotPassword() {
        axiosInterceptors().post('/forgot-password', {
          'email': this.state.email
        })
        .then((response) => {
          // alert(response.data.success);
          swal(response.data.success, "continue", "success");
          console.log(response);
          this.setState({ success: response.data.success });

          // this.props.history.push('/confirmPassword'); 
          //hgfdjh   
        })
        .catch((err) => {
          alert(err.response.data.error);
          swal(err.response.data.error, "Please try again", "error");
          console.log(err.response.data.error);
          this.setState({ error: err.response.data.error })
        })
      }
    
      handleSubmit = e => {
        e.preventDefault();
        
        if (formValid(this.state)) {
          // alert(`
          //    --SUBMITTING--
          //    email: ${this.state.email}
          //    password: ${this.state.password}
          // `);
    
          this.forgotPassword();
        } else {
          // alert("FORM INVALID - DISPLAY ERROR MESSAGE");
          swal("FORM INVALID", "Please fill the form", "warning");
        }
      }; 
    
      handleChange = e => {
        e.preventDefault();
        const { name, value } = e.target;
        let formErrors = this.state.formErrors;
    
        switch (name) {
            case "email":
                formErrors.email =
                emailRegex.test(value)  
                ?""
                : "invalid email address"
            break;
    
            case "password":
               formErrors.password =
               value.length < 6  
                ?"mininum 6 characters required"
               : "";
            break;
            default:
              break;
        }
        this.setState({ formErrors, [name]: value }, () => console.log(this.state));
      };
  render() {
    const { formErrors } = this.state;
    return (
      <div>
         {/* <Banner name={this.props.name} />
         <section className="jobguru-login-area section_70">
        <div className="container">
          <div className="row">
            <div className="col-md-12">
              <div className="login-box">
                <div className="login-title">
                  <h3 style={{marginBottom: '15px'}}>Forgot your Password?</h3>
                  <p style={{textAlign: 'center'}}>Dont worry! Resetting your password is easy, just type in 
                      the email address you resgistered with.
                  </p>
                </div>
                <form onSubmit={this.handleSubmit} noValidate>
                  <div className="single-login-field">
                    <input type="email" placeholder="Email Address"
                     name="email" 
                     noValidate
                     onChange={this.handleChange}
                      />
                    {formErrors.email.length > 0 && (
                       <span className="errorMessage">{formErrors.email}</span>
                    )}
                  </div>
                  <div className="single-login-field">
                    <button type="submit"> SEND</button>
                  </div>
                </form>
                
              </div>
            </div>
          </div>
        </div>
      </section> */}
      {/* Login Area End */}

      <div className="forny-container">
        
<div className="forny-inner">
    <div className="forny-form text-center">
        <div className="mb-8 text-center forny-logo">
            <a href="/"><img src={logo} /></a>
        </div>
        <div className="reset-form d-block">

    <form className="reset-password-form" onSubmit={this.handleSubmit} noValidate>
    <h4 className="mb-5">Reset Your password</h4>
    <p className="mb-10">
        Please enter your email address and we will send you a password password link.
    </p>
    <div className="form-group">
    <div className="input-group">
        <div className="input-group-prepend">
            <span className="input-group-text">
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="16" viewBox="0 0 24 16">
    <g transform="translate(0)">
        <path d="M23.983,101.792a1.3,1.3,0,0,0-1.229-1.347h0l-21.525.032a1.169,1.169,0,0,0-.869.4,1.41,1.41,0,0,0-.359.954L.017,115.1a1.408,1.408,0,0,0,.361.953,1.169,1.169,0,0,0,.868.394h0l21.525-.032A1.3,1.3,0,0,0,24,115.062Zm-2.58,0L12,108.967,2.58,101.824Zm-5.427,8.525,5.577,4.745-19.124.029,5.611-4.774a.719.719,0,0,0,.109-.946.579.579,0,0,0-.862-.12L1.245,114.4,1.23,102.44l10.422,7.9a.57.57,0,0,0,.7,0l10.4-7.934.016,11.986-6.04-5.139a.579.579,0,0,0-.862.12A.719.719,0,0,0,15.977,110.321Z" transform="translate(0 -100.445)"/>
    </g>
</svg>
            </span>
        </div>
        <input required className="form-control" name="email" type="email" placeholder="Email Address" 
        noValidate
        onChange={this.handleChange}/>
        {formErrors.email.length > 0 && (
        <span className="errorMessage">{formErrors.email}</span>
        )}
    </div>
</div> 
    <div className="row">
        <div className="col-md-12">
            <button className="btn  btn-block" style={{backgroundColor: "#20D6DE", color:"white"}}>Send Reset Link</button>
        </div>
    </div>
</form>
</div>
<div className="reset-confirmation d-none">
    <h4 className="mb-5">Link was sent</h4>
<div>
    Please, check your inbox for a password reset link.
</div> 
</div>
    </div>
</div>

    </div>


      </div>
    )
  }
}

export default ForgotPassword;
