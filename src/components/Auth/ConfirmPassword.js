import React, { Component } from 'react'
import Layout from '../Layouts/Layout';
import { Link, Redirect } from 'react-router-dom';
import Banner from '../Layouts/Banner';

// import axios from 'axios';

import axiosInterceptors from '../helpers/axiosInterceptors';

import swal from 'sweetalert';

const formValid =  ({ formErrors, ...rest }) => {
  let valid = true;

  // validate form errors being empty
  Object.values(formErrors).forEach( val => { 
    val.length > 0 && (valid = false)
  });

  // validate the form was filled out
  Object.values(rest).forEach(val => {
    val === null && (valid = false)
  });

  return valid;
}

class ConfirmPassword extends Component {
  constructor(props) {
    super(props);

    this.state = {
      password: '',
      tokenParam: '',
      formErrors: {
        password: "",
        confirm_password: "",
      }
    };

  }

  

  componentDidMount() {

    const search = window.location.search;
    const params = new URLSearchParams(search);
    const tokenParam = params.get('token');

    this.setState({
      tokenParam: tokenParam
    })

    if(tokenParam.length < 64) {
      this.props.history.push('/')
    }
  }

  confirmPassword() {
    axiosInterceptors().post('/reset-password', {
      'password': this.state.password,
      'token': this.state.tokenParam
    })
    .then((response) => {
      // alert(response.data.success);
      swal(response.data.success, "Done", "success");
      console.log(response);
      this.setState({ success: response.data.success });

      this.props.history.push('/');    
    })
    .catch((err) => {
        // alert(err.response.data.error);
        swal(err.response.data.error, "Try again", "error");
        console.log(err.response.data.error);
        this.setState({ error: err.response.data.error })
    })
  }

    
  handleSubmit = e => {
    e.preventDefault();
    
    if (formValid(this.state)) {
      // alert(`
      //    --SUBMITTING--
      //    password: ${this.state.password}
      //    confirm_password: ${this.state.confirm_password}
      // `);

      const { password, confirm_password } = this.state;

      this.confirmPassword();
      
      if (password !== confirm_password) {
          // alert("Passwords don't match");
          swal("Passwords don't match", "Try again", "warning");
      }
      
    } else {
      // alert("FORM INVALID - DISPLAY ERROR MESSAGE");
      swal("FORM INVALID - DISPLAY ERROR MESSAGE", "Try again", "error");
    }
  }; 

  handleChange = e => {
    e.preventDefault();
    const { name, value } = e.target;
    let formErrors = this.state.formErrors;

    switch (name) {
        case "password":
           formErrors.password =
           value.length < 6  
            ?"mininum 6 characters required"
           : "";
        break;

        //  case "cornfirm_password":
        //    formErrors.cornfirm_password =
        //    value.length < 6
        //    ?"mininum 6 characters required"
        //     : "";
        // break;
        default:
          break;
    }
    this.setState({ formErrors, [name]: value }, () => console.log(this.state));
  };
  render() {
    const { formErrors } = this.state;
    return (
      <Layout>
                <Banner name={this.props.name} />
      <section className="jobguru-login-area section_70">
        <div className="container">
          <div className="row">
            <div className="col-md-12">
              <div className="login-box">
                <div className="login-title">
                  <p>New Password</p>
                </div>
                <form onSubmit={this.handleSubmit} noValidate>
                <div className="single-login-field">
                    <input type="password" placeholder="Password" 
                    name="password" 
                    noValidate
                    onChange={this.handleChange}
                    />
                     {formErrors.password.length > 0 && (
                     <span className="errorMessage">{formErrors.password}</span>
                    )}
                  </div>
                  <div className="single-login-field">
                    <input type="password" placeholder="Confirm Password" 
                    name="confirm_password" 
                    noValidate
                    onChange={this.handleChange}
                    />
                     {formErrors.confirm_password.length > 0 && (
                     <span className="errorMessage">{formErrors.confirm_password}</span>
                    )}
                  </div>
            
                  <div className="single-login-field">
                    <button type="submit">Update</button>
                  </div>
                </form>
                
              </div>
            </div>
          </div>
        </div>
      </section>
      </Layout>
    )
  }
}

export default ConfirmPassword;
