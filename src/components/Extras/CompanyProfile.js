import React, { Component } from 'react'
import Layout from '../Layouts/Layout';
import { Link } from 'react-router-dom';

import { ResumeAvatar } from '../../assets/img/resume-avatar.jpg'

import Sidebar from '../Layouts/Sidebar'

import Grabber from '../helpers/Grabber'

 class CompanyProfile extends Component {

  constructor(props) {
    super(props)

    this.state = {
      usersTypeId: '',
      isCategoryLoading: true,
      categories: null,
      category: 1,
    }
  }

  componentDidMount = () => {
    this.getUserTypes()
    this.getCategories()
  }

  getUserTypes = () => {
    Grabber.getUserType().then((response) => {
      console.log(response.data.user)
      this.setState({
        firstName: response.data.user.firstName,
        lastName: response.data.user.lastName,
        phoneNumber: response.data.user.phoneNumber,
        email: response.data.user.email,
        address: response.data.user.address,
        usersTypeId: response.data.user.usersTypeId
      })
    })
    .catch((err) => {
      console.log(err)
    })
  }

  getCategories = () => {
    Grabber.getCategories().then((response) => {
      // console.log(response.data.categories)
      this.setState({
        categories: response.data.categories,
        isCategoryLoading: false
      })
    })
    .catch((err) => {
      console.log(err)
    })
  }

  returnCategorySelected = (value) => {
    if(this.state.category == value) 
      return true
  }

  render() {
    return (
      <Layout>
         {/* Breadcromb Area Start */}
         <div className="mt-5"></div>
        {/* Breadcromb Area End */}
        {/* Candidate Dashboard Area Start */}
        <section className="candidate-dashboard-area section_70">
          <div className="container">
            <div className="row">
              
              <Sidebar anonymous={this.state.usersTypeId}/>
              
              <div className="col-md-12 col-lg-9">
                <div className="dashboard-right">
                  <div className="candidate-profile shadow">
                    
                    <div className="candidate-single-profile-info">
                    <div>
        <ul className="nav nav-tabs md-tabs" id="myTabEx" role="tablist" >
          <li className="nav-item">
            <a className="nav-link active show" id="home-tab-ex" data-toggle="tab" href="#home-ex" role="tab" aria-controls="home-ex" aria-selected="true">Personal Details</a>
          </li>
          <li className="nav-item">
            <a className="nav-link" id="profile-tab-ex" data-toggle="tab" href="#profile-ex" role="tab" aria-controls="profile-ex" aria-selected="false">Company Details</a>
          </li>
          <li className="nav-item">
            <a className="nav-link" id="contact-tab-ex" data-toggle="tab" href="#contact-ex" role="tab" aria-controls="contact-ex" aria-selected="false">Company Page</a>
          </li>
        </ul>
        <div className="tab-content pt-3" id="myTabContentEx">
          <div className="tab-pane fade active show" id="home-ex" role="tabpanel" aria-labelledby="home-tab-ex">
          <form>
                        <div className="resume-box">
                          <h3>Personal Details</h3>
                          <div className="single-resume-feild feild-flex-2">
                            <div className="single-input">
                              <label htmlFor="firstName">First Name:</label>
                              <input type="text" id="firstName" placeholder={this.state.firstName} />
                            </div>
                            <div className="single-input">
                              <label htmlFor="lastName">Last Name:</label>
                              <input type="text" id="lastName" placeholder={this.state.lastName} />
                            </div>
                          </div>
                          <div className="single-resume-feild feild-flex-2">
                            <div className="single-input">
                              <label htmlFor="Phone">Phone:</label>
                              <input type="text" id="Phone" placeholder={this.state.phoneNumber} />
                            </div>
                            <div className="single-input">
                              <label htmlFor="Email">Email:</label>
                              <input type="text" id="Email" placeholder={this.state.email} />
                            </div>
                          </div>
                          
                          <div className="single-resume-feild feild-flex-2">
                           
                            <div className="single-input">
                              <label htmlFor="Address22">Address:</label>
                              <input type="text" placeholder={this.state.address} id="Address22" />
                            </div>
                          </div>
                        </div>
                        <div className="submit-resume">
                          <button type="submit">Update</button>
                        </div>
                      </form>
          </div>
          <div className="tab-pane fade" id="profile-ex" role="tabpanel" aria-labelledby="profile-tab-ex">
          <form>
                        <div className="resume-box">
                          <h3>Company Details</h3>
                          <div className="single-resume-feild feild-flex-2">
                            <div className="single-input">
                              <label htmlFor="Company Name">Company Name:</label>
                              <input type="text" id="company" placeholder="enter company name" />
                            </div>
                            <div className="single-input">
                              <label htmlFor="Industry Name">Industry Name:</label>
                              <select id="industry">
                                <option disabled>select category</option>
                                <option  value="it">IT/Telecoms</option>
                                <option  value="agriculture">Agriculture</option>
                                <option value="construction">Construction</option>
                                <option value="education">Education</option>
                                <option value="banking">Banking & Finance</option>
                                <option value="energy">Energy & Utilities</option>
                                <option value="health">Health</option>
                                <option value="Hospital">Hospital & Hotel</option>
                                <option value="recruitment">Recruitment</option>
                                <option value="law">Law & Compliance</option> 
                                <option value="shipping">Shipping & Logistics</option>
                                <option value="Manufacturing">Manufacturing & Warehousing</option> 
                                <option value="advertising">Advertising, Media & Communications</option>
                                <option value="mining">Mining, Energy & Metals</option>
                                <option value="retail">Retail & Fashion</option>
                                <option value="realestate">Real Estate</option>                                   
                              </select>
                            </div>
                          </div>
                          <div className="single-resume-feild feild-flex-2">
                            <div className="single-input">
                              <label htmlFor="Number of employees">Number of employees:</label>
                              <select id="noOfEmployees">
                                <option disabled>select category</option>
                                <option  value="1-4">1-4</option>
                                <option  value="5-10">5-10</option>
                                <option value="11-25">11-25</option>
                                <option value="26-50">26-50</option>
                                <option value="51-100">51-100</option>
                                <option value="101-200">101-200</option>
                                <option value="200-500">200-500</option>
                                <option value="500-1000">500-1000</option>
                                
                              </select>
                            </div>
                            <div className="single-input">
                              <label htmlFor="Type of employer">Type of employer:</label>
                              <select id="industry">
                                <option disabled>select category</option>
                                <option  value="directEmployer">Direct Employer</option>
                                <option  value="recruitmentAgency">Recruitment Agency</option>
                                
                              </select>
                            </div>
                          </div>
                          <div className="single-resume-feild feild-flex-2">
                            <div className="single-input">
                              <label htmlFor="website">Website:</label>
                              <input type="text" id="website" placeholder="https://" />
                            </div>
                            <div className="single-input">
                              <label htmlFor="Country">Country:</label>
                              <input type="text" id="country" placeholder={this.state.lastName} />
                            </div>
                          </div>
                          <div className="single-resume-feild feild-flex-2">
                            <div className="single-input">
                              <label htmlFor="Phone">Phone:</label>
                              <input type="text" id="Phone" placeholder={this.state.phoneNumber} />
                            </div>
                            <div className="single-input">
                              <label htmlFor="Email">Email For Notifications:</label>
                              <input type="text" id="Email" placeholder={this.state.email} />
                            </div>
                          </div>
                          
                          <div className="single-resume-feild feild-flex-2">
                           
                            <div className="single-input">
                              <label htmlFor="Address22">Address:</label>
                              <input type="text" placeholder={this.state.address} id="Address22" />
                            </div>
                          </div>
                        </div>
                        <div className="submit-resume">
                          <button type="submit">Save</button>
                        </div>
                      </form>
          </div>
          <div className="tab-pane fade" id="contact-ex" role="tabpanel" aria-labelledby="contact-tab-ex">
                  <form>
                        <div className="resume-box">
                          <h3>My Company Page</h3>
                            <p className="">
                            Create your employer page on Squarework to cover all critical elements of employer branding;
                            A list of all your open positions, your company name and a short blurb to let potential job 
                            seekers know who you are as a company and why should they work for you.
                            </p>
                          <div className="single-resume-feild feild-flex-2 mt-4">
                            <div className="single-input">
                              <label htmlFor="headquarters">Head Quarters:</label>
                              <input type="text" id="headquarters" placeholder="enter head quarters" />
                            </div>
                            <div className="single-input">
                              <label htmlFor="year">Year Founded:</label>
                              <input type="text" id="year" placeholder="year" />
                            </div>
                          </div>
                          <div className="single-resume-feild feild-flex-2">
                            <div className="single-input">
                              <label htmlFor="office">Office Location:</label>
                              <input type="text" id="office" placeholder="enter office location" />
                            </div>
                            <div className="single-input">
                              <label htmlFor="Email">Email:</label>
                              <input type="text" id="Email" placeholder={this.state.email} />
                            </div>
                          </div>
                          
                          <div className="single-resume-feild feild-flex-2">
                           
                            <div className="single-input">
                              <label htmlFor="about">About us:</label>
                              <input type="text" placeholder="write something about your company" id="aboutus" />
                            </div>
                          </div>
                        </div>
                        <div className="submit-resume">
                          <button type="submit">Save</button>
                        </div>
                      </form>
          </div>
        </div>
      </div>
                     
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        {/* Candidate Dashboard Area End */}
      </Layout>
    )
  }
}

export default CompanyProfile;
