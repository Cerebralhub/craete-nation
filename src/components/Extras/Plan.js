import React from 'react';
import { Link } from 'react-router-dom'



class Plan extends React.Component{
    render(){
        return(
            <section className="jobguru-pricing-area section_70">
            <div className="container">
                <div className="row">
                <div className="col-md-12">
                    <div className="site-heading">
                        <h2>choose best <span>plan</span></h2>
                        <p>A better career is out there. We'll help you find it. We're your first step to becoming everything you want to be.</p>
                    </div>
                </div>
                </div>
                <div className="row">
                <div className="col-md-4 no-pad-right">
                    <div className="single-pricing price-basic">
                        <div className="price-heading">
                            <h3>basic</h3>
                        </div>
                        <div className="price-value">
                            <h2><i className="fa fa-dollar"></i> 35</h2>
                            <p>per month</p>
                        </div>
                        <div className="price-btn">
                            <Link to="#" className="jobguru-btn-2">sign up</Link>
                        </div>
                        <div className="price-body">
                            <p>Features of Basic Plan</p>
                            <p>1 Listing</p>
                            <p>30 Days Visibility</p>
                            <p>10 bids per day</p>
                            <p>Highlighted in Search Results</p>
                        </div>
                    </div>
                </div>
                <div className="col-md-4 no-pad-all">
                    <div className="single-pricing price-standard">
                        <div className="price-heading">
                            <h3>basic</h3>
                        </div>
                        <div className="price-value">
                            <h2><i className="fa fa-dollar"></i> 35</h2>
                            <p>per month</p>
                        </div>
                        <div className="price-btn">
                            <Link to="#" className="jobguru-btn-2">sign up</Link>
                        </div>
                        <div className="price-body">
                            <p>Features of Basic Plan</p>
                            <p>1 Listing</p>
                            <p>30 Days Visibility</p>
                            <p>10 bids per day</p>
                            <p>Highlighted in Search Results</p>
                        </div>
                    </div>
                </div>
                <div className="col-md-4 no-pad-left">
                    <div className="single-pricing price-premium">
                        <div className="price-heading">
                            <h3>basic</h3>
                        </div>
                        <div className="price-value">
                            <h2><i className="fa fa-dollar"></i> 35</h2>
                            <p>per month</p>
                        </div>
                        <div className="price-btn">
                            <Link to="#" className="jobguru-btn-2">sign up</Link>
                        </div>
                        <div className="price-body">
                            <p>Features of Basic Plan</p>
                            <p>1 Listing</p>
                            <p>30 Days Visibility</p>
                            <p>10 bids per day</p>
                            <p>Highlighted in Search Results</p>
                        </div>
                    </div>
                </div>
                </div>
            </div>
      </section>
        );
    }
}

export default Plan