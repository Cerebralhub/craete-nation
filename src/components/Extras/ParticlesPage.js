import React, { Component } from 'react';
import Particles from 'react-particles-js';
import { Link } from 'react-router-dom'

const ParticleOpt = {
    particles: {
        number: {
            value: 50,
            density: {
                enable: true,
                value_area: 600
            }
        }
        
    }
}

 class ParticlesPage extends Component {
  render() {
    return (
      <div>
        <div className="container">
          <h1 style={{position: 'absolute',top: '30%',left: '30%',color: '#fff'}}>Are you a world-class dev?</h1>
          <p style={{position: 'absolute',top: '40%',left: '40%',color: '#fff'}}>Get a job at top tech companies!</p>
           <ul>
           <li><Link to="/postjob" className="post-jobs" style={{position: 'absolute',top: '50%',left: '42%', color: '#fff',
    padding: '10px 30px',
    fontSize: '18px' ,
    fontWeight: '400',
    textShadow: '0 2px rgba(0,0,0,.1)',
    background: '#28a745',
    transition: 'all .45s',
    borderRadius: '3px',
    boxSshadow: '-3px -3px 14px -1px rgba(0,0,0,.08), 3px 3px 14px 1px rgba(0,0,0,.18), 2px 4px 18px 3px rgba(0,0,0,.019)'}}> Start a project</Link></li>
           </ul>
        </div>
              <Particles 
              params={ParticleOpt}
             style={{ background: '#09792e'}}/>
            
      </div>
    );
  }
}

export default ParticlesPage;


