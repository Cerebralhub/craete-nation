import React from 'react';
import Layout from '../Layouts/Layout';

// Assets
import Banner from '../Layouts/Banner';

class Contact extends React.Component{
    constructor(props){
        super(props);

    }
    render(){
        return(
            <Layout>
                <Banner name="Contact" />
                {/* Body Starts here */}
                <section class="jobguru-contact-page-area section_70">
                    <div class="container">
                        <div class="row">
                        <div class="col-md-5">
                            <div class="contact-left">
                                <h3>Contact information</h3>
                                <div class="contact-details">
                                    <p><i class="fa fa-map-marker"></i>47, Oba Yekini Street, Ikate, Elegushi, Lekki, Lagos, Nigeria. </p>
                                    <div class="single-contact-btn">
                                    <h4>Email Us</h4>
                                    <a href="#" class="jobguru-btn-2">support@squarework.com</a>
                                    </div>
                                    <div class="single-contact-btn">
                                    <h4>Call Us</h4>
                                    <a href="#" class="jobguru-btn-2">+(09)-2134-76894-9</a>
                                    </div>
                                    <div class="social-links-contact">
                                    <h4>Follow Us:</h4>
                                    <ul>
                                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                        <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                        <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                    </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-7">
                            <div class="contact-right">
                                <h3>Feel free to contact us!</h3>
                                <form>
                                    <div class="row">
                                    <div class="col-md-6">
                                        <div class="single-contact-field">
                                            <input type="text" placeholder="Your Name"/>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="single-contact-field">
                                            <input type="email" placeholder="Email Address"/>
                                        </div>
                                    </div>
                                    </div>
                                    <div class="row">
                                    <div class="col-md-12">
                                        <div class="single-contact-field">
                                            <input type="text" placeholder="Subject"/>
                                        </div>
                                    </div>
                                    </div>
                                    <div class="row">
                                    <div class="col-md-12">
                                        <div class="single-contact-field">
                                            <textarea placeholder="Write here your message"></textarea>
                                        </div>
                                    </div>
                                    </div>
                                    <div class="row">
                                    <div class="col-md-12">
                                        <div class="single-contact-field">
                                            <button type="submit"><i class="fa fa-paper-plane"></i> Send Message</button>
                                        </div>
                                    </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        </div>
                    </div>
                </section>
            </Layout>
        )
    }
}

export default Contact