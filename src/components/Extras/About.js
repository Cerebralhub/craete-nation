import React from 'react';
import Layout from '../Layouts/Layout';
import Planbill from './Plan';
import Banner from '../Layouts/Banner';
import { Link } from 'react-router-dom';

// Assets
import '../../assets/css/style.css';
import '../../assets/css/responsive.css'
import '../../assets/css/animate.min.css';
import '../../assets/css/select2.min.css';
import '../../assets/css/perfect-scrollbar.min.css';

class About extends React.Component{
    render(){
        return(
            <Layout>
                <Banner name="about" />
            {/* Body Content Starts Here */}
                <section className="jobguru-about-page section_70_about">
                    <div className="container">
                        <div className="row">
                        <div className="col-md-12">
                            <div className="about-main-box">
                                <h4>SquareWork is a robust online technology platform focused on bridging the gap between skilled
creatives and prospective clients globally.
                                </h4>
                            </div>
                        </div>
                        </div>
                        <div className="row mt-5">
                        <div className="col-lg-12 col-md-12">
                            <h2><strong>Our Story</strong></h2>
                            <p className="mt-4">SquareWork brings a new dimension of ease, confidence and speed to find, hire, and collaborate
                            with creatives all over the world. As a company looking to work with a productive team of creatives,
                            to achieve a set objective, or as an individual that just wants something done, SquareWork helps you
                            with accurate and timely solutions. We deliver premium services that provide for the ever-
                            growing needs of the technological and creative industries across the world. We guarantee quality,
                            efficiency, effectiveness, and total value for money across every service we provide. SquareWork
                            takes away the hassle and risks of contracts, logistics, negotiations, and financials, between
                            employers and employees.
                            </p>
                            
                            <h5 className="mt-4">Creating a world of work for creatives</h5>
                            <p className="mt-4">We deliver quality and timely output. We totally take away the risk of working with the wrong
                            people. Our creatives are vetted for talent and quality to achieve satisfactory client experience. We
                            are the link between a global community of professional creatives and prospective clients.
                            </p>

                            <p>Creatives are encouraged to join SquareWork, showcase their portfolio, get hired to work on
                            projects, and become part of the global creative community. We are committed to helping creative
                            talents learn, grow, and get hired for projects by individuals and organisations globally.</p>
                            
                            <h5 className="mt-4">Our Mission</h5>
                            <p className="mt-4">At Squarework, our mission is to foster a collaborative work place that thrives to create the best
                            experience for Creatives.
                            </p>

                            <h5 className="mt-4">Our Proposition</h5>
                            <p className="mt-4">Connect, create, collaborate and become a part of the growing creative community.
                            </p>
                        
                        </div>
                        </div>
                    </div>
                </section>

                {/* <Planbill /> */}

                {/* Team Section */}
                {/* <section className="jobguru-team-area section_70">
                    <div className="container">
                        <div className="row">
                        <div className="col-md-12">
                            <div className="site-heading">
                                <h2>Awesome Team <span>Member</span></h2>
                                <p>A better career is out there. We'll help you find it. We're your first step to becoming everything you want to be.</p>
                            </div>
                        </div>
                        </div>
                        <div className="row">
                        <div className="col-md-6 col-lg-3">
                            <div className="single-team-member">
                                <div className="team-img">
                                    <img src={Teamtwo} alt="teamMember" />
                                    <div className="team-overlay">
                                    <ul>
                                        <li><Link to="#"><i className="fa fa-facebook"></i></Link></li>
                                        <li><Link to="#"><i className="fa fa-twitter"></i></Link></li>
                                        <li><Link to="#"><i className="fa fa-linkedin"></i></Link></li>
                                        <li><Link to="#"><i className="fa fa-google-plus"></i></Link></li>
                                    </ul>
                                    </div>
                                </div>
                                <div className="team-text">
                                    <h4>Jylin Scott</h4>
                                    <p>Founder & CEO</p>
                                </div>
                            </div>
                        </div>
                        <div className="col-md-6 col-lg-3">
                            <div className="single-team-member">
                                <div className="team-img">
                                    <img src={Teamtwo} alt="team member" />
                                    <div className="team-overlay">
                                    <ul>
                                        <li><Link to="#"><i className="fa fa-facebook"></i></Link></li>
                                        <li><Link to="#"><i className="fa fa-twitter"></i></Link></li>
                                        <li><Link to="#"><i className="fa fa-linkedin"></i></Link></li>
                                        <li><Link to="#"><i className="fa fa-google-plus"></i></Link></li>
                                    </ul>
                                    </div>
                                </div>
                                <div className="team-text">
                                    <h4>James Sienna</h4>
                                    <p>managing director</p>
                                </div>
                            </div>
                        </div>
                        <div className="col-md-6 col-lg-3">
                            <div className="single-team-member">
                                <div className="team-img">
                                    <img src={Teamone} alt="team member" />
                                    <div className="team-overlay">
                                    <ul>
                                        <li><Link to="#"><i className="fa fa-facebook"></i></Link></li>
                                        <li><Link to="#"><i className="fa fa-twitter"></i></Link></li>
                                        <li><Link to="#"><i className="fa fa-linkedin"></i></Link></li>
                                        <li><Link to="#"><i className="fa fa-google-plus"></i></Link></li>
                                    </ul>
                                    </div>
                                </div>
                                <div className="team-text">
                                    <h4>Charles Luke</h4>
                                    <p>HR Manager</p>
                                </div>
                            </div>
                        </div>
                        <div className="col-md-6 col-lg-3">
                            <div className="single-team-member">
                                <div className="team-img">
                                    <img src={Teamfour} alt="team member" />
                                    <div className="team-overlay">
                                    <ul>
                                        <li><Link to="#"><i className="fa fa-facebook"></i></Link></li>
                                        <li><Link to="#"><i className="fa fa-twitter"></i></Link></li>
                                        <li><Link to="#"><i className="fa fa-linkedin"></i></Link></li>
                                        <li><Link to="#"><i className="fa fa-google-plus"></i></Link></li>
                                    </ul>
                                    </div>
                                </div>
                                <div className="team-text">
                                    <h4>Catherine Matt</h4>
                                    <p>Senior Consultant</p>
                                </div>
                            </div>
                        </div>
                        </div>
                    </div>
                </section> */}
            {/* End of team section */}

            </Layout>
        )
    }
}

export default About