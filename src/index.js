import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';

// Redux setup
import { createStore } from 'redux';
import { Provider } from 'react-redux';
import configureStore from './store';

import Root from './components/Root'
import rootReducer from './reducers/rootReducer';

import * as serviceWorker from './serviceWorker';
import { BrowserRouter} from "react-router-dom";

const store = createStore(rootReducer)

ReactDOM.render(
<Root store={store} />, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
