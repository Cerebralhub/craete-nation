import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import rootReducer from './reducers/rootReducer';
import logger from 'redux-logger'
import ReduxPromise from "redux-promise-middleware";

const middleware = applyMiddleware(ReduxPromise(), thunk, logger());
// const store = createStore(rootReducer, middleware);
// const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
// const enhancer = composeEnhancers(middleware);

export default createStore(rootReducer);

// export default function configureStore(initialState = {}) {
//     return createStore(
//         rootReducer,
//         initialState,
//         applyMiddleware(thunk)
//     );
// }

// export default createStore(
//     rootReducer,
//     enhancer
// )