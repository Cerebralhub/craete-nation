import simpleAction from '../actions/simpleAction'


function simpleReducers(state = initialState, action) {
    switch(action.type) {
        case 'SIMPLE_ACTION':
            return Object.assign({}, state, {
                result: action.payload
            })
        default:
            return state;
    }
}

export default simpleReducers