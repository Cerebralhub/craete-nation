import { FIRST_ACTION, REQUEST_PENDING, REQUEST_FULFILLED, REQUEST_REJECTED, TOGGLE_LOADER } from '../actions/actions';

const initialState = {
    fetching: false,
    fetched: false,
    items: [],
    error: '',
    success: '',
    message: ''
}

function rootReducer(state = initialState, action) {
    switch(action.type) {
        case TOGGLE_LOADER:
            return Object.assign({}, state, {
                loader: {
                    status: action.status
                }
            })
        case FIRST_ACTION: 
            return Object.assign({}, state, {
                grabAction: [...state.grabAction, action.text]
            });
        case REQUEST_PENDING: {
            return {
                ...state,
                fetching: true
            }
        }
        case REQUEST_FULFILLED: {
            return {
                ...state,
                fetching: false,
                fetched: true,
                items: action.payload
            }
        }
        case REQUEST_REJECTED: {
            return {
                ...state,
                fetching: false,
                error: action.payload
            }
        }
        default:
            return state;
    }
}

export default rootReducer;