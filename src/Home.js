import React from 'react';
import Layout from './components/Layouts/Layout';
import ProfileList from './components/Profile/Profilelist';
import { Link } from 'react-router-dom';


// Images
import ArrowImage from './assets/img/arrow-right-top.png'
import ArrowImagee from './assets/img/arrow-right-bottom.png'
import Animation from './assets/img/animation-min.jpg';
import Graphics from './assets/img/graphics.jpg';
import Illustrators from './assets/img/illustrators.jpg';
import Digitalmarketer from './assets/img/digitalmarketer.jpg';
import Socialinfluencers from './assets/img/influencers.jpg';
import Writing from './assets/img/writing.jpg';
import Jobpost from './assets/img/undraw_mobile_wireframe_euf4.png';
import BannerImage from './assets/img/undraw_dev_focus_b9xo.png';

import axiosInterceptors from './components/helpers/axiosInterceptors';
import swal from 'sweetalert';

import TriangleLoader from './components/Loader/triangleLoader'

import Shimmer from "react-shimmer-effect";
import injectSheet from "react-jss";

const StyleSheet = {
   container: {
     border: "0px solid rgba(255, 255, 255, 1)",
     boxShadow: "0px 0px 20px rgba(0, 0, 0, .1)",
     borderRadius: "4px",
     backgroundColor: "white",
     display: "flex",
     padding: "16px",
     width: "200px"
   },
   circle: {
     height: "56px",
     width: "56px",
     borderRadius: "50%"
   },
   line: {
     width: "96px",
     height: "8px",
     alignSelf: "center",
     marginLeft: "16px",
     borderRadius: "8px"
   }
 };


class Home extends React.Component {
    constructor(props){
      super(props);

      
      this.state = {
         categories: null,
         isLoading: true
      }
  }

   componentDidMount() {
      this.getCategories()
   }

   getCategories() {
      axiosInterceptors().get('/categoryLimit?offset=9&limit=6')
      .then((response) => {
         this.setState({
            categories: response.data.categories,
            isLoading: false
         })
      })
      .catch((err) => {
         swal('Categories could not be loaded', 'There was a problem loading categories', 'error')
      })
   }

  render() {
   const imgArr = [Animation, Graphics, Illustrators, Digitalmarketer, Socialinfluencers, Writing];
   const { classes } = this.props;
    return (

      <Layout>

   <div>
      <div className="home-video-banner parallax">
         <div className="banner-area">
            <div className="banner-caption container">
               <div className="container">
                  <div className="row">
                     <div className="col-md-12 col-sm-12 ">
                        <div className="banner-search">
                           <h2 className="text-center">Hire expert <span style={{color:"#1BC8D1"}}>Creatives.</span></h2>
                           <h4 className="text-center"><span style={{color:"#1BC8D1", fontSize:"25px"}}>Squarework</span> connects creatives to businesses seeking specialized talent.</h4>
                           <div className="col-6 bannerbtn">
                           <form>
                              <div className="p-1 bg-light rounded rounded-pill shadow-sm mb-4">
                                 <div className="input-group ">
                                    <input type="search" placeholder='Try "Graphics Designer"' aria-describedby="button-addon1" className="form-control border-0 bg-light" />
                                    <div className="input-group-append">
                                    <button id="button-addon1" type="submit" className="btn btn-link text-primary"><i className="fa fa-search" style={{color: "#1BC8D1"}} /></button>
                                    </div>
                                 </div>
                              </div>
                           </form>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            </div>
         </div>
         </div>

         <section className=" section_2">
         <div className="container">
            <div className="row">
               <div className="col-md-6">
               <h3>Are you an<span style={{color:"#1BC8D1"}}> Animator? Illustrator? Graphics Designer Or Content Writer?</span> Join Squarework
               today, and unleash your potential.
               </h3>
               <Link to="/register" className="jobguru-btn-3 mt-4">Get Started</Link>
            </div>
            <div className="col-md-6">
               <img src={BannerImage} alt="job post" style={{height: "auto", marginTop: "-63px"}}/>
            </div>
          </div>
         </div>
      </section>     

          {/* Category Starts here */}
          <section className="jobguru-categories-area section_705">
          <div className="container">
              <div className="row">
                <div className="col-md-12">
                    <div className="site-heading">
                      <h2>Our <span style={{color:"#1BC8D1"}}>Categories</span></h2>
                      <p>Find quality talent
                      
                      </p>
                    </div>
                </div>
              </div>
              <div className="row">
              {!this.state.isLoading ?
                  this.state.categories.map(allRound => {
                     const {name} = allRound;
                     return(
                        <div className="col-lg-4 col-md-6 col-sm-6">
                           <Link to="#" className="single-category-holder account_cat">
                              <div className="category-holder-icon">
                                 <i className="fa fa-briefcase"></i>
                              </div>
                              <div className="category-holder-text">
                                 <h3>{name}</h3>
                              </div>
                              
                              <img src={Animation} alt="category" />
                           </Link>
                        </div>
                     )
                  })
                  :
                  <>
                  <div className="col-lg-4 col-md-6 col-sm-6">
                     <Link to="#" className="single-category-holder account_cat">
                        <div className={classes.container}>
                           <Shimmer>
                              <div className={classes.circle} />
                              <div className={classes.line} />
                           </Shimmer>
                        </div>
                     </Link>
                  </div>
                  <div className="col-lg-4 col-md-6 col-sm-6">
                     <Link to="#" className="single-category-holder account_cat">
                        <div className={classes.container}>
                           <Shimmer>
                              <div className={classes.circle} />
                              <div className={classes.line} />
                           </Shimmer>
                        </div>
                     </Link>
                  </div>
                  <div className="col-lg-4 col-md-6 col-sm-6">
                     <Link to="#" className="single-category-holder account_cat">
                        <div className={classes.container}>
                           <Shimmer>
                              <div className={classes.circle} />
                              <div className={classes.line} />
                           </Shimmer>
                        </div>
                     </Link>
                  </div>
                  <div className="col-lg-4 col-md-6 col-sm-6">
                     <Link to="#" className="single-category-holder account_cat">
                        <div className={classes.container}>
                           <Shimmer>
                              <div className={classes.circle} />
                              <div className={classes.line} />
                           </Shimmer>
                        </div>
                     </Link>
                  </div>
                  <div className="col-lg-4 col-md-6 col-sm-6">
                     <Link to="#" className="single-category-holder account_cat">
                        <div className={classes.container}>
                           <Shimmer>
                              <div className={classes.circle} />
                              <div className={classes.line} />
                           </Shimmer>
                        </div>
                     </Link>
                  </div>
                  <div className="col-lg-4 col-md-6 col-sm-6">
                     <Link to="#" className="single-category-holder account_cat">
                        <div className={classes.container}>
                           <Shimmer>
                              <div className={classes.circle} />
                              <div className={classes.line} />
                           </Shimmer>
                        </div>
                     </Link>
                  </div>
                  </>
               }
                
              </div>
          </div>
      </section>
      {/* End of category */}

      <section className="jobguru-inner-hire-area section_100">
         <div className="hire_circle"></div>
         <div className="container">
            <div className="row">
               <div className="col-md-12">
                  <div className="inner-hire-left">
                     <h3>Why Enlist Us</h3>
                     <p> As a company looking to work with a productive team of creatives,
                     to achieve a set objective, or as an individual that just wants something done, SquareWork helps you
                     with accurate and timely solutions.
                     </p>
                     <Link to="/register" className="jobguru-btn-3">Sign up as a creative</Link>
                  </div>
               </div>
            </div>
         </div>
      </section>
      {/* <!-- Inner Hire Area End --> */}

       {/* Job Tab Area Start  */}
      <section className="jobguru-job-tab-area section_70">
         <div className="container">
            <div className="row">
               <div className="col-md-12">
                  <div className="site-heading">
                     <h2>Our<span id="color"> top creatives</span></h2>
                     <p>They can't wait to work for you.</p>
                  </div>
               </div>
            </div>

            <ProfileList />

            <div className="row">
               <div className="col-md-12">
                  <div className="load-more">
                     <Link to="/browsecandidates" className="jobguru-btn">Browse More Candidates</Link>
                  </div>
                  
               </div>
            </div>
         </div>
      </section>

      {/* Video starts here */}
      <section className=" section_100">
         <div className="container">
            <div className="row">
               <div className="col-md-6">
               <h3>Do you have a project to work on? Hire expert <span style={{color:"#1BC8D1"}}>creatives</span> today for your job.</h3>
               <Link to="/register" className="jobguru-btn-3 mt-5">Sign up as an employer</Link>
            </div>
            <div className="col-md-6">
               <img src={Jobpost} alt="job post" style={{height: "auto", marginTop: "-60px"}}/>
            </div>
          </div>
         </div>
      </section>
      {/* End of video */}
      

      {/* How it works */}
      <section className="how-works-area section_70">
         <div className="container">
            <div className="row">
               <div className="col-md-12">
                  <div className="site-heading">
                     <h2>how it <span id="color">works</span></h2>
                     <p>It's easy.Simply sign-up, create a project, pick a professional</p>
                  </div>
               </div>
            </div>
            <div className="row">
               <div className="col-md-4">
                  <div className="how-works-box box-1">
                     <img src={ArrowImage} alt="works" />
                     <div className="works-box-icon">
                        <i className="fa fa-user"></i>
                     </div>
                     <div className="works-box-text">
                        <p>sign up</p>
                     </div>
                  </div>
               </div>
               <div className="col-md-4">
                  <div className="how-works-box box-2">
                     <img src={ArrowImagee} alt="works" />
                     <div className="works-box-icon">
                        <i className="fa fa-gavel"></i>
                     </div>
                     <div className="works-box-text">
                        <p>Create a project</p>
                     </div>
                  </div>
               </div>
               <div className="col-md-4">
                  <div className="how-works-box box-3">
                     <div className="works-box-icon">
                        <i className="fa fa-thumbs-up"></i>
                     </div>
                     <div className="works-box-text">
                        <p>choose expert</p>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>
      {/* How it works */}

      </Layout>

    );
  }
  
}

// export default Home;
export default injectSheet(StyleSheet)(Home);
