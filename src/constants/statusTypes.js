const statusTypes = {
    verified: 1,
    unverified: 2,
    active: 3,
    inactive: 4,
    open: 5,
    closed: 6,
    applied: 7,
    suspended: 8
}

export default statusTypes;