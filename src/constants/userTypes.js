const userTypes = {
    professionals: 1,
    employers: 2,
    admin: 3
}

export default userTypes;