import React, { Component } from 'react';
import { 
   Switch, 
   Route,
   Link,
   Redirect,
   useHistory,
   useLocation
 } from "react-router-dom";

import { connect } from 'react-redux';
import action  from './actions/simpleAction';

// Pages
import Banner from './components/Layouts/Banner';
import Home from './Home';
import About from './components/Extras/About';
import Privacy from './components/Extras/Privacypolicy';
import Terms from './components/Extras/Terms&Condition';
import FAQs from './components/Extras/Faq'
import Register from './components/Auth/Register';
import Contact from './components/Extras/Contact';
import Resume from './components/Professionals/Resume';
import Candidatedashboard from './components/Professionals/Candidatedashboard';
import Profile from './components/Profile/Profile';
import Login from './components/Auth/Login';
import Listjobs from './components/Jobs/Listjobs';
import EmployerDashboard from './components/Employers/EmployerDashboard';
import ManageCandidates from './components/Employers/Managecandidates';
import CompanyProfile from './components/Extras/CompanyProfile';
import PostJob from './components/Jobs/PostJob';
import CreateJob from './components/Jobs/CreateJob';
import ChangePassword from './components/Auth/ChangePassword';
import ParticlesPage from './components/Extras/ParticlesPage';
import BrowseJobs from './components/Professionals/BrowseJobs';
import BrowseCategories from './components/Professionals/BrowseCategories';
import BrowseCompanies from './components/Professionals/BrowseCompanies';
import CandidatesDetails from './components/Professionals/CandidatesDetails'
import CompanyDetails from './components/Professionals/CompanyDetails';
import BrowseCandidates from './components/Professionals/BrowseCandidates';
import ManageJobs from './components/Professionals/Managejobs';
import ApplyJobs from './components/Jobs/Applyjobs';
import ForgotPassword from './components/Auth/ForgotPassword';
import ConfirmPassword from './components/Auth/ConfirmPassword';


import dotenv from 'dotenv';
import Candidateblock from './components/Professionals/Candidateblock';
dotenv.config();

let getToken = localStorage.getItem('token');

const checkToken = () => {
   if(getToken)
      return true
   else
      return false
}

const mapStateToProps = state => ({
   ...state
  })

const mapDispatchToProps = dispatch => ({
   action: () => dispatch(action())
})

let authCheck = {
   isAuthenticated: false,

   authenticate(cb) {
      this.isAuthenticated = getToken ? true : false;
     setTimeout(cb, 100); 
   },

   signout(cb) {
      authCheck.isAuthenticated = getToken ? true : false;
     setTimeout(cb, 100);
   }
 };

 const PrivateRoute = ({ children, ...rest }) => {
   return (
     <Route
       {...rest}
       render={({ location }) =>
       localStorage.getItem('token') ? (
           children
         ) : (
           <Redirect
             to={{
               pathname: "/login",
               state: { from: location }
             }}
           />
         )
       }
     />
   );
 }

function App() {
    return (
      <Switch>
      <Route exact path="/" >
        <Home />
      </Route>
      <Route path="/register" render={(props) => <Register name="Register" {...props} />} />
      
      <Route path="/login" render={(props) => <Login name="Login" {...props} />} />
      
      <Route path="/banner">
        <Banner />
      </Route>

      <Route path="/about">
         <About/>
      </Route>

      <Route path="/privacypolicy">
         <Privacy/>
      </Route>
      <Route path="/terms&condition">
        <Terms/>
     </Route>
     <Route path="/faqs">
     <FAQs/>
  </Route>

      <Route path="/contact">
         <Contact/>
      </Route>

      {/* Authenticated routes */}
      <PrivateRoute path="/candidatedashboard">
         <Candidatedashboard />
      </PrivateRoute>

      <PrivateRoute path="/employerdashboard">
         <EmployerDashboard />
      </PrivateRoute>

      <PrivateRoute path="/managecandidates">
         <ManageCandidates />
      </PrivateRoute>

      <PrivateRoute path="/profile">
         <Profile/>
      </PrivateRoute>

      <PrivateRoute path="/managejobs">
         <ManageJobs/>
      </PrivateRoute>

      <PrivateRoute path="/listjobs">
         <Listjobs />
      </PrivateRoute>

      <PrivateRoute path="/CandidatesDetails">
         <CandidatesDetails />
      </PrivateRoute>

      <PrivateRoute path="/applyjobs">
         <ApplyJobs/>
      </PrivateRoute>

      <PrivateRoute path="/createjob">
         <CreateJob/>
      </PrivateRoute>

      {/* Authenticated routes end */}

      <Route path="/resume">
         <Resume/>
      </Route>

      

      <Route path="/companyprofile">
         <CompanyProfile/>
      </Route>
      <Route path="/postjob">
         <PostJob/>
      </Route>
      
      <Route path="/changepassword">
         <ChangePassword/>
      </Route>
      <Route path="/ParticlesPage">
         <ParticlesPage/>
      </Route>
      <Route path="/Candidateblock">
         <Candidateblock/>
      </Route>
         
      <Route path="/BrowseJobs" render={(props) => 
         <BrowseJobs name="Browse Job" {...props} /> }>   
      </Route>
      
      <Route path="/BrowseCategories" render={(props) => 
         <BrowseCategories name="Browse Categories" {...props} /> }>   
      </Route>

      <Route path="/BrowseCompanies" render={(props) => 
         <BrowseCompanies name="Browse Companies" {...props} /> }>   
      </Route>

      <Route path="/CandidatesDetails">
         <CandidatesDetails/>
      </Route>

      <Route path="/CompanyDetails">
         <CompanyDetails/>
      </Route>
      <Route path="/BrowseCandidates">
         <BrowseCandidates/>
      </Route>
      <Route path="/ForgotPassword" render={(props) => 
         <ForgotPassword name="Forgot Password" {...props} /> }>
      </Route>
      <Route path="/confirmPassword" render={(props) => 
         <ConfirmPassword name="Confirm Password" {...props} /> }>
      </Route>
    </Switch>
    )
  }
  
// }

export default connect(mapStateToProps, mapDispatchToProps)(App);
