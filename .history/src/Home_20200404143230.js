import React from 'react';
import Layout from './components/Layouts/Layout';
import ProfileList from './components/Profile/Profilelist';
import { Link } from 'react-router-dom'

// Images
import ArrowImage from './assets/img/arrow-right-top.png'
//import JobSeeker from './assets/img/woman.jpg';
import ArrowImagee from './assets/img/arrow-right-bottom.png'
// import ParticlesPage from './components/Extras/ParticlesPage'
import Animation from './assets/img/animation.jpg';
import Graphics from './assets/img/graphics.jpg';
import Illustrators from './assets/img/illustrators.jpg';
import Digitalmarketer from './assets/img/digitalmarketer.jpg';
import Socialinfluencers from './assets/img/influencers.jpg';
import Writing from './assets/img/writing.jpg';
import Jobpost from './assets/img/undraw_work_chat_erdt.svg';

import axiosInterceptors from './components/helpers/axiosInterceptors';
import swal from 'sweetalert';

import TriangleLoader from './components/Loader/triangleLoader'

import Shimmer from "react-shimmer-effect";
import injectSheet from "react-jss";

const StyleSheet = {
   container: {
     border: "0px solid rgba(255, 255, 255, 1)",
     boxShadow: "0px 0px 20px rgba(0, 0, 0, .1)",
     borderRadius: "4px",
     backgroundColor: "white",
     display: "flex",
     padding: "16px",
     width: "200px"
   },
   circle: {
     height: "56px",
     width: "56px",
     borderRadius: "50%"
   },
   line: {
     width: "96px",
     height: "8px",
     alignSelf: "center",
     marginLeft: "16px",
     borderRadius: "8px"
   }
 };

class Home extends React.Component {
    constructor(props){
      super(props);

      
      this.state = {
         categories: null,
         isLoading: true
      }
  }

   componentDidMount() {
      this.getCategories()
   }

   getCategories() {
      axiosInterceptors().get('/categoryLimit')
      .then((response) => {
         this.setState({
            categories: response.data.categories,
            isLoading: false
         })
      })
      .catch((err) => {
         swal('Categories could not be loaded', 'There was a problem loading categories', 'error')
      })
   }

  render() {
   const imgArr = [Animation, Graphics, Illustrators, Digitalmarketer, Socialinfluencers, Writing];
   const { classes } = this.props;
    return (

      <Layout>

<div>
      <div className="home-video-banner parallax">
         <div className="banner-area">
            <div className="banner-caption container">
               <div className="container">
                  <div className="row">
                     <div className="col-md-12 col-sm-12 ">
                        <div className="banner-search">
                           <h2 className="text-center">Hire expert freelancers.</h2>
                           <h4 className="text-center">Get matched with expert freelance creatives on your projects.</h4>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            </div>
         </div>
         </div>
              

          {/* Category Starts here */}
          <section className="jobguru-categories-area section_70">
          <div className="container">
              <div className="row">
                <div className="col-md-12">
                    <div className="site-heading">
                      <h2>All our <span id="color">Categories</span></h2>
                      <p>We have creatives who are professionals at the following.
                      
                      </p>
                    </div>
                </div>
              </div>
              <div className="row">
              {!this.state.isLoading ?
                  this.state.categories.map(allRound => {
                     const {name} = allRound;
                     return(
                        <div className="col-lg-4 col-md-6 col-sm-6">
                           <Link to="#" className="single-category-holder account_cat">
                              <div className="category-holder-icon">
                                 <i className="fa fa-briefcase"></i>
                              </div>
                              <div className="category-holder-text">
                                 <h3>{name}</h3>
                              </div>
                              
                              <img src={Animation} alt="category" />
                           </Link>
                        </div>
                     )
                  })
                  :
                  <>
                  <div className="col-lg-4 col-md-6 col-sm-6">
                     <Link to="#" className="single-category-holder account_cat">
                        <div className={classes.container}>
                           <Shimmer>
                              <div className={classes.circle} />
                              <div className={classes.line} />
                           </Shimmer>
                        </div>
                     </Link>
                  </div>
                  <div className="col-lg-4 col-md-6 col-sm-6">
                     <Link to="#" className="single-category-holder account_cat">
                        <div className={classes.container}>
                           <Shimmer>
                              <div className={classes.circle} />
                              <div className={classes.line} />
                           </Shimmer>
                        </div>
                     </Link>
                  </div>
                  <div className="col-lg-4 col-md-6 col-sm-6">
                     <Link to="#" className="single-category-holder account_cat">
                        <div className={classes.container}>
                           <Shimmer>
                              <div className={classes.circle} />
                              <div className={classes.line} />
                           </Shimmer>
                        </div>
                     </Link>
                  </div>
                  <div className="col-lg-4 col-md-6 col-sm-6">
                     <Link to="#" className="single-category-holder account_cat">
                        <div className={classes.container}>
                           <Shimmer>
                              <div className={classes.circle} />
                              <div className={classes.line} />
                           </Shimmer>
                        </div>
                     </Link>
                  </div>
                  <div className="col-lg-4 col-md-6 col-sm-6">
                     <Link to="#" className="single-category-holder account_cat">
                        <div className={classes.container}>
                           <Shimmer>
                              <div className={classes.circle} />
                              <div className={classes.line} />
                           </Shimmer>
                        </div>
                     </Link>
                  </div>
                  <div className="col-lg-4 col-md-6 col-sm-6">
                     <Link to="#" className="single-category-holder account_cat">
                        <div className={classes.container}>
                           <Shimmer>
                              <div className={classes.circle} />
                              <div className={classes.line} />
                           </Shimmer>
                        </div>
                     </Link>
                  </div>
                  </>
               }
                
              </div>
          </div>
      </section>
      {/* End of category */}

      <section className="jobguru-inner-hire-area section_100">
         <div className="hire_circle"></div>
         <div className="container">
            <div className="row">
               <div className="col-md-12">
                  <div className="inner-hire-left">
                     <h3>Why Enlist Us</h3>
                     <p> Sign up to be the first to get well paid Jobs from reputable African companies, and guess what you dont have to leave the comfort of abode </p>
                     <Link to="/register" className="jobguru-btn-3">Sign up as a creative</Link>
                  </div>
               </div>
            </div>
         </div>
      </section>
      {/* <!-- Inner Hire Area End --> */}
       
       {/* Job Tab Area Start  */}
      <section className="jobguru-job-tab-area section_70">
         <div className="container">
            <div className="row">
               <div className="col-md-12">
                  <div className="site-heading">
                     <h2>Our<span id="color"> top creatives</span></h2>
                     <p>They can't wait to work for you.</p>
                  </div>
               </div>
            </div>

            <ProfileList />

            <div className="row">
               <div className="col-md-12">
                  <div className="load-more">
                     <Link to="/browsecandidates" className="jobguru-btn">Browse More Candidates</Link>
                  </div>
               </div>
            </div>
         </div>
      </section>

      {/* Video starts here */}
      <section className=" section_100">
         <div className="container">
            <div className="row">
               <div className="col-md-6">
                  {/* <div className="video-container">
                     <h2>Hire experts freelancers today for <br/> any job, any time.</h2>
                     <div className="video-btn">
                        <a className="popup-youtube" href="https://www.youtube.com/watch?v=k-R6AFn9-ek">
                        <i className="fa fa-play"></i>
                        how it works
                        </a>
                     </div>
               </div> */}
               <h2>Hire experts freelancers today for <br/> any job, any time.</h2>
            </div>
            <div className="col-md-6">
               <img src={Jobpost} alt="job post"/>
            </div>
          </div>
         </div>
      </section>
      {/* End of video */}

      {/* How it works */}
      <section className="how-works-area section_70">
         <div className="container">
            <div className="row">
               <div className="col-md-12">
                  <div className="site-heading">
                     <h2>how it <span id="color">works</span></h2>
                     <p>It's easy.Simply sign-up, create a project, pick a professional</p>
                  </div>
               </div>
            </div>
            <div className="row">
               <div className="col-md-4">
                  <div className="how-works-box box-1">
                     <img src={ArrowImage} alt="works" />
                     <div className="works-box-icon">
                        <i className="fa fa-user"></i>
                     </div>
                     <div className="works-box-text">
                        <p>sign up</p>
                     </div>
                  </div>
               </div>
               <div className="col-md-4">
                  <div className="how-works-box box-2">
                     <img src={ArrowImagee} alt="works" />
                     <div className="works-box-icon">
                        <i className="fa fa-gavel"></i>
                     </div>
                     <div className="works-box-text">
                        <p>Create a project</p>
                     </div>
                  </div>
               </div>
               <div className="col-md-4">
                  <div className="how-works-box box-3">
                     <div className="works-box-icon">
                        <i className="fa fa-thumbs-up"></i>
                     </div>
                     <div className="works-box-text">
                        <p>choose expert</p>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>
      {/* How it works */}

      </Layout>

    );
  }
  
}

// export default Home;
export default injectSheet(StyleSheet)(Home);
