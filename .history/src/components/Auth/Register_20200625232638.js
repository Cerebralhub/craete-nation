import React from 'react';
import Layout from '../Layouts/Layout';
import { Link } from 'react-router-dom';
import swal from 'sweetalert';

// import axios from 'axios';
import axiosInterceptors from '../helpers/axiosInterceptors';

// Assets
import '../../assets/css/style.css';
import '../../assets/css/responsive.css'
import '../../assets/css/animate.min.css';
import '../../assets/css/select2.min.css';
import '../../assets/css/perfect-scrollbar.min.css';

import '../../assets/css/common.css';
import '../../assets/css/theme-03.css';
import logo from '../../assets/img/logo.png';

// Components
import Banner from '../Layouts/Banner';

//Constants
import userTypes from '../../constants/userTypes';

const emailRegex = RegExp(/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/)
 
const formValid =  ({ formErrors, ...rest }) => {
  let valid = true;

  // validate form errors being empty
  Object.values(formErrors).forEach( val => { 
    val.length > 0 && (valid = false)
  });

  // validate the form was filled out
  Object.values(rest).forEach(val => {
    val === null && (valid = false)
  });

  return valid;
}

class Register extends React.Component{
  constructor(props) {
    super(props);

    this.state = {
      firstName: null,
      lastName: null,
      email: null,
      phoneNumber: '',
      address: '',
      userTypes: '',
      successMsg: '',
      errorMsg: '',
      token: '',
      formErrors: {
        firstName: "",
        lastName: "",
        email: "",
        password: "",
        confirm_password: ""
      }
    };
  }


  registerUser() {
    console.log('first');
    console.log(this.state)
    axiosInterceptors().post(`/register`, {
      'firstName': this.state.firstName,
      'lastName': this.state.lastName,
      'email': this.state.email,
      'phoneNumber': this.state.phoneNumber,
      'address': this.state.address, // this.state.address,
      'password': this.state.password,
      'usersTypeId': this.state.userTypes,
    })
    .then((response) => {
      // alert(response.data.message);
      console.log('second');
      swal(response.data.message, "continue", "success");
      this.setState({ token: response.headers.Authorization })

      localStorage.setItem('token', this.state.token)

      switch(response.data.user.usersTypeId) {
        case userTypes.professionals: 
          this.props.history.push('/candidatedashboard');
          break;
        case userTypes.employers:
          this.props.history.push('/employerdashboard');
          break;
        default:
          this.props.history.push('/');
          break;
      }
      console.log('third');
      console.log(response);
      this.props.history.push('/employerdashboard');
    })
    .catch((err) => {
      console.log('fourth', err);
      swal(err.response.data.error, "Please try again.", "error");
      console.log(err);
      console.log(err.response.data.error);
    })
  }

  handleSubmit = e => {
    e.preventDefault();
    
    if (formValid(this.state)) {
      const { password, confirm_password } = this.state
      if (password !== confirm_password) {
          // alert("Passwords don't match");
          swal("Passwords don't match", "Please try again.", "error");
      }

      if(!this.state.userTypes) {
        // alert('Please select a user type');
        swal("Please select a user type", "Try again.", "warning");
      }

      this.registerUser();

    } else {
      // alert("FORM INVALID - DISPLAY ERROR MESSAGE");
      swal("FORM INVALID", "Please fill the form.", "warning");
    }
  }; 

  handleChange = e => {
    e.preventDefault();
    const { name, value } = e.target;
    let formErrors = this.state.formErrors;

    switch (name) {
      case "firstName":
        formErrors.firstName =
         value.length < 3  
         ? "mininum 3 characters required"
        : "";
        break;
        case "lastName":
            formErrors.lastName =
             value.length < 3  
             ?"mininum 3 characters required"
            : "";
        break;
        case "address":
            formErrors.address =
             value.length < 3  
             ?"mininum 3 characters required"
            : "";
        break;
        
        case "email":
            formErrors.email =
            emailRegex.test(value)  
            ?""
            : "invalid email address"
        break;

        case "password":
           formErrors.password =
           value.length < 6  
            ?"mininum 6 characters required"
           : "";
        break;
         
        // case "cornfirm_password":
        //    formErrors.cornfirm_password =
        //    password !== confirm_password
        //     ?"password dosen't match"
        //    : "";
        // break;
        
        default:
          break;
    }
    this.setState({ formErrors, [name]: value }, () => console.log(this.state));
  };
  

    render(){
      const { formErrors } = this.state;
        return(
            <div>
              

      <div className="forny-container">
        
<div className="forny-inner">
    <div className="forny-form">
        <div className="mb-5 text-center forny-logo">
            <a href="/"><img src={logo} alt="" /></a>
        </div>
        <div className="mb-8 text-center">
            <h4>Create an account</h4>
        </div>
        
        
        <form onSubmit={this.handleSubmit} noValidate>
            
    <div className="form-group">
        
        <div className="input-group">
            <div className="input-group-prepend">
                <span className="input-group-text">
                    <svg xmlns="http://www.w3.org/2000/svg" width="18" height="24" viewBox="0 0 18 24">
    <g transform="translate(-61.127)">
        <g transform="translate(61.127)">
            <path d="M75.6,15.584A3.128,3.128,0,0,1,72.452,12.7a5.374,5.374,0,0,0,1.229-1.234,7.564,7.564,0,0,0,1.331-3.524.537.537,0,0,0,.134-.191,5.891,5.891,0,0,0,.445-2.264A5.275,5.275,0,0,0,70.574,0a4.6,4.6,0,0,0-2.088.5,3.62,3.62,0,0,0-.738.134,4.171,4.171,0,0,0-2.6,2.407,6.062,6.062,0,0,0-.292,3.924,6.386,6.386,0,0,0,.27.831.537.537,0,0,0,.125.185A6.772,6.772,0,0,0,67.8,12.7a3.129,3.129,0,0,1-3.146,2.885,3.689,3.689,0,0,0-3.53,3.706V23.46a.536.536,0,0,0,.532.54H78.595a.536.536,0,0,0,.532-.54V19.291A3.688,3.688,0,0,0,75.6,15.584ZM68.044,1.676a2.588,2.588,0,0,1,.61-.1.526.526,0,0,0,.224-.061,3.576,3.576,0,0,1,1.7-.433,4.2,4.2,0,0,1,3.951,4.41c0,.073,0,.146-.005.218A2.3,2.3,0,0,0,72.862,5H69.234a.974.974,0,0,1-.593-.2,1.006,1.006,0,0,1-.328-.432.649.649,0,0,0-.645-.413.656.656,0,0,0-.592.5,5.033,5.033,0,0,1-1.2,2.188C65.336,4.406,66.3,2.187,68.044,1.676Zm-.463,9.346a6.408,6.408,0,0,1-1.29-3.289,6.123,6.123,0,0,0,1.549-2.2A2.083,2.083,0,0,0,68,5.669a2.021,2.021,0,0,0,1.23.414h3.629a1.264,1.264,0,0,1,1.153.76s0,.008,0,.011c0,3.051-1.744,5.532-3.887,5.532A3.315,3.315,0,0,1,67.581,11.022ZM68.8,13.23a3.821,3.821,0,0,0,2.647,0A4.241,4.241,0,0,0,73,15.78l-2.8,4.041a.091.091,0,0,1-.151,0l-2.8-4.042A4.242,4.242,0,0,0,68.8,13.23Zm9.258,9.69H62.192V19.29a2.612,2.612,0,0,1,2.59-2.629,4.5,4.5,0,0,0,1.553-.333l2.846,4.114a1.153,1.153,0,0,0,.947.5h0a1.153,1.153,0,0,0,.947-.5l2.846-4.113a4.326,4.326,0,0,0,1.552.333,2.612,2.612,0,0,1,2.59,2.629Z" transform="translate(-61.127)"/>
        </g>
    </g>
</svg>
                </span>
            </div>
            
            <input   className="form-control" name="firstName" type="firstname"
            placeholder="Firstname"
            noValidate
            // className={formErrors.firstName.length > 0 ? "error" : null}
            onChange={this.handleChange} />
            {formErrors.firstName.length > 0 && (
            <span className="errorMessage">{formErrors.firstName}</span>
            )}

        </div>
    </div>

    <div className="form-group">
        
        <div className="input-group">
            <div className="input-group-prepend">
                <span className="input-group-text">
                    <svg xmlns="http://www.w3.org/2000/svg" width="18" height="24" viewBox="0 0 18 24">
    <g transform="translate(-61.127)">
        <g transform="translate(61.127)">
            <path d="M75.6,15.584A3.128,3.128,0,0,1,72.452,12.7a5.374,5.374,0,0,0,1.229-1.234,7.564,7.564,0,0,0,1.331-3.524.537.537,0,0,0,.134-.191,5.891,5.891,0,0,0,.445-2.264A5.275,5.275,0,0,0,70.574,0a4.6,4.6,0,0,0-2.088.5,3.62,3.62,0,0,0-.738.134,4.171,4.171,0,0,0-2.6,2.407,6.062,6.062,0,0,0-.292,3.924,6.386,6.386,0,0,0,.27.831.537.537,0,0,0,.125.185A6.772,6.772,0,0,0,67.8,12.7a3.129,3.129,0,0,1-3.146,2.885,3.689,3.689,0,0,0-3.53,3.706V23.46a.536.536,0,0,0,.532.54H78.595a.536.536,0,0,0,.532-.54V19.291A3.688,3.688,0,0,0,75.6,15.584ZM68.044,1.676a2.588,2.588,0,0,1,.61-.1.526.526,0,0,0,.224-.061,3.576,3.576,0,0,1,1.7-.433,4.2,4.2,0,0,1,3.951,4.41c0,.073,0,.146-.005.218A2.3,2.3,0,0,0,72.862,5H69.234a.974.974,0,0,1-.593-.2,1.006,1.006,0,0,1-.328-.432.649.649,0,0,0-.645-.413.656.656,0,0,0-.592.5,5.033,5.033,0,0,1-1.2,2.188C65.336,4.406,66.3,2.187,68.044,1.676Zm-.463,9.346a6.408,6.408,0,0,1-1.29-3.289,6.123,6.123,0,0,0,1.549-2.2A2.083,2.083,0,0,0,68,5.669a2.021,2.021,0,0,0,1.23.414h3.629a1.264,1.264,0,0,1,1.153.76s0,.008,0,.011c0,3.051-1.744,5.532-3.887,5.532A3.315,3.315,0,0,1,67.581,11.022ZM68.8,13.23a3.821,3.821,0,0,0,2.647,0A4.241,4.241,0,0,0,73,15.78l-2.8,4.041a.091.091,0,0,1-.151,0l-2.8-4.042A4.242,4.242,0,0,0,68.8,13.23Zm9.258,9.69H62.192V19.29a2.612,2.612,0,0,1,2.59-2.629,4.5,4.5,0,0,0,1.553-.333l2.846,4.114a1.153,1.153,0,0,0,.947.5h0a1.153,1.153,0,0,0,.947-.5l2.846-4.113a4.326,4.326,0,0,0,1.552.333,2.612,2.612,0,0,1,2.59,2.629Z" transform="translate(-61.127)"/>
        </g>
    </g>
</svg>
                </span>
            </div>
            
              <input   className="form-control" name="lastName" type="lastname"
              placeholder="Lastname"
              // className={formErrors.lastName.length > 0 ? "error" : null} 
              noValidate
              onChange={this.handleChange} />
              {formErrors.lastName.length > 0 && (
              <span className="errorMessage">{formErrors.lastName}</span>
              )}
        </div>
    </div>

            
    <div className="form-group">
        
        <div className="input-group">
            <div className="input-group-prepend">
                <span className="input-group-text">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="16" viewBox="0 0 24 16">
    <g transform="translate(0)">
        <path d="M23.983,101.792a1.3,1.3,0,0,0-1.229-1.347h0l-21.525.032a1.169,1.169,0,0,0-.869.4,1.41,1.41,0,0,0-.359.954L.017,115.1a1.408,1.408,0,0,0,.361.953,1.169,1.169,0,0,0,.868.394h0l21.525-.032A1.3,1.3,0,0,0,24,115.062Zm-2.58,0L12,108.967,2.58,101.824Zm-5.427,8.525,5.577,4.745-19.124.029,5.611-4.774a.719.719,0,0,0,.109-.946.579.579,0,0,0-.862-.12L1.245,114.4,1.23,102.44l10.422,7.9a.57.57,0,0,0,.7,0l10.4-7.934.016,11.986-6.04-5.139a.579.579,0,0,0-.862.12A.719.719,0,0,0,15.977,110.321Z" transform="translate(0 -100.445)"/>
    </g>
</svg>
                </span>
            </div>
            
              <input   className="form-control" name="email" type="email"
              placeholder="Email Address"
              noValidate
              onChange={this.handleChange} />
              {formErrors.email.length > 0 && (
              <span className="errorMessage">{formErrors.email}</span>
              )}
        </div>
    </div>

    <div className="form-group">
        
        <div className="input-group">
            <div className="input-group-prepend">
                <span className="input-group-text">
                <svg class="icon icon-phone" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                  <path d="M6.62 10.79c1.44 2.83 3.76 5.14 6.59 6.59l2.2-2.2c.27-.27.67-.36 1.02-.24 1.12.37 2.33.57 3.57.57.55 
                  0 1 .45 1 1V20c0 .55-.45 1-1 1-9.39 0-17-7.61-17-17 0-.55.45-1 1-1h3.5c.55 0 1 .45 1 1 0 1.25.2 2.45.57 3.57.11.35.03.74-.25 1.02l-2.2 2.2z"/>
                  </svg>
                </span>
            </div>
            
    <input   className="form-control" name="phoneNumber" type="number"
    placeholder="Phone number" 
    onChange={event => this.setState({ phoneNumber: event.target.value })}/>

        </div>
        
    </div>

    <div className="form-group">
        
        <div className="input-group">
            <div className="input-group-prepend">
                <span className="input-group-text">
                    <svg xmlns="http://www.w3.org/2000/svg" width="18" height="24" viewBox="0 0 18 24">
    <g transform="translate(-61.127)">
        <g transform="translate(61.127)">
            <path d="M75.6,15.584A3.128,3.128,0,0,1,72.452,12.7a5.374,5.374,0,0,0,1.229-1.234,7.564,7.564,0,0,0,1.331-3.524.537.537,0,0,0,.134-.191,5.891,5.891,0,0,0,.445-2.264A5.275,5.275,0,0,0,70.574,0a4.6,4.6,0,0,0-2.088.5,3.62,3.62,0,0,0-.738.134,4.171,4.171,0,0,0-2.6,2.407,6.062,6.062,0,0,0-.292,3.924,6.386,6.386,0,0,0,.27.831.537.537,0,0,0,.125.185A6.772,6.772,0,0,0,67.8,12.7a3.129,3.129,0,0,1-3.146,2.885,3.689,3.689,0,0,0-3.53,3.706V23.46a.536.536,0,0,0,.532.54H78.595a.536.536,0,0,0,.532-.54V19.291A3.688,3.688,0,0,0,75.6,15.584ZM68.044,1.676a2.588,2.588,0,0,1,.61-.1.526.526,0,0,0,.224-.061,3.576,3.576,0,0,1,1.7-.433,4.2,4.2,0,0,1,3.951,4.41c0,.073,0,.146-.005.218A2.3,2.3,0,0,0,72.862,5H69.234a.974.974,0,0,1-.593-.2,1.006,1.006,0,0,1-.328-.432.649.649,0,0,0-.645-.413.656.656,0,0,0-.592.5,5.033,5.033,0,0,1-1.2,2.188C65.336,4.406,66.3,2.187,68.044,1.676Zm-.463,9.346a6.408,6.408,0,0,1-1.29-3.289,6.123,6.123,0,0,0,1.549-2.2A2.083,2.083,0,0,0,68,5.669a2.021,2.021,0,0,0,1.23.414h3.629a1.264,1.264,0,0,1,1.153.76s0,.008,0,.011c0,3.051-1.744,5.532-3.887,5.532A3.315,3.315,0,0,1,67.581,11.022ZM68.8,13.23a3.821,3.821,0,0,0,2.647,0A4.241,4.241,0,0,0,73,15.78l-2.8,4.041a.091.091,0,0,1-.151,0l-2.8-4.042A4.242,4.242,0,0,0,68.8,13.23Zm9.258,9.69H62.192V19.29a2.612,2.612,0,0,1,2.59-2.629,4.5,4.5,0,0,0,1.553-.333l2.846,4.114a1.153,1.153,0,0,0,.947.5h0a1.153,1.153,0,0,0,.947-.5l2.846-4.113a4.326,4.326,0,0,0,1.552.333,2.612,2.612,0,0,1,2.59,2.629Z" transform="translate(-61.127)"/>
        </g>
    </g>
</svg>
                </span>
            </div>
            
              <input   className="form-control" name="address" type="text"
              placeholder="Address"
              // className={formErrors.lastName.length > 0 ? "error" : null} 
              noValidate
              onChange={this.handleChange} />
              {formErrors.address.length > 0 && (
              <span className="errorMessage">{formErrors.address}</span>
              )}
        </div>
    </div>

            
    <div className="form-group password-field">
        
        <div className="input-group">
            <div className="input-group-prepend">
                <span className="input-group-text">
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="24" viewBox="0 0 16 24">
    <g transform="translate(0)">
        <g transform="translate(5.457 12.224)">
            <path d="M207.734,276.673a2.543,2.543,0,0,0-1.749,4.389v2.3a1.749,1.749,0,1,0,3.5,0v-2.3a2.543,2.543,0,0,0-1.749-4.389Zm.9,3.5a1.212,1.212,0,0,0-.382.877v2.31a.518.518,0,1,1-1.035,0v-2.31a1.212,1.212,0,0,0-.382-.877,1.3,1.3,0,0,1-.412-.955,1.311,1.311,0,1,1,2.622,0A1.3,1.3,0,0,1,208.633,280.17Z" transform="translate(-205.191 -276.673)"/>
        </g>
        <path d="M84.521,9.838H82.933V5.253a4.841,4.841,0,1,0-9.646,0V9.838H71.7a1.666,1.666,0,0,0-1.589,1.73v10.7A1.666,1.666,0,0,0,71.7,24H84.521a1.666,1.666,0,0,0,1.589-1.73v-10.7A1.666,1.666,0,0,0,84.521,9.838ZM74.346,5.253a3.778,3.778,0,1,1,7.528,0V9.838H74.346ZM85.051,22.27h0a.555.555,0,0,1-.53.577H71.7a.555.555,0,0,1-.53-.577v-10.7a.555.555,0,0,1,.53-.577H84.521a.555.555,0,0,1,.53.577Z" transform="translate(-70.11)"/>
    </g>
</svg>
                </span>
            </div>
            
              <input   className="form-control" name="password" type="password"
              placeholder="Password"
              noValidate
              onChange={this.handleChange} />
              {formErrors.password.length > 0 && (
              <span className="errorMessage">{formErrors.password}</span>
              )}

            <div className="input-group-append cursor-pointer">
                <span className="input-group-text">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="16" viewBox="0 0 24 16">
    <g transform="translate(0 0)">
        <g transform="translate(0 0)">
            <path d="M23.609,117.568a15.656,15.656,0,0,0-5.045-4.859,12.823,12.823,0,0,0-6.38-1.811c-.062,0-.309,0-.371,0a12.823,12.823,0,0,0-6.38,1.811,15.656,15.656,0,0,0-5.045,4.859,2.464,2.464,0,0,0,0,2.658,15.656,15.656,0,0,0,5.045,4.859,12.822,12.822,0,0,0,6.38,1.811c.062,0,.309,0,.371,0a12.823,12.823,0,0,0,6.38-1.811,15.656,15.656,0,0,0,5.045-4.859A2.464,2.464,0,0,0,23.609,117.568Zm-17.74,6.489a14.622,14.622,0,0,1-4.712-4.538,1.155,1.155,0,0,1,0-1.245,14.621,14.621,0,0,1,4.712-4.538,12.747,12.747,0,0,1,1.586-.79,8.964,8.964,0,0,0,0,11.9A12.748,12.748,0,0,1,5.869,124.057ZM12,125.75c-3.213,0-5.827-3.074-5.827-6.853s2.614-6.853,5.827-6.853,5.827,3.074,5.827,6.853S15.211,125.75,12,125.75Zm10.841-6.23a14.621,14.621,0,0,1-4.712,4.538,12.737,12.737,0,0,1-1.585.788,8.964,8.964,0,0,0,0-11.9,12.74,12.74,0,0,1,1.587.791,14.622,14.622,0,0,1,4.712,4.538A1.155,1.155,0,0,1,22.839,119.52Z" transform="translate(0.002 -110.897)"/>
        </g>
        <g transform="translate(9.565 5.565)">
            <path d="M205.24,202.8a2.435,2.435,0,1,0,2.435,2.435A2.438,2.438,0,0,0,205.24,202.8Zm0,3.917a1.482,1.482,0,1,1,1.482-1.482A1.483,1.483,0,0,1,205.24,206.721Z" transform="translate(-202.805 -202.804)"/>
        </g>
    </g>
</svg>
                </span>
            </div>
        </div>
    </div>

    <div className="form-group password-field">
        
        <div className="input-group">
            <div className="input-group-prepend">
                <span className="input-group-text">
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="24" viewBox="0 0 16 24">
    <g transform="translate(0)">
        <g transform="translate(5.457 12.224)">
            <path d="M207.734,276.673a2.543,2.543,0,0,0-1.749,4.389v2.3a1.749,1.749,0,1,0,3.5,0v-2.3a2.543,2.543,0,0,0-1.749-4.389Zm.9,3.5a1.212,1.212,0,0,0-.382.877v2.31a.518.518,0,1,1-1.035,0v-2.31a1.212,1.212,0,0,0-.382-.877,1.3,1.3,0,0,1-.412-.955,1.311,1.311,0,1,1,2.622,0A1.3,1.3,0,0,1,208.633,280.17Z" transform="translate(-205.191 -276.673)"/>
        </g>
        <path d="M84.521,9.838H82.933V5.253a4.841,4.841,0,1,0-9.646,0V9.838H71.7a1.666,1.666,0,0,0-1.589,1.73v10.7A1.666,1.666,0,0,0,71.7,24H84.521a1.666,1.666,0,0,0,1.589-1.73v-10.7A1.666,1.666,0,0,0,84.521,9.838ZM74.346,5.253a3.778,3.778,0,1,1,7.528,0V9.838H74.346ZM85.051,22.27h0a.555.555,0,0,1-.53.577H71.7a.555.555,0,0,1-.53-.577v-10.7a.555.555,0,0,1,.53-.577H84.521a.555.555,0,0,1,.53.577Z" transform="translate(-70.11)"/>
    </g>
</svg>
                </span>
            </div>
            
          <input   className="form-control" name="confirm_password" type="password"
          placeholder="Confirm Password"
          noValidate
          onChange={this.handleChange} />
          {formErrors.confirm_password.length > 0 && (
          <span className="errorMessage">{formErrors.confirm_password}</span>
          )}

            <div className="input-group-append cursor-pointer">
                <span className="input-group-text">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="16" viewBox="0 0 24 16">
    <g transform="translate(0 0)">
        <g transform="translate(0 0)">
            <path d="M23.609,117.568a15.656,15.656,0,0,0-5.045-4.859,12.823,12.823,0,0,0-6.38-1.811c-.062,0-.309,0-.371,0a12.823,12.823,0,0,0-6.38,1.811,15.656,15.656,0,0,0-5.045,4.859,2.464,2.464,0,0,0,0,2.658,15.656,15.656,0,0,0,5.045,4.859,12.822,12.822,0,0,0,6.38,1.811c.062,0,.309,0,.371,0a12.823,12.823,0,0,0,6.38-1.811,15.656,15.656,0,0,0,5.045-4.859A2.464,2.464,0,0,0,23.609,117.568Zm-17.74,6.489a14.622,14.622,0,0,1-4.712-4.538,1.155,1.155,0,0,1,0-1.245,14.621,14.621,0,0,1,4.712-4.538,12.747,12.747,0,0,1,1.586-.79,8.964,8.964,0,0,0,0,11.9A12.748,12.748,0,0,1,5.869,124.057ZM12,125.75c-3.213,0-5.827-3.074-5.827-6.853s2.614-6.853,5.827-6.853,5.827,3.074,5.827,6.853S15.211,125.75,12,125.75Zm10.841-6.23a14.621,14.621,0,0,1-4.712,4.538,12.737,12.737,0,0,1-1.585.788,8.964,8.964,0,0,0,0-11.9,12.74,12.74,0,0,1,1.587.791,14.622,14.622,0,0,1,4.712,4.538A1.155,1.155,0,0,1,22.839,119.52Z" transform="translate(0.002 -110.897)"/>
        </g>
        <g transform="translate(9.565 5.565)">
            <path d="M205.24,202.8a2.435,2.435,0,1,0,2.435,2.435A2.438,2.438,0,0,0,205.24,202.8Zm0,3.917a1.482,1.482,0,1,1,1.482-1.482A1.483,1.483,0,0,1,205.24,206.721Z" transform="translate(-202.805 -202.804)"/>
        </g>
    </g>
</svg>
                </span>
            </div>
        </div>
    </div>

                  <div className="text-center">
                    <h4>Sign up as</h4>
                  </div>
                  <div className="remember-row single-login-field clearfix" onChange={event => this.setState({ userTypes: event.target.value })}>
                    <p className="checkbox remember">
                      <input className="checkbox-spin" type="radio" name="role" id="employer" value={userTypes.employers} />
                      <label htmlFor="employer"><span /> An employer &nbsp;&nbsp;</label>
                    </p> 
                    <p className="checkbox remember">
                      <input className="checkbox-spin" type="radio" name="role" id="Freelance" value={userTypes.professionals} />
                      <label htmlFor="Freelance"><span /> A professional </label>
                    </p>
                  </div>


            <div>
                <button type="submit" className="btn btn-block" style={{backgroundColor: "#20D6DE", color:"white"}}>Register</button>
            </div>


            <div className="text-center mt-10">
                Already have an account? <a href="/login">Login here</a>
            </div>
        </form>
    </div>
</div>

    </div>


    </div>
    )
  }
}

export default Register;