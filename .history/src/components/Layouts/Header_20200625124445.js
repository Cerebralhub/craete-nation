import React from 'react';
import { Link, Redirect, RedirectProps } from 'react-router-dom';
// images
import logo from '../../assets/img/logo.png';
import logo2 from '../../assets/img/logo.png';
import '../../assets/css/style.css';
import '../../assets/css/font-awesome.min.css';

import { connect, ReactReduxContext } from 'react-redux'

import LogoutFunc from '../helpers/logout'

import getUserType from '../helpers/getUserType'
import userTypes from '../../constants/userTypes'
import swal from 'sweetalert'

// import { slide as Menu } from 'react-burger-menu'

// import '../../assets/css/custom.css'



class Header extends React.Component {

   constructor(props) {
      super(props)

      this.state = {
         currentUserType: '',
         width: 0,
         height: 0,
         open: null
      }

      this.updateWindowDimensions = this.updateWindowDimensions.bind(this);
   }

   logout = () => {
      LogoutFunc()
   }

   componentDidMount() {

      this.updateWindowDimensions();
  
      window.addEventListener('resize', this.updateWindowDimensions);

      if(localStorage.getItem('token')) {
         getUserType().then((response) => {
            this.setState({
               currentUserType: response.data.user.usersTypeId
            })
         }).catch((err) => {
         
            // alert(err.response.data.error)
            swal(err.response.data.error, 'You need to login again', 'error')
            .then((value) => {
               this.logout()
            })
            
         })
      }
   }

   componentWillUnmount() {
      window.removeEventListener('resize', this.updateWindowDimensions);
    }
    
    updateWindowDimensions() {
      this.setState({ width: window.innerWidth, height: window.innerHeight });
    }

   showSettings (event) {
      event.preventDefault();
 
    }

    handleClick() {
      this.setState({
          open: !this.state.open
      })
   }
   
   

   render(){
      console.log('Dimension: ', this.state.width, ' - ', this.state.height)
      

    return(
       
       <div>
          <nav className="navbar fixed-top navbar-expand-lg bg-white px-5">
        <Link className="navbar-brand" to="/"><img src={logo2} alt="jobguru" className="stick-logo" /></Link>
        <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon" > <i class="fa fa-navicon" style={{color: "Grey", fontSize:"28px"}}></i></span>
        </button>
        <div className="collapse navbar-collapse " id="navbarSupportedContent">
          <ul className="navbar-nav mr-auto">

          {localStorage.getItem('token') ?
            <></>
            :
            <li className="nav-item">
               <Link to="/about"className="nav-link">About Us</Link>
            </li>
                              
          }
                                 {
                                 this.state.currentUserType == userTypes.professionals
                                 ?
                                    <>
                                       <li className="nav-item"><Link to="/BrowseCategories" className="nav-link">Browse Categories</Link></li>
                                       <li className="nav-item"><Link to="/browsecompanies" className="nav-link">Browse Companies</Link></li>
                                       <li className="nav-item"><Link to="/candidatedashboard" className="nav-link">My Profile</Link></li>
                                    </>
                                 :
                                    <>
                                       
                                    </>
                                 }
                                 {
                                 this.state.currentUserType == userTypes.employers
                                 ?
                                    <>
                                       <li className="nav-item"><Link to="/browsecandidates" className="nav-link">Browse Candidates</Link></li>
                                       <li className="nav-item"><Link to="/companydetails" className="nav-link">Company Details</Link></li>
                                       <li className="nav-item"><Link to="/createjob" className="nav-link">Post a Job</Link></li>
                                       <li className="nav-item"><Link to="/employerdashboard" className="nav-link">My Profile</Link></li>
                                    </>
                                 :
                                    <>
                                      <li className="nav-item"><Link to="/browsejobs" className="nav-link">Browse Jobs</Link></li>
                                      
                                    </>
                                   
                                 }
          </ul>
          {/* <form className="form-inline">
            <div className="md-form my-0">
              <input className="form-control mr-sm-2" type="text" placeholder="try 'graphics designer'" aria-label="Search" />
            </div>
            <button className="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
          </form> */}
          <div className="header-right-menu">
                        <ul>
                          {localStorage.getItem('token') ?
                              <li className="nav-item"><Link  onClick={this.logout} to="/" style={{color: "#393941"}}>Log Out</Link></li>
                              :
                              <>
                                 
                                 <li className="nav-item"><Link to="/register" className="nav-link" style={{color: "#393941"}}><i className="fa fa-user"></i>sign up</Link></li>
                                 <li className="nav-item"><Link to="/login" className="nav-link" style={{color: "#393941"}}><i className="fa fa-lock"></i>login</Link></li>
                                 <li className="nav-item"><Link to="/createjob" className="nav-link" className="post-jobs">Post Jobs</Link></li>                         
                                 
                              </>
                           }                                                                   
                        </ul>
                     </div>
        </div>
      </nav>
       </div>
       
    )
  }
}

export default connect()(Header)