import React, { Component } from 'react'
import Layout from '../Layouts/Layout';
import { Link } from 'react-router-dom'
import Avatar from 'react-avatar';

import axiosInterceptors from '../helpers/axiosInterceptors';
import swal from 'sweetalert';

import Shimmer from "react-shimmer-effect";
import injectSheet from "react-jss";
import BrowseCompanies from './BrowseCompanies';

// Images
import avatar from '../../assets/img/male-avatar.png'

const StyleSheet = {
   container: {
     border: "0px solid rgba(255, 255, 255, 1)",
     boxShadow: "0px 0px 20px rgba(0, 0, 0, .1)",
     borderRadius: "4px",
     backgroundColor: "white",
     display: "flex",
     padding: "16px",
     width: "200px"
   },
   circle: {
     height: "56px",
     width: "56px",
     borderRadius: "50%"
   },
   line: {
     width: "96px",
     height: "8px",
     alignSelf: "center",
     marginLeft: "16px",
     borderRadius: "8px"
   }
 };

class BrowseCandidates extends Component {
   


  constructor(props) {
    super(props);

    
    this.state = {
       isLoading: true,
       professionals: null,
      //  isFreelance: false,
      //  isFullTime: false,
      //  isInternship: false,
      //  isPartTime: false,
      //  isTemporary: false

    }
  }

  componentDidMount() {
    this.getProfessionals()
 }

 getProfessionals() {
  axiosInterceptors().get('/professionalsLimit', {
     params: {
        offset: 0,
        limit: 6
     }
  })
  .then((response) => {
     this.setState({
        professionals: response.data.success,
        isLoading: false
     })
     console.log('Response: ', response.data.success)
  })
  .catch((err) => {
     swal('Professionals could not be loaded', 'There was a problem loading professionals', 'error')
  })
}

  // onChangeFreelance = () => {
  //   this.setState(initialState => ({
  //     isFreelance: !initialState.isFreelance,
  //   }));
  // }

  // onChangeFullTime = () => {
  //   this.setState(initialState => ({
  //     isFullTime: !initialState.isFullTime,
  //   }));
  // }

  // onChangeInternship = () => {
  //   this.setState(initialState => ({
  //     isInternship: !initialState.isInternship,
  //   }));
  // }

  // onChangePartTime = () => {
  //   this.setState(initialState => ({
  //     isPartTime: !initialState.isPartTime,
  //   }));
  // }

  // onChangeTemporary = () => {
  //   this.setState(initialState => ({
  //     isTemporary: !initialState.isTemporary,
  //   }));
  // }


  // // Submit
  // onSubmit = (e) => {
  //   e.preventDefault();
  //   console.log(this.state);
  // }
  





  render() {
    const { classes } = this.props;
    return (
      <Layout>
        <div className="mt-5"></div>
          
        {/* Top Job Area Start */}
        <section className="jobguru-top-job-area browse-page section_70">
          <div className="container">
            <div className="row">
              <div className="col-md-12 col-lg-3">
                <div className="job-grid-sidebar">
                  {/* Single Job Sidebar Start */}
                  <div className="single-job-sidebar sidebar-location">
                    <h3>location</h3>
                    <div className="job-sidebar-box shadow">
                      <form>
                        <p>
                          <input type="search" placeholder="Location" />
                        </p>
                        {/* <p className="location-value">
                          <input type="text" defaultValue={50} />
                        </p> */}
                        {/* <div className="dropdown">
                          <button className="btn-dropdown dropdown-toggle" type="button" id="location" data-toggle="dropdown" aria-haspopup="true">km</button>
                          <ul className="dropdown-menu" aria-labelledby="location">
                            <li>km</li>
                            <li>miles</li>
                          </ul>
                        </div> */}
                      </form>
                    </div>
                  </div>
                  {/* Single Job Sidebar End */}
                  {/* Single Job Sidebar Start */}
                  <div className="single-job-sidebar sidebar-location">
                    <h3>Category</h3>
                    <div className="job-sidebar-box shadow">
                    <div className="single-input">
                              <select id="industry">
                                <option disabled>select category</option>
                                <option  value="Animators">Animators</option>
                                <option  value="2D Animators">2D Animators</option>
                                <option value="3D Animators">3D Animators</option>
                                <option value="content writers">Content Writers</option>
                                <option value="graphics designer">Graphics Designer</option>
                                <option value="illustrators">Illustrators</option>                               
                              </select>
                            </div>
                            </div>
                  </div>
                  {/* Single Job Sidebar End */}
                  {/* Single Job Sidebar Start */}
         
             <form>
                  <div className="single-job-sidebar sidebar-location">
                    <h3>job type</h3>
                    <div className="date-post-job job-sidebar-box shadow">
                      
                    <input type="radio" className="control-lab" name="gender" value="freelance" style={{content: '',
                     background: '#fff none repeat scroll 0 0',
                     borderRadius: '100%',
                     border: '1px solid #dbdbdb',
                     display: 'inline-block',
                     width: '1.4em',
                     height: '1.4em',
                     position: 'relative',
                     top: '0',
                     marginRight: '1em',
                     verticalAlign: 'middle',
                     cursor: 'pointer',
                     textAlign: 'center',
                     transition: 'all 250ms ease'
                     }}/> <label>Freelance </label><br/>
                    <input type="radio" className="control-lab" name="gender" value="fullTime" 
                    style={{content: '',
                    background: '#fff none repeat scroll 0 0',
                    borderRadius: '100%',
                    border: '1px solid #dbdbdb',
                    display: 'inline-block',
                    width: '1.4em',
                    height: '1.4em',
                    position: 'relative',
                    top: '0',
                    marginRight: '1em',
                    verticalAlign: 'middle',
                    cursor: 'pointer',
                    textAlign: 'center',
                    transition: 'all 250ms ease'
                    }}
                    /> <label>Full Time</label><br/>
                    <input type="radio" className="control-lab" name="gender" value="PartTime" 
                    style={{content: '',
                    background: '#fff none repeat scroll 0 0',
                    borderRadius: '100%',
                    border: '1px solid #dbdbdb',
                    display: 'inline-block',
                    width: '1.4em',
                    height: '1.4em',
                    position: 'relative',
                    top: '0',
                    marginRight: '1em',
                    verticalAlign: 'middle',
                    cursor: 'pointer',
                    textAlign: 'center',
                    transition: 'all 250ms ease'
                    }}
                    /> 
                    <label>Part Time </label><br />
                    <input type="radio" className="control-lab" name="gender" value="Temporary" 
                    style={{content: '',
                    background: '#fff none repeat scroll 0 0',
                    borderRadius: '100%',
                    border: '1px solid #dbdbdb',
                    display: 'inline-block',
                    width: '1.4em',
                    height: '1.4em',
                    position: 'relative',
                    top: '0',
                    marginRight: '1em',
                    verticalAlign: 'middle',
                    cursor: 'pointer',
                    textAlign: 'center',
                    transition: 'all 250ms ease'
                    }}
                    /> <label>Temporary</label>
                      {/* <div className="form-group form-radio">
                        <input id="last_hour" name="jobTime" type="radio" />
                        <label htmlFor="Freelance" className="inline control-lab">Freelance</label>
                      </div>
                      <div className="form-group form-radio">
                        <input id="last_24" name="jobTime" type="radio" />
                        <label htmlFor="FullTime" className="inline control-lab">Full Time</label>
                      </div>
                      <div className="form-group form-radio">
                        <input id="last_7" name="jobTime" type="radio" />
                        <label htmlFor="PartTime" className="inline control-lab">Part Time</label>
                      </div>
                      <div className="form-group form-radio">
                        <input id="last_14" name="jobTime" type="radio" />
                        <label htmlFor="Temporary" className="inline control-lab">Temporary</label>
                      </div> */}
                    
                      {/* <div className="form-group form-radio">
                        <input id="last_30" name="radio" type="radio" />
                        <label htmlFor="last_30" className="inline control-label">Last 30 days</label>
                      </div>
                      <div className="form-group form-radio">
                        <input id="last_all" name="radio" type="radio" />
                        <label htmlFor="last_all" className="inline control-label">all</label>
                      </div> */}
                    </div>
                  </div>
                  </form>
                  {/* Single Job Sidebar End */}
                  {/* Single Job Sidebar Start */}
                  {/* <div className="single-job-sidebar sidebar-type">
                    <h3>job type</h3>
                  <form onSubmit={this.onSubmit}>
                    <div className="job-sidebar-box">
                      <ul>
                        <li className="checkbox">
                          <input className="checkbox-spin" 
                          type="radio" 
                          id="Freelance" 
                          name="radio"
                          // checked={this.state.isFreelance}
                          // onChange={this.onChangeFreelance}
                          />
                          <label htmlFor="Freelance"><span />Freelance</label>
                        </li>
                        <li className="checkbox">
                        <input className="checkbox-spin" 
                        type="radio" 
                        name="role" 
                        id="Full_Time"
                          // checked={this.state.isFullTime}
                          // onChange={this.onChangeFullTime}
                          />
                          <label htmlFor="Full_Time"><span />Full Time</label>
                        </li>
                        <li className="checkbox">
                        <input className="checkbox-spin" 
                        type="radio" 
                        name="role" 
                        id="Internship"
                          // checked={this.state.isInternship}
                          // onChange={this.onChangeInternship}
                        />
                          <label htmlFor="Internship"><span />Internship</label>
                        </li>
                        <li className="radio">
                        <input className="checkbox-spin" 
                        type="radio" 
                        name="role" 
                        id="Part_Time" 
                        // checked={this.state.isPartTime}
                        // onChange={this.onChangePartTime}
                        />
                          <label htmlFor="Part_Time"><span />Part Time</label>
                        </li>
                        <li className="checkbox">
                        <input className="checkbox-spin" 
                        type="radio" 
                        name="role" 
                        id="Temporary" 
                        />
                        <label htmlFor="Temporary"><span />Temporary</label>
                        </li>
                      </ul>
                    </div>
                    </form>
                  </div> */}
                 
                  {/* Single Job Sidebar End */}
                  {/* Single Job Sidebar Start */}
                  {/* <div className="single-job-sidebar sidebar-salary">
                    <h3>Filter by Rate</h3>
                    <div className="job-sidebar-box">
                      <p>
                        <input type="text" id="amount" readOnly />
                      </p>
                      <div id="slider" />
                    </div>
                  </div> */}
                  {/* Single Job Sidebar End */}
                </div>
              </div>
              <div className="col-md-12 col-lg-9">
                <div className="job-grid-right">
                  <div className="browse-job-head-option shadow">
                    <div className="job-browse-search">
                      <form>
                        <input type="search" placeholder="Search Jobs Here..." />
                        <button type="submit"><i className="fa fa-search" /></button>
                      </form>
                    </div>
                    <div className="job-browse-action">
                      {/* <div className="email-alerts">
                        <input type="checkbox" className="styled" id="b_1" />
                        <label className="styled" htmlFor="b_1">email alerts for this search</label>
                      </div> */}
                      <div className="dropdown">
                        <button className="btn-dropdown dropdown-toggle" type="button" id="dropdowncur" data-toggle="dropdown" aria-haspopup="true">Sort By</button>
                        <ul className="dropdown-menu" aria-labelledby="dropdowncur">
                          <li>Newest</li>
                          <li>Oldest</li>
                          <li>Random</li>
                        </ul>
                      </div>
                    </div>
                  </div>
                  {/* end job head */}
                  <div className="candidate-list-page shadow">
                  {!this.state.isLoading ?
                    this.state.professionals.map(allRound => {
                      const {userId, firstName, lastName, address, name, bio} = allRound;
                      console.log('some userId', userId)
                      return(
                          
                          <div className="single-candidate-list">
                            <div className="main-comment">
                              <div className="candidate-image">
                              <Avatar name={firstName + ' ' + lastName} size="70" round="50px" color="Grey"/>
                              </div>
                              <div className="candidate-text">
                                <div className="candidate-info">
                                  <div className="candidate-title">
                                    <h3><Link to="#">{firstName} {lastName}</Link></h3>
                                    {/* <img src="assets/img/de.svg" alt="region" /> */}
                                  </div>
                                  <p> - {name}</p>
                                </div>
                                <div className="candidate-text-inner">
                                  <p>{bio}</p>
                                </div>
                                <div className="candidate-text-bottom">
                                  <div className="candidate-text-box">
                                    <p className="varify"><i className="fa fa-check"></i> Verified</p>
                                    <p className="company-state"><i className="fa fa-map-marker" /> {address}</p>
                                  </div>
                                  <div className="candidate-action">
                                    <Link to={'/candidatesdetails/?query'+userId} userId={userId} className="jobguru-btn-2">view profile</Link>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          
                      )
                    })
                    :
                    <div className="col-lg-4 col-md-6 col-sm-6">
                    <Link to="#" className="single-category-holder account_cat">
                        <div className={classes.container}>
                          <Shimmer>
                              <div className={classes.circle} />
                              <div className={classes.line} />
                          </Shimmer>
                        </div>
                    </Link>
                  </div>
                  }
                  </div>
                  
                  <div className="pagination-box-row">
                    <p>Page 1 of 6</p>
                    <ul className="pagination">
                      <li className="active"><Link to="#">1</Link></li>
                      <li><Link to="#">2</Link></li>
                      <li><Link to="#">3</Link></li>
                      <li>...</li>
                      <li><Link to="#">6</Link></li>
                      <li><Link to="#"><i className="fa fa-angle-double-right" /></Link></li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        {/* Top Job Area End */}
      
      </Layout>
    )
  }
}

export default injectSheet(StyleSheet)(BrowseCandidates);
