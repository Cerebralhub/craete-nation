import React from 'react'
import { TagInput } from 'reactjs-tag-input'

import axiosInterceptors from '../helpers/axiosInterceptors'
import axios from 'axios';
import swal from 'sweetalert';
import Avatar from 'react-avatar';
import { moment } from 'moment';

class Dashboardcontent extends React.Component{

   constructor(props) {
      super(props)
      
      this.state = {
         firstName: '',
         lastName: '',
         phoneNumber: '',
         email: '',
         address: '',
         isLoading: true,
         categories: null,
         category: 1,
         categoryId: '',

         jobType: '',
         isJobTypeLoading: true,
         
         school: '',
         degree: '',
         fieldOfStudy: '',
         startYear: '',
         endYear: '',
         education: null,
         isEducationLoading: true,
         professionalInfo: {
            gender: 'male',
            dateOfBirth: '',
            bio: '',
            maritalStatus: 'single',
         },
         experienceInfo: {
            title: '',
            company: '',
            employmentType: '',
            startYear: '',
            endYear: '',
            currentlyWork: '',
            description: ''
         }
       }
   }

   componentDidMount() {
      this.getUserInfo()
      this.getCategories()
      this.getProfessionalDetails()
      this.getJobTypes()
      
    }

    addProfessionalEducation = e =>{
      e.preventDefault();

       axiosInterceptors().post('/professionals/add/education', {
         'school': this.state.school,
         'degree': this.state.degree,
         'fieldOfStudy': this.state.fieldOfStudy,
         'startYear': this.state.startYear,
         'endYear': this.state.endYear
       },{ params: {
         token: localStorage.getItem('token')
       }})
       .then((response) => {
         swal(response.data.success, 'Completed', 'success');
       })
       .catch((err) => {
         swal(err.response.data.error, 'Not Successful', 'error');
         console.log(err)
       })
    }

    addProfessionalExperience = e => {
      e.preventDefault();

       axiosInterceptors().post('/professionals/add/experience', {
         'title': this.state.experienceInfo.title,
         'company': this.state.experienceInfo.company,
         'employmentType': this.state.experienceInfo.employmentType,
         'startYear': this.state.experienceInfo.startYear,
         'endYear': this.state.experienceInfo.endYear,
         'currentlyWork': this.state.experienceInfo.currentlyWork,
         'description': this.state.experienceInfo.description
       },{ params: {
         token: localStorage.getItem('token')
       }})
       .then((response) => {
         swal(response.data.success, 'Completed', 'success');
       })
       .catch((err) => {
         swal(err.response.data.error, 'Not Successful', 'error');
         console.log(err)
       })
    }

    getUserInfo = () => {
      axiosInterceptors().get('/users', {
        params: {
          token: localStorage.getItem('token')
        }
      })
      .then((response) => {
        this.setState({
          firstName: response.data.user.firstName,
          lastName: response.data.user.lastName,
          phoneNumber: response.data.user.phoneNumber,
          email: response.data.user.email,
          address: response.data.user.address,
        })
      })
      .catch((err) => {
        // alert(err.response.data.error);
        swal(err.response.data.error, 'Not Successful', 'error');
        console.log(err.response.data.error);
        this.setState({ error: err.response.data.error })
      })
    }

    getCategories() {
      axiosInterceptors().get('/categories')
      .then((response) => {
         this.setState({
            categories: response.data.categories,
            isLoading: false
         })
      })
      .catch((err) => {
         swal('Categories could not be loaded', 'There was a problem loading categories', 'error');
      })
   }

   getJobTypes() {
      axiosInterceptors().get('/job-type')
      .then((response) => {
         console.log('jobtype: ',response.data.success)
         this.setState({
            jobType: response.data.success,
            isJobTypeLoading: false
         })
      })
      .catch((err) => {
         swal('Job Types could not be loaded', 'There was a problem loading job types', 'error');
      })
   }

   getProfessionalDetails = () => {
      axiosInterceptors().get('/professionals/show', {
        params: {
          'token': localStorage.getItem('token')
        }
      }).then((response) => {
        this.setState({
          professionalInfo: {
             bio: response.data.category.bio,
             gender: response.data.category.gender,
             dateOfBirth: response.data.category.dateOfBirth,
             maritalStatus: response.data.category.maritalStatus,
          },
          
          categoryId: response.data.category.categoryId,
          education: response.data.education,
          isEducationLoading: false
        })
        
      })
      .catch((err) => {
         console.log(err)
      })
    }

    updateProfessional = e => {
      e.preventDefault();

      console.log('bio ', this.state.professionalInfo.bio)

      axiosInterceptors().patch('/professionals/update', {
         'bio': this.state.professionalInfo.bio,
         'gender': this.state.professionalInfo.gender,
         'dateOfBirth': this.state.professionalInfo.dateOfBirth,
         'maritalStatus': this.state.professionalInfo.maritalStatus
      })
      .then((response) => {
        // alert(response.data.success)
        swal(response.data.success, 'Completed', 'success');
      })
      .catch((err) => {
        // alert(err.response.data.error)
        swal(err.response.data.error, 'Not Successful', 'error');
        console.log(err)
      })
  
    }

    handleSubmit = e => {
      e.preventDefault();
      
      //Do some checks here
      // this.updateProfessional()
      // this.addProfessionalEducation()
      // alert(this.state.category)
    };
    
    returnGenderSelected = (value) => {
      if(this.state.professionalInfo.gender == value) 
        return true
    }
  
    returnMaritalSelected = (value) => {
      if(this.state.professionalInfo.maritalStatus == value) 
        return true
    }
  
    returnCategorySelected = (value) => {
      if(this.state.category == value) 
        return true
    }


    render(){
        return(
            
             <div className="col-lg-9 col-md-12">
                  <div className="dashboard-right">
                     <div className="welcome-dashboard card shadow">
                        <div className="card-body">
                           
                        <Avatar name={this.props.firstName + ' ' + this.props.lastName}  round="50px" color="Grey"/>
                           <h3 className="mt-4">Welcome <span>{this.props.firstName} {this.props.lastName}</span></h3>
                           {/* <h6 className="mt-2" style={{fontFamily:"Montserrat, sans-serif" , letterSpacing:"2px", color:"#505050"}}>
                              {this.state.category}
                           </h6> */}
                           <p className="mt-1">Nigeria - <a href="#" data-toggle="modal" data-target="#contactModal" style={{color:"#20D6DE"}}>Contact Info</a></p>
                        </div>
                     </div>
                     <div class="modal fade" id="contactModal" tabindex="-1" role="dialog" aria-labelledby="contactModalLabel" aria-hidden="true">
                           
                           <form onSubmit={this.handleSubmit}>
                           <div class="modal-dialog modal-dialog-centered" role="document">
                              <div class="modal-content">
                                 <div class="modal-header">
                                 <h5 class="modal-title" id="contactModalLabel">{this.props.firstName} {this.props.lastName}</h5>
                                 <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                 </button>
                                 </div>
                                 <div class="modal-body">
                                    
                                 <div className="">
                                 <h5 className="float-left" style={{fontSize: "18px"}}>Contact Info                                     
                                 </h5>
                                 <h5>
                                    <a href="#">
                                       <i className="fa fa-pencil float-right" data-toggle="modal" data-target="#editcontactModal" style={{color:"#C0C0C0"}}></i>
                                    </a>
                                 </h5><br/>
                                 <div className="mb-2"></div>
                                 <p className="float-left"><strong>Email:</strong></p><br/>
                                 <p>{this.state.email}</p>
                                 <p className="float-left"><strong>Phone:</strong></p><br/>
                                 <p>{this.state.phoneNumber}</p>
                                 <p className="float-left"><strong>Address:</strong></p><br/>
                                 <p>{this.state.address}</p>
                                 <p className="float-left"><strong>Birthday:</strong></p><br/>
                                 <p>{this.state.dateOfBirth}</p>
                                    {/* <div className="single-resume-feild feild-flex-2">

                                          <div className="single-input">
                                             <label htmlFor="name">Phone Number:</label>
                                             <input type="text" placeholder={this.state.phoneNumber} onChange={event => this.setState({ lastName: event.target.value }) } id="name" />
                                          </div>

                                          <div className="single-input">
                                             <label htmlFor="Email">Email:</label>
                                             <input type="email" placeholder={this.state.email} onChange={event => this.setState({ email: event.target.value }) } id="Email" />
                                          </div>
                                    </div>

                                    <div className="single-resume-feild feild-flex-2">

                                          <div className="single-input">
                                          <label htmlFor="Address">Address:</label>
                                          <input type="text" placeholder={this.state.address} onChange={event => this.setState({ address: event.target.value }) } id="Address" />
                                          </div>

                                          <div className="single-input">
                                          <label htmlFor="Gender">Gender:</label>
                                             <select id="gender" onChange={event => this.setState({ gender: event.target.value }) }>
                                             <option disabled>Gender</option>
                                             
                                             <option selected={this.returnGenderSelected('male') } value="male">Male</option>
                                             <option selected={this.returnGenderSelected('female')} value="female">Female</option>
                                             
                                             </select>
                                          </div>
                                    </div>

                                    <div className="single-input">
                                       <label htmlFor="dob">Date Of Birth:</label>
                                       <input type="date" id="dob" onChange={event => this.setState({ dateOfBirth: event.target.value }) } />
                                    </div> */}

                                 </div>                         
                                   
                                 </div>
                                 <div class="modal-footer">
                                 <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                 </div>          
                              </div>
                           </div>
                           </form>
                        </div>

                        <div class="modal fade" id="editcontactModal" tabindex="-1" role="dialog" aria-labelledby="editcontactModalLabel" aria-hidden="true">
                           
                           <form onSubmit={this.handleSubmit}>
                           <div class="modal-dialog modal-dialog-centered" role="document">
                              <div class="modal-content">
                                 <div class="modal-header">
                                 <h5 class="modal-title" id="editcontactModalLabel">Edit Contact</h5>
                                 <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                 </button>
                                 </div>
                                 <div class="modal-body">
                                    
                                    <div className="resume-box">
                                    <div className="single-resume-feild feild-flex-2">

                                          <div className="single-input">
                                             <label htmlFor="name">Phone Number:</label>
                                             <input type="text" placeholder={this.state.phoneNumber} onChange={event => this.setState({ lastName: event.target.value }) } id="name" />
                                          </div>

                                          <div className="single-input">
                                             <label htmlFor="Email">Email:</label>
                                             <input type="email" placeholder={this.state.email} onChange={event => this.setState({ email: event.target.value }) } id="Email" />
                                          </div>
                                    </div>

                                    <div className="single-resume-feild feild-flex-2">

                                          <div className="single-input">
                                          <label htmlFor="Address">Address:</label>
                                          <input type="text" placeholder={this.state.address} onChange={event => this.setState({ address: event.target.value }) } id="Address" />
                                          </div>

                                          <div className="single-input">
                                          <label htmlFor="Gender">Gender:</label>
                                             <select id="gender" onChange={event => this.setState({ professionalInfo: {gender: event.target.value}  }) }>
                                             <option disabled>Gender</option>
                                             
                                             <option selected={this.returnGenderSelected('male') } value="male">Male</option>
                                             <option selected={this.returnGenderSelected('female')} value="female">Female</option>
                                             
                                             </select>
                                          </div>
                                    </div>

                                    <div className="single-input">
                                       <label htmlFor="dob">Date Of Birth:</label>
                                       <input type="date" id="dob" onChange={event => this.setState({ professionalInfo: {dateOfBirth: event.target.value}  }) } />
                                    </div>
                                    </div>                         
                                   
                                 </div>
                                 <div class="modal-footer">
                                 <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                 <button type="button" class="btn" style={{backgroundColor: "#20D6DE", color: "#fff"}}>Save changes</button>
                                 </div>
                              </div>
                           </div>
                           </form>
                        </div>

                        
                        <div className="card shadow rounded mt-4">
                           <div className="card-body">
                              <h5 style={{fontSize: "18px", color:"#333", letterSpacing:"0.5px", fontWeight:"600", fontFamily:'Montserrat, sans-serif' }}>ABOUT <a href="#"><i className="fa fa-pencil float-right" data-toggle="modal" data-target="#aboutModal" style={{color:"#C0C0C0"}}></i></a></h5>
                              <p className="mt-3">
                                 {this.state.professionalInfo.bio}
                              </p>
                           </div>
                        </div>
                        <div class="modal fade" id="aboutModal" tabindex="-1" role="dialog" aria-labelledby="aboutModalLabel" aria-hidden="true">
                           
                           <form onSubmit={this.updateProfessional}>
                           <div class="modal-dialog modal-dialog-centered" role="document">
                              <div class="modal-content">
                                 <div class="modal-header">
                                 <h5 class="modal-title" id="aboutModalLabel">About</h5>
                                 <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                 </button>
                                 </div>
                                 <div class="modal-body">
                                    
                                    <div className="resume-box">
                                    <div className="single-resume-feild ">
                                       <div className="single-input">
                                          <label htmlFor="bio">Introduce Yourself:</label>
                                          <textarea id="bio" onChange={event => this.setState({ professionalInfo: {bio: event.target.value} }) } placeholder={this.state.professionalInfo.bio} />
                                       </div>
                                    </div>
                                    </div>                         
                                   
                                 </div>
                                 <div class="modal-footer">
                                 <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                 <button type="submit" class="btn" style={{backgroundColor: "#20D6DE", color: "#fff"}}>Save changes</button>
                                 </div>
                              </div>
                           </div>
                           </form>
                        </div>
                     <div className="row  mt-4">
                        <div className="col-lg-4 col-md-12 col-sm-12">
                           <div className="widget_card_page shadow grid_flex widget_bg_blue">
                              <div className="widget-icon">
                                 <i className="fa fa-gavel"></i>
                              </div>
                              <div className="widget-page-text">
                                 <h4>0</h4>
                                 <h2>Saved</h2>
                              </div>
                           </div>
                        </div>
                        <div className="col-lg-4 col-md-12 col-sm-12">
                           <div className="widget_card_page shadow grid_flex  widget_bg_purple">
                              <div className="widget-icon">
                                 <i className="fa fa-usd"></i>
                              </div>
                              <div className="widget-page-text">
                                 <h4>0</h4>
                                 <h2>Earnings</h2>
                              </div>
                           </div>
                        </div>
                        <div className="col-lg-4 col-md-12 col-sm-12">
                           <div className="widget_card_page shadow grid_flex widget_bg_dark_red">
                              <div className="widget-icon">
                                 <i className="fa fa-users"></i>
                              </div>
                              <div className="widget-page-text">
                                 <h4>0</h4>
                                 <h2>Jobs Applied</h2>
                              </div>
                           </div>
                        </div>
                     </div>

                     <div className="card shadow mt-4">
                           <div className="card-body">
                              <h5 style={{fontSize: "18px", color:"#333", letterSpacing:"0.5px", fontWeight:"600", fontFamily:'Montserrat, sans-serif'}}>EDUCATION<a href="#"><i className="fa fa-plus float-right" data-toggle="modal" data-target="#educationModal" style={{color:"#C0C0C0"}}></i></a></h5>
                              {!this.state.isEducationLoading &&
                                 this.state.education.map(educationAll => {
                                    console.log(educationAll)
                                    const {school, degree, fieldOfStudy, startYear, endYear} = educationAll
                                    return(
                                       <>
                                       <div className="mt-3">
                                       
                                          <p style={{fontSize: "16px"}}>
                                          <i className="fa fa-building-o"></i>
                                             <strong className="ml-2">{school}</strong>
                                          </p>
                                          <p className="ml" style={{fontSize: "13px"}}>
                                             {fieldOfStudy}, {degree}
                                          </p>
                                          <p className="ml" style={{fontSize: "12px", marginTop:"-5px"}}>{startYear} - {endYear}</p>
                                       </div>
                                       <hr/>
                                       </>
                                    )
                                 })
                              }
                              
                              
                           </div>
                     </div>

                     <div class="modal fade" id="educationModal" tabindex="-1" role="dialog" aria-labelledby="educationModalLabel" aria-hidden="true">
                           <div class="modal-dialog modal-dialog-centered" role="document">
                              <div class="modal-content">
                              <form onSubmit={this.addProfessionalEducation}>
                                 <div class="modal-header">
                                 <h5 class="modal-title" id="educationModalLabel">Add Education</h5>
                                 <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                 </button>
                                 </div>
                                 <div class="modal-body">
                                    
                                    <div className="resume-box">
                                    <div className="single-resume-feild ">
                                       <div className="single-input">
                                          <label htmlFor="name">School:</label>
                                          <input type="text" id="school" placeholder={this.state.school} onChange={event => this.setState({ school: event.target.value }) } />
                                       </div>
                                    </div>

                                    <div className="single-resume-feild ">
                                       <div className="single-input">
                                          <label htmlFor="name">Degree:</label>
                                          <input type="text" id="school"placeholder={this.state.degree} onChange={event => this.setState({ degree: event.target.value }) } />
                                       </div>
                                    </div>

                                    <div className="single-resume-feild ">
                                       <div className="single-input">
                                          <label htmlFor="name">Field Of Study:</label>
                                          <input type="text" id="school" placeholder={this.state.fieldOfStudy} onChange={event => this.setState({ fieldOfStudy: event.target.value }) } />
                                       </div>
                                    </div>

                                    <div className="single-resume-feild feild-flex-2">
                                    <div className="single-input">
                                       <label htmlFor="Start Year">Start Year:</label>
                                       <select id="start" onChange={event => this.setState({ startYear: event.target.value }) }>
                                       <option disabled>Year</option>
                                       <option value="2020">2020</option>
                                       <option value="2019">2019</option>
                                       <option value="2018">2018</option>
                                       <option value="2017">2017</option>
                                       <option value="2016">2016</option>
                                       <option value="2015">2015</option>
                                       <option value="2014">2014</option>
                                       <option value="2013">2013</option>
                                       <option value="2012">2012</option>
                                       <option value="2011">2011</option>
                                       <option value="2010">2010</option>
                                       <option value="2009">2009</option>
                                       <option value="2008">2008</option>
                                       <option value="2007">2007</option>
                                       <option value="2006">2006</option>
                                       <option value="2005">2005</option>
                                       <option value="2004">2004</option>
                                       <option value="2003">2003</option>
                                       <option value="2002">2002</option>
                                       <option value="2001">2001</option>
                                       <option value="2000">2000</option>
                                       <option value="1999">1999</option>
                                       <option value="1998">1998</option>
                                       <option value="1997">1997</option>
                                       <option value="1996">1996</option>
                                       <option value="1995">1995</option>
                                       <option value="1994">1994</option>
                                       <option value="1993">1993</option>
                                       <option value="1992">1992</option>
                                       <option value="1991">1991</option>
                                       <option value="1990">1990</option>
                                       <option value="1989">1989</option>
                                       <option value="1987">1987</option>
                                       <option value="1986">1986</option>
                                       <option value="1985">1985</option>
                                       <option value="1984">1984</option>
                                       <option value="1983">1983</option>
                                       <option value="1982">1982</option>
                                       
                                       </select>
                                    </div>
                                    <div className="single-input">
                                       <label htmlFor="End Year">End Year:</label>
                                       <select id="end" onChange={event => this.setState({ endYear: event.target.value }) }>
                                       <option disabled>Year</option>
                                       <option value="2020">2020</option>
                                       <option value="2019">2019</option>
                                       <option value="2018">2018</option>
                                       <option value="2017">2017</option>
                                       <option value="2016">2016</option>
                                       <option value="2015">2015</option>
                                       <option value="2014">2014</option>
                                       <option value="2013">2013</option>
                                       <option value="2012">2012</option>
                                       <option value="2011">2011</option>
                                       <option value="2010">2010</option>
                                       <option value="2009">2009</option>
                                       <option value="2008">2008</option>
                                       <option value="2007">2007</option>
                                       <option value="2006">2006</option>
                                       <option value="2005">2005</option>
                                       <option value="2004">2004</option>
                                       <option value="2003">2003</option>
                                       <option value="2002">2002</option>
                                       <option value="2001">2001</option>
                                       <option value="2000">2000</option>
                                       <option value="1999">1999</option>
                                       <option value="1998">1998</option>
                                       <option value="1997">1997</option>
                                       <option value="1996">1996</option>
                                       <option value="1995">1995</option>
                                       <option value="1994">1994</option>
                                       <option value="1993">1993</option>
                                       <option value="1992">1992</option>
                                       <option value="1991">1991</option>
                                       <option value="1990">1990</option>
                                       <option value="1989">1989</option>
                                       <option value="1987">1987</option>
                                       <option value="1986">1986</option>
                                       <option value="1985">1985</option>
                                       <option value="1984">1984</option>
                                       <option value="1983">1983</option>
                                       <option value="1982">1982</option>
                                       </select>
                                    </div>
                                 </div>

                                 </div>                         
                                   
                                 </div>
                                 <div class="modal-footer">
                                 <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                 <button type="submit" class="btn" style={{backgroundColor: "#20D6DE", color: "#fff"}}>Add</button>
                                 </div>
                                 </form>
                              </div>
                           </div>
                        </div>

                        <div className="card shadow mt-4">
                           <div className="card-body">
                              <h5 style={{fontSize: "18px", color:"#333", letterSpacing:"0.5px", fontWeight:"600", fontFamily:'Montserrat, sans-serif'}}>EXPERIENCE<a href="#"><i className="fa fa-plus float-right" data-toggle="modal" data-target="#experienceModal" style={{color:"#C0C0C0"}}></i></a></h5>
                              <div className="mt-3">
                                       
                                          <p style={{fontSize: "16px"}}>
                                          <i className="fa fa-briefcase"></i>
                                             <strong className="ml-2">Company Name</strong>
                                          </p>
                                          <p className="ml" style={{fontSize: "13px"}}>
                                             Role, Employer
                                          </p>
                                          <p className="ml" style={{fontSize: "12px", marginTop:"-5px"}}>2010 - 2018</p>
                                       </div>
                              <hr/>
                           </div>
                     </div>

                     <div class="modal fade" id="experienceModal" tabindex="-1" role="dialog" aria-labelledby="experienceModalLabel" aria-hidden="true">
                           <div class="modal-dialog modal-dialog-centered" role="document">
                              <div class="modal-content">
                              <form onSubmit={this.addProfessionalExperience}>
                                 <div class="modal-header">
                                 <h5 class="modal-title" id="experienceModalLabel">Add Experience</h5>
                                 <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                 </button>
                                 </div>
                                 <div class="modal-body">
                                    
                                    <div className="resume-box">
                                    <div className="single-resume-feild ">
                                       <div className="single-input">
                                          <label htmlFor="title">Title:</label>
                                          <input type="text" id="title" onChange={event => this.setState({ experienceInfo : {title: event.target.value} }) } placeholder={this.state.experienceInfo.title}  />
                                       </div>
                                    </div>

                                    <div className="single-resume-feild ">
                                       <div className="single-input">
                                          <label htmlFor="company">Company:</label>
                                          <input type="text" id="company" onChange={event => this.setState({ experienceInfo : {company: event.target.value} }) } placeholder={this.state.experienceInfo.company} />
                                       </div>
                                    </div>

                                    <div className="single-resume-feild ">
                                       <div className="single-input">
                                          <label htmlFor="name">Employment Type:</label>
                                          <select id="start" onChange={event => this.setState({ experienceInfo : {employmentType: event.target.value} }) }>
                                       <option disabled>Type</option>
                                       {!this.state.isJobTypeLoading && 
                                          this.state.jobType.map(typeJob => {
                                             const {id, jobType} = typeJob
                                             return(
                                                <>
                                                   <option value={jobType}>{jobType}</option>
                                                </>
                                                )
                                             }
                                          )
                                       }                  
                                       </select>
                                       </div>
                                    </div>

                                    <div className="single-resume-feild feild-flex-2">
                                    <div className="single-input">
                                       <label htmlFor="Start Year">Start Year:</label>
                                       <select id="start" onChange={event => this.setState({ experienceInfo : {startYear: event.target.value} }) }>>
                                       <option disabled>Year</option>
                                       <option value="2020">2020</option>
                                       <option value="2019">2019</option>
                                       <option value="2018">2018</option>
                                       <option value="2017">2017</option>
                                       <option value="2016">2016</option>
                                       <option value="2015">2015</option>
                                       <option value="2014">2014</option>
                                       <option value="2013">2013</option>
                                       <option value="2012">2012</option>
                                       <option value="2011">2011</option>
                                       <option value="2010">2010</option>
                                       <option value="2009">2009</option>
                                       <option value="2008">2008</option>
                                       <option value="2007">2007</option>
                                       <option value="2006">2006</option>
                                       <option value="2005">2005</option>
                                       <option value="2004">2004</option>
                                       <option value="2003">2003</option>
                                       <option value="2002">2002</option>
                                       <option value="2001">2001</option>
                                       <option value="2000">2000</option>
                                       <option value="1999">1999</option>
                                       <option value="1998">1998</option>
                                       <option value="1997">1997</option>
                                       <option value="1996">1996</option>
                                       <option value="1995">1995</option>
                                       <option value="1994">1994</option>
                                       <option value="1993">1993</option>
                                       <option value="1992">1992</option>
                                       <option value="1991">1991</option>
                                       <option value="1990">1990</option>
                                       <option value="1989">1989</option>
                                       <option value="1987">1987</option>
                                       <option value="1986">1986</option>
                                       <option value="1985">1985</option>
                                       <option value="1984">1984</option>
                                       <option value="1983">1983</option>
                                       <option value="1982">1982</option>
                                       
                                       </select>
                                    </div>
                                    <div className="single-input">
                                       <label htmlFor="End Year">End Year:</label>
                                       <select id="end" onChange={event => this.setState({ experienceInfo : {endYear: event.target.value} }) }>>
                                       <option disabled>Year</option>
                                       
                                       <option value="2020">2020</option>
                                       <option value="2019">2019</option>
                                       <option value="2018">2018</option>
                                       <option value="2017">2017</option>
                                       <option value="2016">2016</option>
                                       <option value="2015">2015</option>
                                       <option value="2014">2014</option>
                                       <option value="2013">2013</option>
                                       <option value="2012">2012</option>
                                       <option value="2011">2011</option>
                                       <option value="2010">2010</option>
                                       <option value="2009">2009</option>
                                       <option value="2008">2008</option>
                                       <option value="2007">2007</option>
                                       <option value="2006">2006</option>
                                       <option value="2005">2005</option>
                                       <option value="2004">2004</option>
                                       <option value="2003">2003</option>
                                       <option value="2002">2002</option>
                                       <option value="2001">2001</option>
                                       <option value="2000">2000</option>
                                       <option value="1999">1999</option>
                                       <option value="1998">1998</option>
                                       <option value="1997">1997</option>
                                       <option value="1996">1996</option>
                                       <option value="1995">1995</option>
                                       <option value="1994">1994</option>
                                       <option value="1993">1993</option>
                                       <option value="1992">1992</option>
                                       <option value="1991">1991</option>
                                       <option value="1990">1990</option>
                                       <option value="1989">1989</option>
                                       <option value="1987">1987</option>
                                       <option value="1986">1986</option>
                                       <option value="1985">1985</option>
                                       <option value="1984">1984</option>
                                       <option value="1983">1983</option>
                                       <option value="1982">1982</option>
                                       </select>
                                       
                                 
                                    <div className="single-input mt-1 mx-1">
                                       <div className="custom-control custom-checkbox">
                                          <input type="checkbox" className="custom-control-input" id="cb1" />
                                          <label className="custom-control-label" for="cb1">I currently work
                                          </label>
                                       </div>
                                       </div>
                                    </div>
                                 </div>

                                    <div className="single-resume-feild ">
                                       <div className="single-input">
                                          <label htmlFor="bio">Description:</label>
                                          <textarea id="bio" onChange={event => this.setState({ experienceInfo : {description: event.target.value} }) } placeholder={this.state.experienceInfo.description} />
                                       </div>
                                    </div>

                                 </div>                         
                                   
                                 </div>
                                 <div class="modal-footer">
                                 <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                 <button type="button" class="btn" style={{backgroundColor: "#20D6DE", color: "#fff"}}>Add</button>
                                 </div>
                                 </form>
                              </div>
                           </div>
                        </div>

                     <div className="card shadow mt-4">
                           <div className="card-body">
                              <h5 style={{fontSize: "18px", color:"#333", letterSpacing:"0.5px", fontWeight:"600", fontFamily:'Montserrat, sans-serif'}}>LICENSES & CERTIFICATIONS<a href="#"><i className="fa fa-plus float-right" data-toggle="modal" data-target="#certificationModal" style={{color:"#C0C0C0"}}></i></a></h5>
                              <p className="mt-3">
                              <span style={{fontWeight: "bold"}}>Advanced Diploma in QA Testing</span><br/>
                              <span style={{fontSize: "12px"}}>Issued Nov 2019 . No Expiration Date</span>
                              </p>
                           </div>
                     </div>

                     <div class="modal fade" id="certificationModal" tabindex="-1" role="dialog" aria-labelledby="certificationModalLabel" aria-hidden="true">
                           <div class="modal-dialog modal-dialog-centered" role="document">
                              <div class="modal-content">
                              <form onSubmit={this.handleSubmit}>
                                 <div class="modal-header">
                                 <h5 class="modal-title" id="certificationModalLabel">Add Licenses & Certifications</h5>
                                 <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                 </button>
                                 </div>
                                 <div class="modal-body">
                                    
                                    <div className="resume-box">
                                    <div className="single-resume-feild ">
                                       <div className="single-input">
                                          <label htmlFor="name">Name:</label>
                                          <input type="text" id="name" placeholder="Ex: Google Certified Graphics Designer" />
                                       </div>
                                       <div className="single-input">
                                          <label htmlFor="name">Issuing Organization:</label>
                                          <input type="text" id="org" placeholder="Ex: Google" />
                                       </div>
                                       <div className="single-input ml-1">
                                       <div className="custom-control custom-checkbox">
                                          <input type="checkbox" className="custom-control-input" id="cb1" />
                                          <label className="custom-control-label" for="cb1">No expiration date
                                          </label>
                                       </div>
                                       </div>
                                       <div className="single-resume-feild feild-flex-2">
                                    <div className="single-input">
                                       <label htmlFor="Issued date">Issued date:</label>
                                       <input type="date" id="isdate" onChange={event => this.setState({ dateOfBirth: event.target.value }) } />
                                    </div>
                                    <div className="single-input">
                                       <label htmlFor="Expiration date">Expiration date:</label>
                                       <input type="date" id="exdate" onChange={event => this.setState({ dateOfBirth: event.target.value }) } />
                                    </div>
                                 </div>
                                    </div>
                                    </div>                         
                                   
                                 </div>
                                 <div class="modal-footer">
                                 <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                 <button type="button" class="btn" style={{backgroundColor: "#20D6DE", color: "#fff"}}>Save</button>
                                 </div>
                                 </form>
                              </div>
                           </div>
                        </div>

                        <div className="card shadow rounded mt-4">
                           <div className="card-body">
                              <h5 style={{fontSize: "18px", color:"#333", letterSpacing:"0.5px", fontWeight:"600", fontFamily:'Montserrat, sans-serif'}}>SKILLS <a href="#"><i className="fa fa-plus float-right" data-toggle="modal" data-target="#skillsModal" style={{color:"#C0C0C0"}}></i></a></h5>
                           </div>
                        </div>
                        <div class="modal fade" id="skillsModal" tabindex="-1" role="dialog" aria-labelledby="skillsModalLabel" aria-hidden="true">
                           <div class="modal-dialog modal-dialog-centered" role="document">
                              <div class="modal-content">
                              <form onSubmit={this.handleSubmit}>
                                 <div class="modal-header">
                                 <h5 class="modal-title" id="skillsModalLabel">Add Skills</h5>
                                 <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                 </button>
                                 </div>
                                 <div class="modal-body">
                                    
                                    <div className="resume-box">
                                    <div className="single-resume-feild ">
                                    <div className="single-input">
                                          <label htmlFor="name">Skills:</label>
                                          <input type="text" id="addskills" placeholder="Ex: logo design" />
                                    </div>
                                    </div>
                                    </div>                         
                                   
                                 </div>
                                 <div class="modal-footer">
                                 <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                 <button type="button" class="btn" style={{backgroundColor: "#20D6DE", color: "#fff"}}>Add</button>
                                 </div>
                                 </form>
                              </div>
                           </div>
                        </div>

                     <div className="card shadow mt-4">
                           <div className="card-body">
                              <h5 style={{fontSize: "18px", color:"#333", letterSpacing:"0.5px", fontWeight:"600", fontFamily:'Montserrat, sans-serif'}}>SOCIAL ACCOUNTS</h5>
                              <div className="resume-box mt-4">
                                    <div className="single-resume-feild feild-flex-2">

                                          <div className="single-input">
                                             <label htmlFor="facebook">Facebook:</label>
                                             <input type="text" placeholder="https://" id="facebook" />
                                          </div>

                                          <div className="single-input">
                                             <label htmlFor="instagram">Instagram:</label>
                                             <input type="text" placeholder="https://"  id="instagram" />
                                          </div>
                                    </div>
                                    <div className="single-resume-feild feild-flex-2">

                                          <div className="single-input">
                                             <label htmlFor="behance">Behance:</label>
                                             <input type="text" placeholder="https://"  id="behance" />
                                          </div>

                                          <div className="single-input">
                                             <label htmlFor="dribble">Dribble:</label>
                                             <input type="text" placeholder="https://"  id="dribble" />
                                          </div>
                                    </div>
                                    <button type="button" class="btn" style={{backgroundColor: "#20D6DE", color: "#fff"}}>Save</button>                      
                                   </div>
                           </div>
                     </div>

                  </div>
               </div>
        );
    }
}

export default Dashboardcontent