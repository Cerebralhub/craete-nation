import React, { Component } from "react";
import { Link } from "react-router-dom";

import axiosInterceptors from "../helpers/axiosInterceptors";
import swal from "sweetalert";
import Avatar from 'react-avatar';
import Shimmer from "react-shimmer-effect";
import injectSheet from "react-jss";

import Layout from "../Layouts/Layout";

const StyleSheet = {
  container: {
    border: "0px solid rgba(255, 255, 255, 1)",
    boxShadow: "0px 0px 20px rgba(0, 0, 0, .1)",
    borderRadius: "4px",
    backgroundColor: "white",
    display: "flex",
    padding: "16px",
    width: "200px",
  },
  circle: {
    height: "56px",
    width: "56px",
    borderRadius: "50%",
  },
  line: {
    width: "96px",
    height: "8px",
    alignSelf: "center",
    marginLeft: "16px",
    borderRadius: "8px",
  },
};

class CandidatesDetails extends Component {
  constructor(props) {
    super(props);

    this.state = {
      userId: "",
      isLoading: true,
      professionals: null,
      firstName: "",
      lastName: "",
      email: "",
      phoneNumber: "",
      bio: "",
      gender: "",
      maritalStatus: "",
      availability: "",
    };
  }

  componentDidMount() {
    this.getCandidateDetails();
    console.log('props form page', props.match.params.value)
  }

  getCandidateDetails() {
    axiosInterceptors().get('/users', {
      params: {
        'token': localStorage.getItem('token')
      }
    })
    .then((response) => {
      console.log('UserId no', response)
      axiosInterceptors()
      .get('/professionals/show/'+'1' , {
        params: {
          token: localStorage.getItem("token"),
        },
      })
      .then((response) => {
        console.log('Professional response', response)
        this.setState({
          professionals: response.data,
          isLoading: false,
          firstName: response.data.category.firstName,
          lastName: response.data.category.lastName,
          email: response.data.category.email,
          phoneNumber: response.data.category.phoneNumber,
          bio: response.data.category.bio,
          name: response.data.category.name,
        });
        console.log("Response: ", response.data);
      })
      .catch((err) => {
        console.log(err)
        swal(
          "Professionals could not be loaded",
          "There was a problem loading professionals",
          "error"
        );
      });
    })

    
  }

  render() {
    return (
      <Layout>
        {/* Single Candidate Start */}
        <section className="single-candidate-page section_70">
          <div className="container">
            <div className="row">
              <div className="col-md-6">
                <div className="single-candidate-box">
                  <div className="single-candidate-img">
                  <Avatar name={this.state.firstName + ' ' + this.state.lastName} size="70" round="50px" color="Grey"/>
                  </div>
                  <div className="single-candidate-box-right">
                    <h4>
                      {this.state.firstName} {this.state.lastName}
                    </h4>
                    <p>{this.state.name}</p>
                    <div className="single-candidate-rate">
                      <p className="rating-company">4.9</p>
                      <ul>
                        <li>
                          <i className="fa fa-star" />
                        </li>
                        <li>
                          <i className="fa fa-star" />
                        </li>
                        <li>
                          <i className="fa fa-star" />
                        </li>
                        <li>
                          <i className="fa fa-star" />
                        </li>
                        <li>
                          <i className="fa fa-star-half-o" />
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-md-6">
                <div className="single-candidate-action">
                  {/* <a href="#" className="bookmarks"><i className="fa fa-star" />Bookmarks</a> */}
                  <a href="#" className="candidate-contact">
                    <i className="fa fa-envelope" />
                    contact
                  </a>
                </div>
              </div>
            </div>
          </div>
        </section>
        {/* Single Candidate End */}
        {/* Single Candidate Bottom Start */}
        <div className="single-candidate-bottom-area section_70">
          <div className="container">
            <div className="row">
              <div className="col-md-8 col-lg-9">
                <div className="single-candidate-bottom-left">
                  <div className="single-candidate-widget">
                    <h3>About Me</h3>
                    <p>{this.state.bio}</p>
                  </div>
                  <div className="single-candidate-widget">
                    <h3>Skills</h3>
                    <ul>
                      <li>
                        <a href="#">{this.state.name}</a>
                      </li>
                    </ul>
                  </div>

                  <div className="single-candidate-widget">
                    <h3>work History</h3>
                    <div className="single-work-history">
                      <div className="single-candidate-list">
                        <div className="main-comment">
                          <div className="candidate-image">
                            <img src="assets/img/a_logo.png" alt="author" />
                          </div>
                          <div className="candidate-text">
                            <div className="candidate-info">
                              <div className="candidate-title">
                                <h3>
                                  <a href="#">Lead UX/UI Designer</a>
                                </h3>
                              </div>
                              <p>
                                <i className="fa fa-calendar-check-o" />
                                September 2017 - Present
                              </p>
                            </div>
                            <div className="candidate-text-inner">
                              <p>
                                Pellentesque habitant morbi tristique senectus
                                et netus et malesuada fames ac turpis egestas.
                                Etiam eu velit cursus, tempor ipsum in, tempus
                                lectus. Nullam tempus nisi id nisl luctus, non
                                tempor justo molestie.
                              </p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="single-work-history">
                      <div className="single-candidate-list">
                        <div className="main-comment">
                          <div className="candidate-image">
                            <img src="assets/img/b_logo.png" alt="author" />
                          </div>
                          <div className="candidate-text">
                            <div className="candidate-info">
                              <div className="candidate-title">
                                <h3>
                                  <a href="#">Junior UX/UI Designer</a>
                                </h3>
                              </div>
                              <p>
                                <i className="fa fa-calendar-check-o" />
                                May 2015 - July 2017
                              </p>
                            </div>
                            <div className="candidate-text-inner">
                              <p>
                                Pellentesque habitant morbi tristique senectus
                                et netus et malesuada fames ac turpis egestas.
                                Etiam eu velit cursus, tempor ipsum in, tempus
                                lectus. Nullam tempus nisi id nisl luctus, non
                                tempor justo molestie.
                              </p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-md-4 col-lg-3">
                <div className="single-candidate-bottom-right">
                  {/* <div className="single-candidate-widget-2">
                        <a href="#" className="jobguru-btn-2">
                        <i className="fa fa-balance-scale"></i>
                        compare with others
                        </a>
                     </div> */}
                  <div className="single-candidate-widget-2">
                    <ul>
                      <li>
                        <i className="fa fa-envelope"></i> example@mail.com
                      </li>
                      <li>
                        <i className="fa fa-phone"></i> +11-012-3456-89
                      </li>
                      <li>
                        <i className="fa fa-globe"></i> www.example.com
                      </li>
                    </ul>
                  </div>
                  <div className="single-candidate-widget-2">
                    <h3>Social Links</h3>
                    <ul className="candidate-social">
                      <li>
                        <a href="#">
                          <i className="fa fa-facebook"></i>
                        </a>
                      </li>
                      <li>
                        <a href="#">
                          <i className="fa fa-twitter"></i>
                        </a>
                      </li>
                      <li>
                        <a href="#">
                          <i className="fa fa-linkedin"></i>
                        </a>
                      </li>
                      <li>
                        <a href="#">
                          <i className="fa fa-google-plus"></i>
                        </a>
                      </li>
                      <li>
                        <a href="#">
                          <i className="fa fa-instagram"></i>
                        </a>
                      </li>
                    </ul>
                  </div>
                  <div className="single-candidate-widget-2">
                    <h3>Quick Contacts</h3>
                    <form>
                      <p>
                        <input type="text" placeholder="Your Name" />
                      </p>
                      <p>
                        <input type="email" placeholder="Your Email Address" />
                      </p>
                      <p>
                        <textarea placeholder="Write here your message"></textarea>
                      </p>
                      <p>
                        <button type="submit">Send Message</button>
                      </p>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        {/* Single Candidate Bottom End */}
      </Layout>
    );
  }
}

export default CandidatesDetails;
