import React from 'react';
import { Link } from 'react-router-dom';
import StarRatings from 'react-star-ratings';
import Avatar from 'react-avatar';

// images
import Company1 from '../../assets/img/company1.png';
import Company2 from '../../assets/img/company2.png';
import Company3 from '../../assets/img/company3.png';
import Company4 from '../../assets/img/company4.png';

import axiosInterceptors from '../helpers/axiosInterceptors';
import swal from 'sweetalert';

import Shimmer from "react-shimmer-effect";
import injectSheet from "react-jss";

const StyleSheet = {
   container: {
     border: "0px solid rgba(255, 255, 255, 1)",
     boxShadow: "0px 0px 20px rgba(0, 0, 0, .1)",
     borderRadius: "4px",
     backgroundColor: "white",
     display: "flex",
     padding: "16px",
     width: "200px"
   },
   circle: {
     height: "56px",
     width: "56px",
     borderRadius: "50%"
   },
   line: {
     width: "96px",
     height: "8px",
     alignSelf: "center",
     marginLeft: "16px",
     borderRadius: "8px"
   }
 };

class Profilelist extends React.Component {

   constructor(props){
      super(props);

      
      this.state = {
         isLoading: true,
         professionals: null
      }
  }

     
 changeRating( newRating, name ) {
   this.setState({
     rating: newRating
   });
 }

  componentDidMount() {
     this.getProfessionals()
  }

  getProfessionals() {
   axiosInterceptors().get('/professionalsLimit', {
      params: {
         offset: 0,
         limit: 6
      }
   })
   .then((response) => {
      this.setState({
         professionals: response.data.success,
         isLoading: false
      })
      console.log('Response: ', response.data.success)
   })
   .catch((err) => {
      swal('Categories could not be loaded', 'There was a problem loading categories', 'error')
   })
}

   render() {
      const { classes } = this.props;
      
    return (
        <div className="container">
        <div className="row">
               <div className="col-md-12">
                  <div className=" job-tab">
                  </div>
                  <div className="tab-content" id="pills-tabContent">
                     <div className="tab-pane fade show active" id="pills-companies" role="tabpanel" aria-labelledby="pills-companies-tab">
                        <div className="top-company-tab">
                           <ul>
                              {!this.state.isLoading ?
                                 this.state.professionals.map(allRound => {
                                    const {firstName, lastName, address, name, userId} = allRound;
                                    return(
                                       <li>
                                          <div className="top-company-list">
                                             <div className="company-list-logo">
                                                <Link to="#">
                                                <Avatar name={firstName + ' ' + lastName} size="70" round="50px" color="Grey"/>
                                                </Link>
                                             </div>
                                             <div className="company-list-details">
                                                <h3><Link to="#">{firstName} {lastName} - {name}</Link></h3>
                                                <p className="company-state"><i className="fa fa-map-marker"></i> {address}</p>
                                                <p className="varify"><i className="fa fa-check"></i>Verified</p>
                                                <StarRatings
                                                   rating={this.state.rating}
                                                   starRatedColor="rgb(255,255,0)"
                                                   // changeRating={this.changeRating}
                                                   numberOfStars={5}
                                                   name='rating'
                                                   starDimension='15px'
                                                   starSpacing='1px'
                                                   starEmptyColor='rgb(255,255,0)'
                                                />
                                             </div>
                                             <div className="company-list-btn">
                                                <Link to={'/candidatesdetails/'+userId} className="jobguru-btn">view profile</Link>
                                             </div>
                                          </div>
                                       </li>
                                    )
                                 })
                                 :
                                 <>
                                 <li>
                                    <div className="top-company-list">
                                       <div className={classes.container}>
                                          <Shimmer>
                                             <div className={classes.circle} />
                                             <div className={classes.line} />
                                             <div className={classes.line} />
                                             <div className={classes.line} />
                                          </Shimmer>
                                       </div>
                                    </div>
                                 </li>
                                 <li>
                                    <div className="top-company-list">
                                       <div className={classes.container}>
                                          <Shimmer>
                                             <div className={classes.circle} />
                                             <div className={classes.line} />
                                             <div className={classes.line} />
                                             <div className={classes.line} />
                                          </Shimmer>
                                       </div>
                                    </div>
                                 </li>
                                 <li>
                                    <div className="top-company-list">
                                       <div className={classes.container}>
                                          <Shimmer>
                                             <div className={classes.circle} />
                                             <div className={classes.line} />
                                             <div className={classes.line} />
                                             <div className={classes.line} />
                                          </Shimmer>
                                       </div>
                                    </div>
                                 </li>
                                 </>
                                 }
                              
                           </ul>
                        </div>
                     </div>
                     <div className="tab-pane fade" id="pills-job" role="tabpanel" aria-labelledby="pills-job-tab">
                        <div className="top-company-tab">
                            <ul>
                              <li>
                                 <div className="top-company-list">
                                    <div className="company-list-logo">
                                       <Link to="#">
                                       <img src="assets/img/company-logo-4.png" alt="company list 1" />
                                       </Link>
                                    </div>
                                    <div className="company-list-details">
                                       <h3><Link to="#">jamal Adelokun - 2D & 3D Animator</Link></h3>
                                       <p className="company-state"><i className="fa fa-map-marker"></i> Lagos, Nigeria</p>
                                       <p className="varify"><i className="fa fa-check"></i>Verified</p>
                                       <p className="rating-company">4.9</p>
                                    </div>
                                    <div className="company-list-btn">
                                       <Link to="#" className="jobguru-btn">view profile</Link>
                                    </div>
                                 </div>
                              </li>
                           <li>
                                 <div className="top-company-list">
                                    <div className="company-list-logo">
                                       <Link to="#">
                                       <img src="assets/img/company-logo-4.png" alt="company list 1" />
                                       </Link>
                                    </div>
                                    <div className="company-list-details">
                                       <h3><Link to="#">jamal Adelokun - 2D & 3D Animator</Link></h3>
                                       <p className="company-state"><i className="fa fa-map-marker"></i> Lagos, Nigeria</p>
                                       <p className="varify"><i className="fa fa-check"></i>Verified</p>
                                       <p className="rating-company">4.9</p>
                                    </div>
                                    <div className="company-list-btn">
                                       <Link to="#" className="jobguru-btn">view profile</Link>
                                    </div>
                                 </div>
                              </li>
                              <li>
                                 <div className="top-company-list">
                                    <div className="company-list-logo">
                                       <Link to="#">
                                       <img src="assets/img/company-logo-4.png" alt="company list 1" />
                                       </Link>
                                    </div>
                                    <div className="company-list-details">
                                       <h3><Link to="#">jamal Adelokun - 2D & 3D Animator</Link></h3>
                                       <p className="company-state"><i className="fa fa-map-marker"></i> Lagos, Nigeria</p>
                                       <p className="varify"><i className="fa fa-check"></i>Verified</p>
                                       <p className="rating-company">4.9</p>
                                    </div>
                                    <div className="company-list-btn">
                                       <Link to="#" className="jobguru-btn">view profile</Link>
                                    </div>
                                 </div>
                              </li>
                             <li>
                                 <div className="top-company-list">
                                    <div className="company-list-logo">
                                       <Link to="#">
                                       <img src="assets/img/company-logo-4.png" alt="company list 1" />
                                       </Link>
                                    </div>
                                    <div className="company-list-details">
                                       <h3><Link to="#">jamal Adelokun - 2D & 3D Animator</Link></h3>
                                       <p className="company-state"><i className="fa fa-map-marker"></i> Lagos, Nigeria</p>
                                    <p className="varify"><i className="fa fa-check"></i>Verified</p>
                                       <p className="rating-company">4.9</p>
                                    </div>
                                    <div className="company-list-btn">
                                       <Link to="#" className="jobguru-btn">view profile</Link>
                                    </div>
                                 </div>
                              </li>
                           </ul>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
          </div>  
    )
   }
}

export default injectSheet(StyleSheet)(Profilelist);