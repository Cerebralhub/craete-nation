const employmentType = {
    fullTime: 1,
    partTime: 2,
    selfEmployed: 3,
    inactive: 4,
    open: 5,
    closed: 6,
    applied: 7,
    suspended: 8
}

export default employmentType;